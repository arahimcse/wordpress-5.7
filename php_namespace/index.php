<?php

/**
 * php manual function used
 * spl_autoload_register(function($className){
     * include dirname(__FILE__)."\\".$className.".php";
    * });
    * new \inc\a\b\fruit();
    * new \inc\c\d\fruit(); 
* When I will call a class spl_autoload_register() search the function
*/

/**
 *  Another way is the dependancey manager namely composer (psr-4 autoload)
 */
require_once "vendor/autoload.php";
new \App\a\b\fruit();
new \App\c\d\fruit();
?>