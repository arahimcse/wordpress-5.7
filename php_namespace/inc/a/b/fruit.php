<?php
// namespace inc\a\b; manually by php build in spl_autoload_register() method

namespace App\a\b; // php dependency manager called composer 
/**
 * Declaring a single namespace
 * const CONNECT_OK = 1;
 * class Connection { }
 * function connect() {  }
 * The above example creates constant App\a\b\CONNECT_OK, class App\a\bConnection and function App\a\b\connect.
 */

class fruit{
    public function __construct()
    {
        echo 'Apple';
    }
}