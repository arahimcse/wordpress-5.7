=== fBiz ===
Contributors: tishonator
Tags: blog, e-commerce, portfolio, two-columns, right-sidebar, custom-background, custom-colors,
custom-header, custom-menu, featured-images, post-formats, threaded-comments,
translation-ready, full-width-template, front-page-post-form
Tested up to: 5.7
Stable tag: 1.3.8
Requires PHP: 5.6
Requires at least: 5.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Clean, Responsive, Easy to use.

== Description ==

Clean, Responsive, Easy to use. fBiz is a WordPress theme suitable for a corporate, business, startup,
a company website or a blog. It is fully responsive, built-in slider included, WooCommerce plugin support,
multi-level drop-down menu, W3C markup validated, right sidebar widget area, 3 homepage columns widget areas,
customize options, search engine optimized, cross-browser compatible (IE9, IE10, IE11, Chrome, Firefox, Opera,
and Safari), Full Width Page Template, FontAwesome integration, translation ready and much more.

== Frequently Asked Questions ==

= How to Display the Homepage Slider =

1. Create a Static Front Page

The slider is visualized ONLY when there is a static front page set for your website. So, the first step is:

1.1. Login to your Admin Panel
1.2. Navigate to Left Menu -> Settings -> Reading
1.3. Select 'A static page (select below)', then choose pages for 'Homepage' and 'Posts page'
1.4. Save changes

Reference: https://codex.wordpress.org/Creating_a_Static_Front_Page

2. Update Slider Settings

2.1. Login to your Admin Panel
2.2. Navigate to Left Menu -> Appearance -> Customize
2.3. Open 'Slider' Section
2.4. Check the 'Display Slider' checkbox
2.5. Set Slider Images ('Slide # Image' fields) and Text ('Slide # Content' fields) for the different Slides
2.6. Save Changes


== Changelog ==

= 1.3.9 =
* Update 'Tested up to' tag to 5.7

= 1.3.8 =
* update description
* fix issue with starter content

= 1.3.7 =
* Fix deprecated jQuery code usage

= 1.3.6 =
* Make Links within content underlined

= 1.3.5 =
* updates for WordPress ver. 5.5

= 1.3.4 =
* update screenshot images with CC0 license

= 1.3.3 =
* Bug fix: make main menu accessible for keyboard navigation under IE and Edge browsers

= 1.3.2 =
* move 'Requires PHP' and 'Requires at least' tags into style.css

= 1.3.1 =
* fix issue with sub-menu items not displayed under IE 11 and Edge browsers

= 1.3.0 =
* update font sizes and layout

= 1.2.9 =
* fix issue with menu items display

= 1.2.8 =
* increase font size

= 1.2.7 =
* add Mobile Keyboard navigation for Main Menu

= 1.2.6 =
* add Skip links for screen readers
* add highlighting of form fields, submit buttons and text links

= 1.2.5 =
* fix issue with title space

= 1.2.4 =
* add css code for Blocks
* update PHP min version required to 5.6.0

= 1.2.3 =
* fix issue with Animations display

= 1.2.2 =
* add background color at footer bottom

= 1.2.1 = 
* update FontAwesome license declaration in readme.txt file
* improve Customizer Sanitization Callback functions

= 1.2.0 = 
* fix issue with image caption alignment

= 1.1.9 (2019-02-06) =
* update Slider Display text in Customizer option to 'Display Slider on a Static Front Page'

= 1.1.8 (2018-12-31) =
* add starter content

= 1.1.7 (2018-12-16) =
* add mobile menu items expand/collapse functionality

= 1.1.6 (2018-11-20) =
* remove set_post_thumbnail_size from functions.php
* update Resources' images info 


= 1.1.5 (2018-11-05) =
* update readme.txt file
* add WooCommerce plugin support

= 1.1.4 (2018-10-27) =
* add instructions in readme.txt file how to configure the Homepage slider

= 1.1.3 (2018-10-19) =
* remove recommended plugins functionality
* move changelog into readme.txt file

= 1.1.2 (2018-08-28) =
* update screenshot according to latest WordPress requirements for screenshot

= 1.1.1 (2018-08-04) =
* display slider and homepage widgets only on static homepage
* remove default widgets content

= 1.1.0 (2018-06-08) =
* fix issue with comments GDPR checkbox
* update screenshot.png

= 1.0.9 (2018-04-25) =
* fix issue with wrong Instagram slug widget

= 1.0.8 (2018-03-12) =
* add Pinterest widget support
* add Facebook widget support
* add Instagram widget support

= 1.0.7 (2017-12-23) =
* improve animation effects
* add footer menu

= 1.0.6 (2017-11-16) =
* add animations

= 1.0.5 (2017-01-08) =
* add full-width page template

= 1.0.4 (2016-11-18) =
* add custom logo
* add footer widget areas
* bug fixes

= 1.0.3 (2016-07-10) =
* update tags in style.css

= 1.0.2 (2016-02-13) =
* FontAwesome update
* bug fixes

= 1.0.1 (2015-10-10) =
* use customizer instead of admin options page

= 1.0.0 (2015-04-13) =
* Initial release

== Resources ==
* FontAwesome 4.6.3 © 2012 Dave Gandy https://github.com/FortAwesome/Font-Awesome, SIL OFL 1.1, MIT
* Twenty Nineteen 1.4 (Code Block CSS), © 2018 - 2019 wordpressdotorg, GNU v2.0
* Code examples and snippet library, © 2018 WordPress Theme Review Team https://github.com/WPTRT/code-examples/, GPL-2.0
* Customizer "Pro" theme section example, © 2016 Justin Tadlock, GNU v2.0
* js/unslider.js, © @idiot, WTFPL
* css/animate.css, © 2017 Daniel Eden, MIT
* js/viewportchecker.js, © 2014 Dirk Groenen, MIT
* img/1.jpg, © 2017 @PxHere https://pxhere.com/en/photo/970964, CC0
* img/2.jpg, © 2018 @PxHere https://pxhere.com/en/photo/1561537, CC0
* img/3.jpg, © 2017 @PxHere https://pxhere.com/en/photo/328535, CC0
* screenshot.png (products), WooCommerce, Copyright (C) Automattic, GPLv3
* screenshot.png (slider image), © 2017 @PxHere https://pxhere.com/en/photo/970964, CC0
