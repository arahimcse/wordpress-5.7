=== Agency Plus ===

Contributors: ithemer
Requires at least: 5.4.2
Requires PHP: 7.2
Tested up to: 5.5.1
Stable tag: 1.0.5
License: GNU General Public License v2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Agency Plus is a secure, speed optimized and fully customizable multi-purpose WordPress theme. It works perfectly with Gutenberg, Elementor, Beaver Builder, Brizy, KingComposer and most of the page builder plugins. Agency Plus is the perfect theme for agency, blogs, small business, startups, portfolio, law firm, university, school, college, medical and WooCommerce storefront with a beautiful & professional design.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== License ==

Agency Plus WordPress Theme, Copyright (C) 2020, WP Concern
Agency Plus is distributed under the terms of the GNU GPL

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Agency Plus includes support for Infinite Scroll in Jetpack.

== Changelog ==

= 1.0.5 - September 10 2020 =
* Extend compatibility to Wordpress 5.5.1
* Added 'output escaping' functions
* Refactoring of CSS and PHP code

= 1.0.4 - June 5 2020 =
* Footer changes
* Blog mobile width
* Demo link changes
* Required header fields for style.css

= 1.0.3 - Apr 13 2020 =
* Footer changes

= 1.0.3 - Apr 13 2020 =
* Fix minor issues
* Few CSS tweaks
* FontAwesome updated
* Theme info page updated

= 1.0.2 - January 14 2020 =
* Theme URL, Demo URL and Author URL updated
* Recommended plugins added

= 1.0.1 - August 05 2019 =
* Initial release

== Credits ==

- Underscores
Source: Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc.
License: [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

- Normalize
Source: normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal
License: [MIT](http://opensource.org/licenses/MIT)

- Google Fonts
Source: https://fonts.google.com/
License: SIL Open Font License, 1.1 - scripts.sil.org/OFL

- Font Awesome:
Source: http://fontawesome.io/, (C) Dave Gandy
License: (CSS)[MIT](http://opensource.org/licenses/MIT), (Fonts)[SIL OFL 1.1](http://scripts.sil.org/OFL)

- Meanmenu
(C) 2012-2013 Chris Wharton (themes@meanthemes.com), [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

== Images ==

* Image used in screenshot, License CC0 Public Domain
- https://pxhere.com/en/photo/1449409
- https://pxhere.com/en/photo/1071282
- https://pxhere.com/en/photo/1456587
