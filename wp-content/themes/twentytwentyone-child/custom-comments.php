<?php

if(post_password_required())
{
    return;
}
$comment_numbers = get_comments_number();
?>
<div class="container">
    <div class="row">
      <?php
        if(have_comments())
        {
            ?>
            <h2 class="comments-title">
               <?php printf(
					/* translators: %s: Comment count number. */
					esc_html( _nx( '%s comment', '%s comments', $comment_numbers, 'Comments title', 'twentytwentyone-child' ) ),
					esc_html( number_format_i18n( $comment_numbers ) )
				); ?>
            </h2>
            <?php
        }
        ?>
        <ol class="comment-body">
            <?php 
                    class wpList{
                        public function __construct()
                        {
                            wp_list_comments([
                    
                                'walker'            => null,
                                'max_depth'         => '',
                                'style'             => 'ul',
                                //'callback'          => [$this, 'callback_comment_html'],
                                'end-callback'      => null,
                                'type'              => 'comment',
                                'page'              => '',
                                'per_page'          => '',
                                'avatar_size'       => 32,
                                'reverse_top_level' => null,
                                'reverse_children'  => '',
                                'format'            => 'html5', // or 'xhtml' if no 'HTML5' theme support
                                'short_ping'        => false,   // @since 3.6
                                //'echo'              => true     // boolean, default is true
                            ]);
                        }                       
                    }
                    new wpList();
            ?>
        </ol>
    </div>
    <div class="row">
    <?php
        /**
         * comment form customize by args pass
         */
        // $commenter = wp_get_current_commenter();
        // $req = get_option( 'require_name_email' );
        //    // The args. 
        //    //link@ https://developer.wordpress.org/reference/functions/comment_form/
        // comment_form(
        //     [
        //         'cancel_reply_link' => __( 'Cancel reply' ),
		//         'label_submit' => __( 'Post Comment' ),
        //         'fields' => array(
        //             'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) . '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" /></p>',
        //             'email' => '<p class="comment-form-email"><label for="email">' . __( 'Email' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) . '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" /></p>',
        //             // 'url' => '<p class="comment-form-url"><label for="url">' . __( 'Website' ) . '</label>' . '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>',
        //         ),
        //         'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
        //         'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be [logged in](%s) to post a comment.' ), wp_login_url( get_permalink() ) ) . '</p>',
        //         'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Logged in as [%2$s](%1$s). [Log out?](%3$s)' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( get_permalink() ) ) . '</p>',
        //         'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email adress will not be published.' ) . '</p>',
        //         'id_form' => 'commentform',
        //         'id_submit' => 'submit',
        //         'label_submit' => __( 'Submit' ),
        //         'title_reply' => __( 'Leave a Reply' ),
        //         'title_reply_to' => __( 'Leave a Reply to %s' ),
        //         'cancel_reply_link' => __( 'Cancel reply' ),
        // ]);

        /**
         * comment form customize by filters
         */
        comment_form();
    
    ?>
    </div>
</div>