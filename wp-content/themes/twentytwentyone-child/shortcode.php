<?php

add_shortcode('foobar', 'test_shortcode');
function test_shortcode()
{
    return 'this is foo and bar practice';
}
//[foobar]

add_shortcode('bartag', 'myfunc');
function myfunc($atts)
{
    $a = shortcode_atts(array(
        'foo' => 'this is foo',
        'bar' => 'this is bar'
    ), $atts);
     return "foo = {$a['foo']}";
}
//[bartag foo="try"]

add_shortcode('caption', 'myfunc2');
function myfunc2($atts, $content)
{
    $a = shortcode_atts(array('class'=>'rahim'), $atts);
    return '<span class="'.$a['class'].'">'.$content.'</span>';

}
//[caption class="customeClass"]

add_shortcode('rahim', 'function23');
function function23($atts, $content)
{
    $a = shortcode_atts(array('class'=>'default'), $atts);
    return '<span class="'.$a['class'].'">'.$content.'</span>';
}


/**
 * short code with attributes
 * [tag-a att1]content[/tag-a]
 * 
 */

 add_shortcode('tag-a', 'rahim1');
 function rahim1($atts, $contents)
 {
     $a = shortcode_atts(array('class' => 'rahim1'), $atts); 
     /**
      * php output buffer 
      * ob_star() output buffer start . after this function all the content store into the output buffer. 
      * there have some other function that provides these content and delete the content from buffer.
      * ob_get_clean() function take the conent and delete from buffer
      */
     ob_start();
     ?>
        <div class="container">
            <div class="row">
                <h2 class="<?php echo $a['class'] ?>"><?php echo $contents ?></h2>
            </div>
        </div>
     <?php
     return ob_get_clean();
 }
/**
 * shortcode inside a shortcode that means do_shortcode()
 * [taga class="container"]
 *  [tagb class="row"]this is the test content[/tagb]
 * [tagb]
 */
add_shortcode('taga', 'ff');
function ff($atts, $contents = null)
{
    $a = shortcode_atts(array('class'=>'testClass'), $atts);
    return '<div class="'.$a['class'].'">'.do_shortcode($contents).'</div>';
}
add_shortcode('tagb', 'fff');
function fff($atts, $content)
{
    $a = shortcode_atts(array('class'=>'insideclass'), $atts);
    return '<div class="'.$a['class'].'">'.$content.'</div>';
}
?>