<?php
/*
enqueure child them style th

*/
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_uri(),
        array(), 
        wp_get_theme()->get('Version') // this only works if you have Version in the style header
    );
    wp_enqueue_style( 'child-bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css',
        array(), 
        wp_get_theme()->get('Version') // this only works if you have Version in the style header
    );
    wp_enqueue_script('child-bootsrap-js', get_stylesheet_directory_uri().'/assets/js/bootstrap.min.js', null, '1.0.0', true);
    /**
     * localization example of wordpress
     * link@ https://developer.wordpress.org/rest-api/using-the-rest-api/authentication/
     * It is applicable when the REST API is used inside of WordPress and the current user is logged in
     * https://developer.wordpress.org/reference/functions/wp_localize_script/
     */
    wp_localize_script( 'child-bootsrap-js', 'wpApiSettings', array(
        'root' => esc_url_raw( rest_url() ),
        'nonce' => wp_create_nonce( 'wp_rest' )
    ) );
}

/**
  * Set up My Child Theme's textdomain.
  *
  * Declare textdomain for this child theme.
  * Translations can be added to the /languages/ directory.
  */
  function twentyfifteenchild_theme_setup() {
    load_child_theme_textdomain( 'twentytwentyonechild', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'twentyfifteenchild_theme_setup' );


/**
 * crete meta boxes
 * link@ https://www.youtube.com/watch?v=6VDpvGTUa6A
 * link@ https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
 * link@ https://codex.wordpress.org/Function_Reference
 * 
 */
class wp_custom_meta{
    public function __construct()
    {
        add_action('add_meta_boxes', [$this, 'create_meta_box']); //add meta boxes hook
        add_action('save_post', [$this, 'save_post_meta']); //save the metabox data in the databases
    }
    /* create metaboxed  */
    public  function create_meta_box()
    {
        add_meta_box('wp_custom_meta_id', 'Test Metabox', [$this, 'wp_custom_meta_html'], ['post']);
    }
    /**meta boxes html user input or html form */
    public function wp_custom_meta_html($post)
    {
        $value = get_post_meta(get_the_ID(), 'wp_custom_meta_editor', true);
        $post_status = get_post_status(get_the_ID());
        ?>
        <label for="wp_custom_meta">Select Value: </label>
        <select name="wp_custom_meta_editor" id="wp_custom_meta">
                <?php
                if($value)
                { ?>
                    <option value="one" <?php selected($value, 'one') ?>>1</option>
                    <option value="two" <?php selected($value, 'two') ?>>2</option>
                    <option value="three" <?php selected($value, 'three') ?>>3</option>

                <?php
                } else if( $post_status == "auto-draft")
                { ?>
                    <option value="one">1</option>
                    <option value="two">2</option>
                    <option value="three">3</option>

                    <?php
                }
                else
                { ?>
                   <option selected > Not found </option>
                   <option value="one">1</option>
                    <option value="two">2</option>
                    <option value="three">3</option>

                <?php
                }
            
            ?>
        </select>
        <?php
    }

    /**Save the custom metabox data into the databases */

    public function save_post_meta($post_id)
    {
        $editor = sanitize_text_field($_POST['wp_custom_meta_editor']);
        if(isset($_POST['wp_custom_meta_editor']) && !empty($_POST['wp_custom_meta_editor']))
        {
            update_post_meta($post_id, 'wp_custom_meta_editor',$editor);
        }
    }
}
new wp_custom_meta();
/**
 * comment form customize by filters
 * 
 * list of avialable filters for comments form
 * comment_form_default_fields
 * comment_form_defaults
 * comment_form_logged_in
 * comment_form_fields
 * comment_form_field_comment
 * comment_form_field_{$name}
 * comment_form_submit_button
 * comment_form_submit_field
 * 
 * I will use the filter hook which one i want to modify.
 * 
 * reference 
 * link@ http://justintadlock.com/archives/2010/07/21/using-the-wordpress-comment-form
 * link@ https://developer.wordpress.org/reference/functions/comment_form/
 */
 function ia_comment_form_fields($fields){
    //$fields['email'] = null ;  //removes email field
    unset($fields['email']);  //removes email field
    $fields['url'] = null ;  //removes website field
    $fields['author'] = null ;
    $fields['cookies'] = null ;
    $fields['twitter'] = '<p class="comment-form-twitter"><label for="twitter">' . __( 'Twitter (@username)' ) . '</label><input type="text" id="twitter" name="twitter" value="" size="30" /></p>';
    return $fields;
}
add_filter('comment_form_default_fields','ia_comment_form_fields');

function ia_comment_form_text ($fields) {
    $fields['label_submit'] = 'Add Comment';
    $fields['title_reply'] = 'Got something to share? Let&#39;s hear it.';
       return $fields;
   }
   add_filter('comment_form_defaults','ia_comment_form_text');


/**
 * 
 * 
 * shortcode api
 * link@ https://codex.wordpress.org/Shortcode_API
 */
require 'shortcode.php';

require 'widgets.php';


/**
 * 
 * Wordpress dashboard API
 * link@ https://developer.wordpress.org/apis/handbook/dashboard-widgets/
 */

class lara_dashboard{
    public function __construct()
    {
        add_action('wp_dashboard_setup', [$this, 'lara_test_widget']);
    }

    public function lara_test_widget()
    {
        wp_add_dashboard_widget(
            'lara_test_widget_id',
            esc_html__( 'Example Dashboard Widget', 'wporg' ), 
            [$this, 'lara_widgets_callback']
        );
    }

    public function lara_widgets_callback()
    {
        ?>
        <div class="container">
            <div class="row">
                <h3><?php esc_html_e('Lara Widgets Test', 'twentytwentyone-child'); ?></h3>
                <?php echo wp_list_pages(); 
                $options = get_option( 'dashboard_widget_options' );
                var_dump($options);
                ?>
            </div>
        </div>
        <?php
    }
}
new lara_dashboard();

/**
 * 
 * databases API
 * wordpress databases API is the colllection of 
 * optionS API
 * transients API
 * metadata API
 * link@  https://developer.wordpress.org/apis/handbook/database/
 * 
 * now discuss options API
 * add an option in databases wp_options tables
 * there have 4 function
 * CRUD
 * create -- add_option()
 * Read/access -- get_option()
 * Update -- update_option()
 * delete -- delete_option()
 * 
 * But this Options API used combination of setting API and controlling under the setting pages
 * link@https://developer.wordpress.org/plugins/settings/options-api/
 */


 class lara_settings{
     public function __construct()
     {
         add_action('admin_init', [$this, 'lara_setting_options']);
     }

     public function lara_setting_options()
     {
         /**
          * A section contain multiple fileds 
          * there have 4 parameters $id, $title, $callback, $page. All of these are required
          * link@ https://developer.wordpress.org/reference/functions/add_settings_section/
          */
        add_settings_section('lara_setting_wrapper_id', 'Lara Setting', [$this, 'lara_setting_section'], 'general');

        /**
         * Register a option that save on wp_options table
         * It has 3 paramets 2 of them required and one optional. 
         * It mainly register an option for every field that means $option_name and input tag name attributes will same
         * link@ https://developer.wordpress.org/reference/functions/register_setting/
         */
         register_setting('general', 'lara_setting_option');
         /**
          * This function rgister and option field 
          * It has 6 parameters 4 of them required and other 2 options
          * link@ https://developer.wordpress.org/reference/functions/add_settings_field/
          */
         add_settings_field('lara_setting_field1', 'Customer Name', [$this, 'lara_setting_field_html'], 'general', 'lara_setting_wrapper_id');

         
         register_setting('general', 'lara_field_2');
         add_settings_field('lara_setting_field2', 'Customer Age', [$this, 'lara_setting_field2_html'], 'general', 'lara_setting_wrapper_id');
     }
     //section callback function
     public function lara_setting_section()
     {
        esc_html_e('Page limit 10', 'twentytwentyone-child');
     }

     //fields callback function

     public function lara_setting_field_html()
     {
         $data = get_option('lara_setting_option');
         var_dump($data);
         ?>
         <label for="lara_setting_input">Name</label>
         <input type="text" id="lara_setting_input" name="lara_setting_option" value="" placeholder="Abdur Rahim">
         <?php
     }

     public function lara_setting_field2_html()
     {
        ?>
            <label for="age">Age</label>
            <input type="number" id="age" name="lara_field_2" value="" placeholder="30">

        <?php
     }
 }
 new lara_settings();

 class lara_custom_post{
     public function __construct()
     {
         add_action('init', [$this, 'lara_custom_post']);
     }

     public function lara_custom_post()
     {
         $labels = [
            'name' => 'Custome Book'
         ];
         $args =[
            'labels'             => $labels,
            'description'        => 'Recipe custom post type.',
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'lbook' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => 20,
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'page-attributes' ),
            //'taxonomies'         => array( 'category', 'post_tag' ),
            'show_in_rest'       => true

         ];
         register_post_type('lbook', $args);
     }
 }
 new lara_custom_post();

 /**
  * transient api example
  */
 class lara_notic_transient{
     public function __construct()
     {
         add_action('admin_notices', [$this, 'lara_them_notice']);
     }
     
     public function lara_them_notice()
     {
         $data = get_transient('lara_notice');
         if(false === $data)
         {
             $notice = 'this is my lara noticefffjjj';
             set_transient('lara_notice', $notice, 25);
         }
         echo $data;
     }
 }
 new lara_notic_transient();

 /**
  * 
  * http api
  * link@ https://developer.wordpress.org/plugins/http-api/ 
  */

  /**
   * REST API
   * Link@ https://codex.wordpress.org/AJAX_in_Plugins
   * link@ https://developer.wordpress.org/rest-api/
   * What Is A REST API? #What Is A REST API?
   * An API is an Application Programming Interface. REST, standing for “REpresentational State Transfer,” is a set of concepts for modeling and accessing your application’s data as interrelated objects and collections.
   * 
   * Using the WordPress REST API
   * if you do wish to write your theme, plugin, or external application as a client-side JavaScript application, or a standalone program in a language other than PHP, then your application will need a structured way to access content within your WordPress site. Any programming language which can make HTTP requests and interpret JSON can use the REST API to interact with WordPress, from PHP, Node.js, Go, and Java, to Swift, Kotlin, and beyond.
   * 
   * 
   * 
   * It is easy to used get api or post api inside wordpress directory or same folder
   * But it is difficult to authonticate for post request, if the user is global or outsite of wordpress plugin
   * So, for authentication we  need authenticate for some plugin
   * link@ https://developer.wordpress.org/rest-api/using-the-rest-api/authentication/#authentication-plugins
   * 
   */

   /**
    * add custom filters hoook as a practice purpose only 
    * link@ https://codex.wordpress.org/Plugin_API
    */

    add_filter('example_filter_10046', 'test_10046');
    function test_10046($data)
    {
        return 'additional test via filter  '.$data;
    }
?>