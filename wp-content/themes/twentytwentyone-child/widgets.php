<?php


/**
 * The widget API link
 * link@ https://codex.wordpress.org/Widgets_API
 * other@ https://www.wpbeginner.com/wp-tutorials/how-to-create-a-custom-wordpress-widget/
 */
class My_Widget extends WP_Widget {

    function __construct() {
 
        parent::__construct(
            'lara_widgets',  // Base ID
            'Lara Widgets'   // Name
        );
 
        add_action( 'widgets_init', function() {
            register_widget( 'My_Widget' ); //this my 
        });

        add_action('widgets_init', [$this, 'lara_widgets_sidebar']);
 
    }
    public function lara_widgets_sidebar()
    {
        /**
         * sidebar is a widgetize area that means in which display widgets
         * Link@ https://developer.wordpress.org/themes/functionality/sidebars/
         */
        register_sidebar(
            array(
                'id'            => 'lara_widget_area',
                'name'          => __( 'Lara Sidebar' ),
                'description'   => __( 'A short description of the sidebar.' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  =>'</div>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   =>'</h3>',
            )
        );
        /* Repeat register_sidebar() code for additional sidebars. */
    }

    public function widget($args, $instance ) {
        extract($args);
        /**
         * args come from the register_sidebar option. otherwise show default
         */
        echo $before_widget;
        if ( ! empty( $instance['title'] ) ) {
            echo $before_title . apply_filters( 'widget_title', $instance['title'] ) . $after_title;
        }
 
        echo '<div class="textwidget">';
 
        echo esc_html__( $instance['text'], 'twentytwentyone-child' );
 
        echo '</div>';
 
        echo $after_widget;
    }
 
    public function form( $instance ) {
 
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'twentytwentyone-child' );
        $text = ! empty( $instance['text'] ) ? $instance['text'] : esc_html__( '', 'twentytwentyone-child' );
        ?>
        <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php echo esc_html__( 'Title:', 'twentytwentyone-child' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'Text' ) ); ?>"><?php echo esc_html__( 'Text:', 'twentytwentyone-child' ); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" type="text" cols="30" rows="10"><?php echo esc_attr( $text ); ?></textarea>
        </p>
        <?php
 
    }
 
    public function update( $new_instance, $old_instance ) {
 
        $instance = array();
 
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['text'] = ( !empty( $new_instance['text'] ) ) ? $new_instance['text'] : '';
 
        return $instance;
    }
 
}
$my_widget = new My_Widget();
?>