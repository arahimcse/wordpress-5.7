<?php

/**
 * Template Name: Lara Book
 */
get_header();

/**
 * metadata api and setting api this are used wp_optins table for data query
 * So, they are related to each other. wordpress options data can accessible form anywhere by call the function get_options
 * option field can be added by two way one is add_option() and another is register_setting('page_name', 'options_name_that_save_into_the_data_base');
 * Link@ https://codex.wordpress.org/Options_API
 */
// $data = add_option('rahim10046', 'this is only for testing purpose');
$data = apply_filters('example_filter_10046', 'this filter values call   ');
echo '<pre>'; 
var_dump($data);
 $loop = get_transient('lbook');
echo '<pre>';
//print_r($loop);
if( false === ($loop))
{
    $args = array(
        'post_type'      => 'lbook',
        'posts_per_page' => 10,
    );
    $loop = new WP_Query($args);

    /**
     * transient api set a content like post or others set for particular time
     * link@ https://developer.wordpress.org/apis/handbook/transients/
     * Put the results in a transient. Expire after 12 hours.
     */
    set_transient( 'lbook', $loop, 12*60*60 );
}

while ( $loop->have_posts() ) {
    $loop->the_post();
    ?>
    <div class="container">
        <?php the_title('<h3><a href="'. get_permalink() .'">', '</a></h3>'); ?>
        <?php the_content(); ?>
    </div>
    <?php
}

get_footer();
?>