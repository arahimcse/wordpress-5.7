<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gridchamp
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'gridchamp' ); ?></a>
	<header id="masthead" class="site-header">

		<?php 
		if(gridchamp_set_to_premium()){
			do_action('gridchamp_top_notification_bar','gridchamp');
		}
			do_action('gridchamp_header_section', 'gridchamp');
			do_action('gridchamp_header_after', 'gridchamp');
		?>
	</header><!-- #masthead -->
</div>

<?php 
$get_allpage_status = get_theme_mod('banner_status_allpage');
if($get_allpage_status){
	get_template_part( 'template-parts/partials/slider/banner'); 
}elseif ( is_front_page() || is_home() ){
	get_template_part( 'template-parts/partials/slider/banner'); 
}
