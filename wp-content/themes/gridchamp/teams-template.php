<?php
/**
 *  Template Name: Team List Template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gridchamp
 */


get_header(); ?>
<div class="walker-wraper team-wraper">
<div class="walker-container team-list text-center">
<?php $gridchamp_query = new WP_Query( array( 'post_type' => 'wc_teams', 'order'=> 'DESC', 'posts_per_page' => -1) );
    while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();?>

   <div class="walkerwp-grid-4">
    	<div class="walkerwp-teams-box">
	  		<?php 
	    	if ( has_post_thumbnail() ) {?>
				<div class="team-image"><?php the_post_thumbnail(); ?></div>
			<?php	} ?>
			
			<div class="content-part">
				<h3 class="team-name"><?php  the_title(); ?></h3>
				<?php echo '<h5 class="team-position">'. ecs_html(get_post_meta( $post->ID, 'walker_team_position', true )) .'</h5>';?>
				<div class="team-social-media"><?php 
				$member_facebook_link = get_post_meta( $post->ID, 'walker_team_facebook', true );
				if($member_facebook_link){
				 	echo '<a href="' . esc_url($member_facebook_link) . '" target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i></a>';
				}
				$member_twitter_link = get_post_meta( $post->ID, 'walker_team_twitter', true );
				if($member_twitter_link){
				 	echo '<a href="' . esc_url($member_twitter_link) . '" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>';
				}
				$member_twitter_instagram = get_post_meta( $post->ID, 'walker_team_instagram', true );
				if($member_twitter_instagram){
				 	echo '<a href="' . esc_url($member_twitter_instagram) . '" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>';
				}
				$member_twitter_linkedin = get_post_meta( $post->ID, 'walker_team_linkedin', true );
				if($member_twitter_linkedin){
				 	echo '<a href="' . esc_url($member_twitter_linkedin) . '" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>';
				}
				$member_twitter_github = get_post_meta( $post->ID, 'walker_team_github', true );
				if($member_twitter_github){
				 	echo '<a href="' . esc_url($member_twitter_github) . '" target="_blank"><i class="fa fa-github" aria-hidden="true"></i></a>';
				}



				?>
					
				</div>
				<span class="team-desc"><?php echo gridchamp_excerpt('20');?></span>
				<?php if(get_theme_mod('team_view_more_text')){
					$team_more_text= get_theme_mod('team_view_more_text');
				}else{
					$team_more_text= __('View More','gridchamp');
				}?>
				<a href="<?php the_permalink();?>" class="gridchamp-primary-button style-extend team-more"><?php echo esc_html($team_more_text); ?></a>
		</div>
	</div>
	</div>
	
<?php endwhile; 
wp_reset_postdata(); ?>
</div>
</div>

<?php get_footer();