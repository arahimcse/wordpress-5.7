jQuery(document).ready(function () {
  var tSwiper = new Swiper(".gridchamp-testimonial", {
    spaceBetween: 30,
    slidesPerView:1,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
  });
  var testimonialSwiper = new Swiper(".gridchamp-testimonial-1", {
    spaceBetween: 30,
    slidesPerView:1,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
  });

   var ttSwiper = new Swiper(".gridchamp-testimonial-layout-2", {
    spaceBetween: 30,
    slidesPerView:1,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        breakpoints: {
        480: {
          slidesPerView: 1,
        },
        768: {
          slidesPerView: 1,
        },
        1024: {
          slidesPerView: 2,
        },
        1180: {
          slidesPerView: 2,
        },
      }
  });
  var tthSwiper = new Swiper(".gridchamp-testimonial-layout-3", {
    spaceBetween: 30,
    slidesPerView:1,
    centeredSlides: true,
    roundLengths: true,
    loop: true,
    loopAdditionalSlides: 30,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        breakpoints: {
        480: {
          slidesPerView: 1,
        },
        768: {
          slidesPerView: 2,
        },
        1024: {
          slidesPerView: 3,
        },
        1180: {
          slidesPerView: 3,
        },
      }
  });

  var portfolioSwiper = new Swiper(".gridchamp-portfolio-slider", {
    spaceBetween: 0,
    slidesPerView:1,
    // centeredSlides: true,
    // roundLengths: true,
    freeMode: false,
    loop: true,
    loopAdditionalSlides: 30,
        pagination: {
          el: ".folio-pagination",
          clickable: true,
        },
        navigation: {
          nextEl: '.gridchamp-folio-next',
          prevEl: '.gridchamp-folio-prev',
          clickable: true,
      },
      breakpoints: {
        560: {
          slidesPerView: 2,
        },
        768: {
          slidesPerView: 2,
        },
        1024: {
          slidesPerView: 3,
        },
        1180: {
          slidesPerView: 4,
        },
      }
  });

  var swiper_brands = new Swiper(".gridchamp-brands", {
    spaceBetween: 0,
    slidesPerView:1,
        pagination: {
          el: ".brands-pagination",
          clickable: true,
        },
        breakpoints: {
        400: {
          slidesPerView: 2,
        },
        768: {
          slidesPerView: 2,
        },
        1024: {
          slidesPerView: 3,
        },
        1180: {
          slidesPerView: 4,
        },
      }
  });
  var swipes_slider1 = new Swiper('.gridchamp-slider-one', {
      loop: true,
      parallax: true,
      speed: 600,
      // effect: 'fade',
      navigation: {
          nextEl: '.gridchamp-slider-next',
          prevEl: '.gridchamp-slider-prev',
          clickable: true,
      },
      autoplay: {
          delay: 5000,
          disableOnInteraction: false,
      },
      pagination: {
          el: '.gridchamp-slider-pagination',
          clickable: true,
      },
  });
  var swipes_slider2 = new Swiper('.gridchamp-slider-two', {
      loop: true,
      speed: 600,
      direction: 'vertical',
      parallax: true,
      navigation: {
          nextEl: '.gridchamp-slider-next',
          prevEl: '.gridchamp-slider-prev',
          clickable: true,
      },
      pagination: {
          el: '.gridchamp-slider-pagination',
          clickable: true,
      },
      autoplay: {
          delay: 5000,
          disableOnInteraction: false,
      },
  });

  var swipes_slider3 = new Swiper('.gridchamp-slider-three', {
      loop: true,
      speed: 600,
      direction: 'vertical',
      parallax: true,
      navigation: {
          nextEl: '.gridchamp-slider-next',
          prevEl: '.gridchamp-slider-prev',
          clickable: true,
      },
      pagination: {
          el: '.gridchamp-slider-pagination',
          clickable: true,
      },
      autoplay: {
          delay: 5000,
          disableOnInteraction: false,
      },
  });

    jQuery('.gridchamp-count').each(function () {
    jQuery(this).prop('gridchamp-count',0).animate({
        Counter: jQuery(this).text()
    }, {
        duration: 1500,
        easing: 'swing',
        step: function (now) {
            jQuery(this).text(Math.ceil(now));
        }
    });
});

var action="click";
var speed="500";

jQuery(document).ready(function() {
    jQuery('li.question').on(action, function() {
        jQuery('li.question').removeClass('active');
        jQuery(this).addClass('active');
        jQuery(this).next().slideToggle(speed).siblings('li.answer').slideUp();

    });
    if(!jQuery('li.question').hasClass('active')){
        jQuery('li.question:first-child').addClass('active');
    }
});

  jQuery('.search-toggle').on('click', function(e) {
    e.preventDefault();
    jQuery('body').addClass('modal-active');
    jQuery('.search-modal').fadeIn('fast');
  });
  jQuery('.modal-close').on('click', function(e){
     e.preventDefault();
     jQuery('body').removeClass('modal-active');
    jQuery('.search-modal').fadeOut('fast');
  });
  jQuery('#searchModel button.btn-default').on('keydown', function(e){
    jQuery('#searchModel button.modal-close').focus();
    e.preventDefault();
  });

  jQuery(window).scroll(function(){ 
        if (jQuery(this).scrollTop() > 100) { 
            jQuery('a.gridchamp-top').fadeIn(); 
        } else { 
            jQuery('a.gridchamp-top').fadeOut(); 
        } 
    }); 
    jQuery('a.gridchamp-top').click(function(){ 
        jQuery("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 


    jQuery(".notice-toggle").click(function(e){
      jQuery(this).toggleClass('hide');
      jQuery('.top-notification-bar').slideToggle();
      e.preventDefault();
    });


    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() >= 50) {
            jQuery('.header-layout-1').addClass('active-sticky');
            jQuery('.header-layout-2').addClass('active-sticky');
            jQuery('.header-layout-3-navigation').addClass('active-sticky');
            jQuery('.header-layout-4-navigation').addClass('active-sticky');
            jQuery('.header-layout-5-navigation').addClass('active-sticky');
            
        }
        else {
            jQuery('.header-layout-1').removeClass('active-sticky');
            jQuery('.header-layout-2').removeClass('active-sticky');
            jQuery('.header-layout-3-navigation').removeClass('active-sticky');
            jQuery('.header-layout-4-navigation').removeClass('active-sticky');
            jQuery('.header-layout-5-navigation').removeClass('active-sticky');
        }
    });
 
/*GRIDCHAP MOBILE TOGLLE MENU HANDLE*/
var menuFocus, navToggleItem, focusBackward;
var menuToggle = document.querySelector('.menu-toggle');
var navMenu = document.querySelector('.nav-menu');
var navMenuLinks = navMenu.getElementsByTagName('a');
var navMenuListItems = navMenu.querySelectorAll('li');
var nav_lastIndex = navMenuListItems.length - 1;
var navLastParent = document.querySelectorAll('.main-navigation > ul > li').length - 1;

document.addEventListener('menu_focusin', function () {
    menuFocus = document.activeElement;
    if (navToggleItem && menuFocus !== navMenuLinks[0]) {
        document.querySelectorAll('.main-navigation > ul > li')[navLastParent].querySelector('a').focus();
    }
    if (menuFocus === menuToggle) {
        navToggleItem = true;
    } else {
        navToggleItem = false;
    }
}, true);


document.addEventListener('keydown', function (e) {
    if (e.shiftKey && e.keyCode == 9) {
        focusBackward = true;
    } else {
        focusBackward = false;
    }
});


for (el of navMenuLinks) {
    el.addEventListener('blur', function (e) {
        if (!focusBackward) {
            if (e.target === navMenuLinks[nav_lastIndex]) {
                menuToggle.focus();
            }
        }
    });
}
menuToggle.addEventListener('blur', function (e) {
    if (focusBackward) {
        navMenuLinks[nav_lastIndex].focus();
    }
});


});
