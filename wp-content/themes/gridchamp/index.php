<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Gridchamp
 */

get_header();
?>
<?php
	if(get_theme_mod('blog_sidebar_layout')){
		$gridchamp_blog_layout = get_theme_mod('blog_sidebar_layout');
	} else{
		$gridchamp_blog_layout ='right-sidebar';
	}

	if($gridchamp_blog_layout =='no-sidebar'){
		$gridchamp_content_class= 'walkerwp-grid-12';
	} else{
		$gridchamp_content_class= 'walkerwp-grid-9';
	}
	if($gridchamp_blog_layout =='right-sidebar'){
		$content_sub_class='right-sidebar-layout';
	} elseif($gridchamp_blog_layout =='left-sidebar'){
		$content_sub_class='left-sidebar-layout';
	} else{
		$content_sub_class='full-width-content';
	}
	if(gridchamp_set_to_premium()){
		if(get_theme_mod('blog_post_view')=='list-layout'){
			$gridchamp_blog_view= 'post-list-layout';
		}elseif(get_theme_mod('blog_post_view')=='full-layout'){
			$gridchamp_blog_view= 'post-full-layout';
		}else{
			$gridchamp_blog_view= 'post-grid-layout';
		}
	}else{
		$gridchamp_blog_view= 'post-grid-layout';
	}
?>
<div class="walker-wraper">
	<div class="walker-container">
		<?php if($gridchamp_blog_layout=='left-sidebar'){?>
			<div class="walkerwp-grid-3 sidebar-block"><?php get_sidebar(); ?></div>
		<?php } ?>
		<main id="primary" class="site-main <?php echo esc_attr($gridchamp_content_class); echo ' '. esc_attr($content_sub_class); ?>">

			<?php
			
			if ( have_posts() ) :?>
				<div class="<?php echo esc_attr($gridchamp_blog_view); ?>">
				<?php /* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/*
					 * Include the Post-Type-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;
				echo '</div>';

				gridchamp_post_pagination();
				// gridchamp_pagination();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>

		</main><!-- #main -->
		<?php if($gridchamp_blog_layout=='right-sidebar'){?>
			<div class="walkerwp-grid-3 sidebar-block"><?php get_sidebar(); ?></div>
		<?php } ?>
	</div>
</div>
<?php get_footer();
