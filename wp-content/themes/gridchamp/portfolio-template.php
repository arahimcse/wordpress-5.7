<?php
/**
 *  Template Name: Portfolio List Template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gridchamp
 */


get_header(); ?>
<div class="walker-wraper portfolio-wraper">
<div class="portfolio-list walker-container masonry">
<?php $gridchamp_query = new WP_Query( array( 'post_type' => 'wc_portfolio', 'order'=> 'DESC', 'posts_per_page' => -1) );
    while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();?>
  
    	<div class="walkerwp-portfolio-box item">
	  		<a href="<?php the_permalink();?>"><?php 
	    	if ( has_post_thumbnail() ) {?>
				<div class="portfolio-image"><?php the_post_thumbnail(); ?></div>
			<?php	} ?>
			
			<div class="content-part">
				<h3 class="portfolio-title"><?php  the_title(); ?></h3>
				
		</div>
	</a>
	</div>
<?php endwhile; 
wp_reset_postdata(); ?>
</div>
</div>

<?php get_footer();