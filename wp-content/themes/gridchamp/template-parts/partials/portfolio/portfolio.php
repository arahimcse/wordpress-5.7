<?php if(get_theme_mod('portfolio_status')){?>
<div class="walker-wraper portfolio-wraper">
	<div class="walker-container  text-center">
		<div class="walkerwp-grid-12">
			<?php if(get_theme_mod('portfolio_heading_text')){?>
				<h1 class="section-heading text-center"><?php echo esc_html(get_theme_mod('portfolio_heading_text'));?></h1>
			<?php }
			if(get_theme_mod('portfolio_desc_text')){?>
				<p class="section-subheader"><?php echo esc_html(get_theme_mod('portfolio_desc_text'));?></p>
			<?php } ?>
			<?php 
				if(get_theme_mod('portfolio_btn_text')){
					if(get_theme_mod('portfolio_btn_url')){
						$more_portfolio_link = get_theme_mod('portfolio_btn_url');
					}else{
						$more_portfolio_link='#';
					}

					if(get_theme_mod('portfolio_btn_target')){
						$more_portfolio_link_target='_blank';
					}else{
						$more_portfolio_link_target='_self';
					}
					?>
					<a href="<?php echo esc_url($more_portfolio_link);?>" class="more-features gridchamp-primary-button" target="<?php echo esc_attr($more_portfolio_link_target);?>">
						<?php echo esc_html(get_theme_mod('portfolio_btn_text'));?>
						</a>
				<?php }
				?>
		</div>
	</div>
<div class="portfolio-list walker-container masonry">
<?php 
	if(get_theme_mod('portfolio_total_items')){
		$total_items_to_show = get_theme_mod('portfolio_total_items');
	}else{
		$total_items_to_show=6;
	}
$gridchamp_query = new WP_Query( array( 'post_type' => 'wcr_portfolio', 'order'=> 'DESC', 'posts_per_page' => $total_items_to_show) );
    while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();?>
  
    	<div class="walkerwp-portfolio-box item">
	  		<a href="<?php the_permalink();?>"><?php 
	    	if ( has_post_thumbnail() ) {?>
				<div class="portfolio-image"><?php the_post_thumbnail(); ?></div>
			<?php	} ?>
			
			<div class="content-part">
				<h3 class="portfolio-title"><?php  the_title(); ?></h3>
				
		</div>
	</a>
	</div>
<?php endwhile; 
wp_reset_postdata(); ?>
</div>
</div>
<?php } ?>