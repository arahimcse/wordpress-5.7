<?php
$gridchamp_testimonial_status = get_theme_mod('testimonial_status');
if($gridchamp_testimonial_status){
	$tesitimonial_cat = get_theme_mod('gridchamp_testimonial_category');?>
	<div class="walker-wraper testimonial-wraper">
		<div class="walker-container text-center">
			<div class="walkerwp-grid-12">
			<?php 
			if(get_theme_mod('testimonial_heading_text') ){
				echo '<h1 class="section-heading text-center">'.esc_html(get_theme_mod('testimonial_heading_text')).'</h1>';
			}
			if(get_theme_mod('testimonial_desc_text') ){
				echo '<p class="section-subheader">'.esc_html(get_theme_mod('testimonial_desc_text')).'</p>';
			}?>
		</div>
		</div>
		<div class="walker-container testimonial-container">
			<div class="swiper-container gridchamp-testimonial">
      			<div class="swiper-wrapper">
					<?php 
					if(get_theme_mod('testimonials_total_items')){
						$total_testimonials_to_show = get_theme_mod('testimonials_total_items');
					}else{
						$total_testimonials_to_show=4;
					}
					$gridchamp_query = new WP_Query( array( 'post_type' => 'post', 'order'=> 'DESC', 'posts_per_page' => $total_testimonials_to_show, 'category_name' => $tesitimonial_cat) );
						    while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();?>
						    <div class="swiper-slide">
						    	<div class="walkerwp-testimonial-box">
							  		<?php 
							    	if ( has_post_thumbnail() ) {?>
										<div class="testimonial-thumbnail"><?php the_post_thumbnail(); ?></div>
									<?php	} ?>
									
									<div class="review-part">
										
										<p class="review-message"><?php echo gridchamp_excerpt('300');?></p>
										<h4 class="reviewer-name"><?php  the_title(); ?></h4>						
									</div>
								</div>
							</div>
						<?php endwhile; 
						wp_reset_postdata(); ?>
					</div>
					<div class="swiper-pagination testimonial-nav"></div>
				</div>
			</div>
	</div>
<?php }
?>