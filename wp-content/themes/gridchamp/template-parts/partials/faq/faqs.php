<?php
if(gridchamp_set_to_premium() && get_theme_mod('faq_status')){
    $current_faq_layout = get_theme_mod('walker_faq_layout');
    if($current_faq_layout =='faq-layout-2'){?>
        <div class="walker-wraper faqs-wraper">
            <div class="walker-container">
                <div class="walkerwp-grid-5">
                    <?php 
                        if(get_theme_mod('faqs_heading_text')){
                            echo '<h1 class="section-heading">'.esc_html(get_theme_mod('faqs_heading_text')).'</h1>';
                        }
                        if(get_theme_mod('faq_desc_text') ){
                            echo '<p class="section-subheader">'.esc_html(get_theme_mod('faq_desc_text')).'</p>';
                        }?>
                </div>
                <div class="walkerwp-grid-7">
                    <div class="gridchamp-faqs">
                        <ul class="faq">
                            <?php $gridchamp_query = new WP_Query( array( 'post_type' => 'wcr_faqs', 'order'=> 'DESC', 'posts_per_page' =>-1) );
                                while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();?>
                                    <li class="question"><?php the_title();?></li>
                                    <li class="answer"><?php the_content();?></li>
                            <?php endwhile; 
                             wp_reset_postdata(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php }else{?>
        <div class="walker-wraper faqs-wraper">
            <div class="walker-container gridchamp-faqs ">
                <div class="walkerwp-grid-12">
                    <?php 
                        if(get_theme_mod('faqs_heading_text') ){
                            echo '<h1 class="section-heading text-center">'.esc_html(get_theme_mod('faqs_heading_text')).'</h1>';
                        }
                        if(get_theme_mod('faq_desc_text') ){
                            echo '<p class="section-subheader text-center">'.esc_html(get_theme_mod('faq_desc_text')).'</p>';
                        }?>
                </div>
            </div>
            <div class="walker-container">
                <div class="walkerwp-grid-12">
                    <div class="gridchamp-faqs">
                        <ul class="faq">
                            <?php $gridchamp_query = new WP_Query( array( 'post_type' => 'wcr_faqs', 'order'=> 'DESC', 'posts_per_page' =>-1) );
                                while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();?>
                                    <li class="question"><?php the_title();?></li>
                                    <li class="answer"><?php the_content();?></li>
                            <?php endwhile; 
                             wp_reset_postdata(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    
<?php  }
 }?>


