<?php
$gridchamp_cta_status = get_theme_mod('gridchamp_cta_status');
if($gridchamp_cta_status){?>
<div class="walker-wraper cta-wraper">
	<?php
		if(get_theme_mod('cta_bg_image')){
			$cta_background_img = get_theme_mod('cta_bg_image'); ?>
			<img class="cta-overlay-image" src="<?php echo esc_url($cta_background_img);?>" />
		<?php } ?>
	<?php if(get_theme_mod('cta_message_text')){?>
	<div class="walker-container">
		<div class="cta-box text-center">
			<h1 class="section-heading cta-heading"><?php echo esc_html(get_theme_mod('cta_message_text')); ?></h1>
			<?php
				if(get_theme_mod('cta_btn_url')){
					$cta_btn_link = get_theme_mod('cta_btn_url');
				}else{
					$cta_btn_link ='#';
				}
				if(get_theme_mod('cta_btn_target')){
					$cta_link_target='_blank';
				}else{
					$cta_link_target='_self';
				}
				if(get_theme_mod('cta_btn_url')){?>
					<a href="<?php echo esc_url($cta_btn_link);?>" class="gridchamp-secondary-button" target="<?php echo esc_attr($cta_link_target);?>"><?php echo esc_html(get_theme_mod('cta_btn_text')); ?>
				</a>
				<?php }
			?>
		</div>
	</div>
<?php } ?>
</div>
<?php } ?>