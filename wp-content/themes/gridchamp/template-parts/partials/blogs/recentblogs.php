<?php
$gridchamp_recentblog_status = esc_attr(get_theme_mod('recent_post_status'));
if($gridchamp_recentblog_status){?>
<div class="walker-wraper recentblog-wraper">
	<div class="walker-container">
		<div class="walkerwp-grid-8">
			<?php 
			if(get_theme_mod('recentpost_heading_text') ){
				echo '<h2 class="section-heading">'.esc_html(get_theme_mod('recentpost_heading_text')).'</h2>';
			}
			if(get_theme_mod('recentpost_desc_text') ){
				echo '<p class="section-subheader">'.esc_html(get_theme_mod('recentpost_desc_text')).'</p>';
			}?>
		</div>
		<div class="walkerwp-grid-4 button-col">
			<?php if(get_theme_mod('recentpost_viewall_text_link')){?>
				<a href="<?php echo esc_html(get_theme_mod('recentpost_viewall_text_link'));?>" class="gridchamp-primary-button"><?php echo esc_html(get_theme_mod('recentpost_viewall_text'));?></a>
			<?php }?>
			
		</div>
	</div>
	<div class="walker-container post-list">
		<?php $recent_post_type = esc_attr(get_theme_mod('recent_blog_home'));
			if($recent_post_type=='latest-post'){
				$sticky = get_option( 'sticky_posts' );
				$gridchamp_query = new WP_Query( array( 'post_type' => 'post', 'order'=> 'DESC', 'posts_per_page' => 3, 
					'ignore_sticky_posts' => 1,'post__not_in' => $sticky) );
					    while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();?>
					    <div class="walkerwp-grid-4">
					    	<div class="walkerwp-recentpost-box">
						  	<?php 
						    	if ( has_post_thumbnail() ) {?>
									<a href="<?php the_permalink();?>" class="home-post-thumbnails"><?php the_post_thumbnail();?></a>
								<?php } ?>
								<?php if(!has_post_thumbnail()){
									$content_part_class="without-thumbnail";
								} else{
									$content_part_class="without-thumbnail";
								}?>
								<div class="content-part <?php echo esc_attr($content_part_class);?>">
									<h3><a href="<?php echo the_permalink();?>"><?php  the_title(); ?></a></h3>	
									<?php
									if(gridchamp_set_to_premium()){
										if(get_theme_mod('author_status','true')){
											gridchamp_posted_by();
										}
										if(get_theme_mod('post_date_status','true')){
											gridchamp_posted_on();
										}
									}else{
										gridchamp_posted_by();
										gridchamp_posted_on();
									}?>
									<p><?php echo esc_html(gridchamp_excerpt( 25 )); ?></p>
									<a href="<?php the_permalink();?>" class="gridchamp-primary-button details-service style-extend"> <?php echo esc_html(get_theme_mod('recentpost_readmore_text'));?></a>
									
								</div>
							
							</div>
						</div>
						<?php endwhile; 
				wp_reset_postdata(); 
			} else{
				$recent_post_cat = esc_attr(get_theme_mod('gridchamp_recent_category'));
				$sticky = get_option( 'sticky_posts' );
				$gridchamp_query = new WP_Query( array( 'post_type' => 'post', 'order'=> 'DESC', 'posts_per_page' => 3, 'category_name' => $recent_post_cat,
					'ignore_sticky_posts' => 1,'post__not_in' => $sticky) );
					    while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();?>
					    <div class="walkerwp-grid-4">
					    	<div class="walkerwp-recentpost-box">
						  	<?php 
						    	if ( has_post_thumbnail() ) {?>
									<a href="<?php the_permalink();?>" class="home-post-thumbnails"><?php the_post_thumbnail();?></a>
								<?php } ?>
								<?php if(!has_post_thumbnail()){
									$content_part_class="without-thumbnail";
								} else{
									$content_part_class="without-thumbnail";
								}?>
								<div class="content-part <?php echo esc_attr($content_part_class);?>">
									<h3><a href="<?php echo the_permalink();?>"><?php  the_title(); ?></a></h3>	
									<?php gridchamp_posted_by();?><?php  gridchamp_posted_on(); ?>
									<p><?php echo esc_html(gridchamp_excerpt( 25 )); ?></p>
									<a href="<?php the_permalink();?>" class="gridchamp-primary-button details-service style-extend"> <?php echo esc_html(get_theme_mod('recentpost_readmore_text'));?></a>
									
								</div>
								
							
							</div>
						</div>
						<?php endwhile; 
				wp_reset_postdata(); 
			}
		?>	
	</div>
</div>
<?php } ?>