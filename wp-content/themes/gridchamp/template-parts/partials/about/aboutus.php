<?php  $gridchamp_about_status = esc_attr(get_theme_mod('about_status'));
if($gridchamp_about_status){?>
	<?php
		$gridchamp_about_page = get_theme_mod( 'about_page');
		if(!empty($gridchamp_about_page) && $gridchamp_about_page != 'None' ){?>
			<div class="walker-wraper about-wraper">
				<div class="walker-container about-container">
			
				<?php
				$about_page_id = get_page_by_title( $gridchamp_about_page, OBJECT, 'page'  );
				$current_pageid = $about_page_id->ID;
				$gridchamp_query = new WP_Query( 'page_id='.$current_pageid);
				if ( $gridchamp_query->have_posts() ) :
				while ( $gridchamp_query->have_posts() ) : $gridchamp_query->the_post();
					if ( has_post_thumbnail() || get_theme_mod('about_video_status')){
						$about_content_class='walkerwp-grid-6';
					}else{
						$about_content_class='walkerwp-grid-12';
					} ?>
							<div class="text-col <?php echo esc_attr($about_content_class);?>">
								<div class="gridchamp-about-box">
									<h5 class="about-title"><?php the_title();?></h5>
									<h1 class="section-heading"><?php echo esc_html(get_theme_mod('about_heading_text'));?></h1>
									<p class="about-description"><?php

									 echo gridchamp_excerpt('30');
									 ?></p>
									<?php if(get_theme_mod('about_readmore_text') || get_theme_mod('about_readmore_text')!=''){?>
										<a href="<?php echo the_permalink();?>" class="gridchamp-primary-button details-about"> <?php echo esc_html(get_theme_mod('about_readmore_text'));?></a>
									<?php } else{?>
										<a href="<?php echo the_permalink();?>" class="details-about gridchamp-secondary-button"> <?php echo __('Read More', 'gridchamp');?></a>
									<?php }?>
								</div>
							</div>
							<?php 
							if(gridchamp_set_to_premium()){
								if ( has_post_thumbnail() || get_theme_mod('about_video_status') ){?>
								<div class="walkerwp-grid-6 img-col">
									<?php
									$video_id = esc_html(get_theme_mod('about_video_url'));
									if(get_theme_mod('about_video_status')){?>
										<iframe width="700" height="455" src="https://www.youtube.com/embed/<?php echo $video_id; ?>" title="YouTube video player" frameborder="0"  allowfullscreen></iframe>
									<?php } else{
										if ( has_post_thumbnail() ) {
									   	 	the_post_thumbnail();
										}
									}
									
									?>
								</div>
							<?php }
						}else{
							if ( has_post_thumbnail() ){?>
								<div class="walkerwp-grid-6 img-col">
									
									<?php 
									   	 	the_post_thumbnail();
										
								
									?>
								</div>
							<?php }

						}
						
				 endwhile;
			endif;?>
			</div>
				
		</div>
	<?php	}
}
?>