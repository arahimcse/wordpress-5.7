<?php  $gridchamp_about_status = esc_attr(get_theme_mod('about_status'));
if($gridchamp_about_status){
	$gridchamp_about_page = get_theme_mod( 'about_page');
		if(!empty($gridchamp_about_page) && $gridchamp_about_page != 'None' ){
		
				$about_page_id = get_page_by_title( $gridchamp_about_page, OBJECT, 'page'  );
				$current_pageid = $about_page_id->ID;
				$gridchamp_query = new WP_Query( 'page_id='.$current_pageid);
					
					if ( $gridchamp_query->have_posts() ) :
					while ( $gridchamp_query->have_posts() ) : $gridchamp_query->the_post(); 
						if ( has_post_thumbnail() || get_theme_mod('about_video_status')){
						$about_content_class='walkerwp-grid-4';
					}else{
						$about_content_class='walkerwp-grid-12';
					} ?>
					<div class="walker-wraper about-wraper about-layout-2">
					
						<div class="walker-container">
							<div class="walkerwp-grid-12">
								<h5 class="about-title"><?php the_title();?></h5>
								<h1 class="section-heading"><?php echo esc_html(get_theme_mod('about_heading_text'));?></h1>
							</div>
						</div>
						<div class="walker-container about-container">
							<div class="<?php echo esc_attr($about_content_class);?> text-col">
								<div class="gridchamp-about-box">
									<p class="about-description"><?php  echo gridchamp_excerpt('70'); ?></p>
									<?php if(get_theme_mod('about_readmore_text') || get_theme_mod('about_readmore_text')!=''){?>
										<a href="<?php echo the_permalink();?>" class="gridchamp-secondary-button style-outline-light details-about"> <?php echo esc_html(get_theme_mod('about_readmore_text'));?></a>
									<?php } else{?>
										<a href="<?php echo the_permalink();?>" class="details-about gridchamp-secondary-button style-outline-light"> <?php echo __('Read More', 'gridchamp');?></a>
									<?php }?>
								</div>
							</div>
							<?php if ( has_post_thumbnail() || get_theme_mod('about_video_status')){?>
							<div class="walkerwp-grid-8 img-col">
								<div class="about-feature-box">
								<?php
									$video_id = esc_html(get_theme_mod('about_video_url'));
									if(get_theme_mod('about_video_status')){?>
									<iframe width="760" height="515" src="https://www.youtube.com/embed/<?php echo $video_id; ?>" title="YouTube video player" frameborder="0"  allowfullscreen></iframe>
									<?php } else{
									if ( has_post_thumbnail() ) {
								   	 	the_post_thumbnail();
									}
								}?>
								</div>
							</div>
						<?php } ?>
						</div>
						
				</div>
				<?php endwhile;
			endif;
		}
			?>
			<div class="walker-wraper counter-wraper">
				<div class="walker-container">
					<div class="walkerwp-grid-12">
						<div class="walkerwp-grid-2">
							<?php if(get_theme_mod('walker_counter_number')){?>
								<h1><div class="gridchamp-count"><?php echo esc_html(get_theme_mod('walker_counter_number'));?></div>+</h1>
							<?php }?>
							<?php if(get_theme_mod('walker_counter_text')){?>
								<h5><?php echo esc_html(get_theme_mod('walker_counter_text'));?></h5>
							<?php }?>
							
						</div>
						<div class="walkerwp-grid-2">
							<?php if(get_theme_mod('walker_counter_number_2')){?>
								<h1><div class="gridchamp-count"><?php echo esc_html(get_theme_mod('walker_counter_number_2'));?></div>+</h1>
							<?php } ?>
							<?php if(get_theme_mod('walker_counter_text_2')){?>
								<h5><?php echo esc_html(get_theme_mod('walker_counter_text_2'));?></h5>
							<?php }?>
						</div>
						<div class="walkerwp-grid-2">
							<?php if(get_theme_mod('walker_counter_number_3')){?>
								<h1><div class="gridchamp-count"><?php echo esc_html(get_theme_mod('walker_counter_number_3'));?></div>+</h1>
							<?php } ?>
							<?php if(get_theme_mod('walker_counter_text_3')){?>
								<h5><?php echo esc_html(get_theme_mod('walker_counter_text_3'));?></h5>
							<?php }?>
						</div>
					</div>
				</div>
			</div>

<?php } 
?>