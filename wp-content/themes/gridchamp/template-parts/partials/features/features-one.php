<?php
if(gridchamp_set_to_premium()){
$gridchamp_feature_status = get_theme_mod('features_status');
if($gridchamp_feature_status){
	 if(get_theme_mod('walker_features_text_align')=='feature-text-align-center'){
			$feature_text_align='text-center';
		}else{
			$feature_text_align='';
	}?>
	<div class="walker-wraper features-wraper">
		<div class="walker-container">
			<div class="walkerwp-grid-12 <?php echo esc_attr($feature_text_align);?>">
				<?php 
				if(get_theme_mod('features_heading_text') ){
					echo '<h1 class="section-heading ' . esc_attr($feature_text_align).'">'.esc_html(get_theme_mod('features_heading_text')).'</h1>';
				}
				if(get_theme_mod('feature_desc_text') ){
					echo '<p class="section-subheader">'.esc_html(get_theme_mod('feature_desc_text')).'</p>';
				}?>
			</div>
			
		</div>
		<div class="walker-container">
			<div class="walkerwp-grid-12 features-list">
			<?php
			$gridchamp_feature_parent_page= get_theme_mod('gridchamp_feature_page');
				if(!empty($gridchamp_feature_parent_page) && $gridchamp_feature_parent_page != 'None' ){
					if(gridchamp_set_to_premium()){
						$gridchamp_feature_items = -1;
					}else{
						$gridchamp_feature_items = 3;
					}
					$feature_page_id = get_page_by_title( $gridchamp_feature_parent_page );
					$args = array(
						'posts_per_page' => $gridchamp_feature_items,
						'post_type' => 'page',
						'post_parent' => $feature_page_id->ID
					);

					$gridchamp_query = new WP_Query( $args );
					if ( $gridchamp_query->have_posts() ) :
					
					while ( $gridchamp_query->have_posts() ) : $gridchamp_query->the_post(); ?>
						<div class="feature-item">
							
							<div class="gridchamp-feature-box <?php echo esc_attr($feature_text_align);?>">
								<?php if ( has_post_thumbnail() ) {?>
								    <a class="feature-thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								        <?php the_post_thumbnail(); ?>
								    </a>

								<?php 
									$feature_content_class="with-thumbnails";
								}else{
									$feature_content_class="without-thumbnails" ;
								} ?>
								<div class="feature-content <?php echo esc_attr($feature_content_class);?>">
									<h4 class="feature-title"><?php the_title();?></h4>
									<p class="feature-description"><?php echo esc_html(gridchamp_excerpt( 20 )); ?></p>
									<?php if(get_theme_mod('features_readmore_text') || get_theme_mod('features_readmore_text')!=''){?>
										<a href="<?php echo the_permalink();?>" class="gridchamp-primary-button details-feature style-extend"> <?php echo esc_html(get_theme_mod('features_readmore_text'));?></a>
									<?php } else{?>
										<a href="<?php echo the_permalink();?>" class="details-feature gridchamp-primary-button style-extend"> <?php echo __('Read More', 'gridchamp');?></a>
									<?php }?>
								</div>
							</div>
						</div>
						<?php
					endwhile;
				endif;
			}?>
		</div>
	</div>
		<div class="walker-container">
			<div class="walkerwp-grid-12 <?php echo esc_attr($feature_text_align);?>">
				<?php 
				if(get_theme_mod('features_viewall_text')){
					if(get_theme_mod('features_viewall_btn_link')){
						$more_features_link = get_theme_mod('features_viewall_btn_link');
					}else{
						$more_features_link='#';
					}
					?>
					<a href="<?php echo esc_url($more_features_link);?>" class="more-features gridchamp-primary-button">
						<?php echo esc_html(get_theme_mod('features_viewall_text'));?>
						</a>
				<?php }
				?>
				
			</div>
		</div>
	</div>
	<?php 
	wp_reset_postdata();
} }?>