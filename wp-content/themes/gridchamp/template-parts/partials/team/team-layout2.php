<?php if(get_theme_mod('team_status')){?>
<div class="walker-wraper team-wraper team-layout-2">
	<div class="overlay-img">
		<?php
		if(get_theme_mod('team_bg_image')){
			$team_background_img = get_theme_mod('team_bg_image'); ?>
			<img class="team-bg-img" src="<?php echo esc_url($team_background_img);?>" />
		<?php }
		?>
	</div>
		<div class="walker-container  text-center">
			<div class="walkerwp-grid-12 team-heading">
				<?php 
	                if(get_theme_mod('team_heading_text') ){
	                    echo '<h1 class="section-heading text-center">'.esc_html(get_theme_mod('team_heading_text')).'</h1>';
	                }
	                if(get_theme_mod('teams_desc_text') ){
	                    echo '<p class="section-subheader text-center">'.esc_html(get_theme_mod('teams_desc_text')).'</p>';
	                }?>
			</div>
	</div>
<div class="walker-container team-list text-center">
<?php if(get_theme_mod('teams_total_items')){
		$total_items_to_show = get_theme_mod('teams_total_items');
	}else{
		$total_items_to_show=4;
	}
	$gridchamp_query = new WP_Query( array( 'post_type' => 'wcr_teams', 'order'=> 'DESC', 'posts_per_page' => $total_items_to_show) );
    while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();
    	$current_total_members = $gridchamp_query->found_posts; 
    	if($current_total_members==1){
    		$team_member_class='walkerwp-grid-12';
    	}elseif($current_total_members==2){
    		$team_member_class='walkerwp-grid-6';
    	}elseif($current_total_members==3){
    		$team_member_class='walkerwp-grid-4';
    	}else{
    		$team_member_class='walkerwp-grid-3';
    	}?>

   <div class="team-member <?php echo esc_attr($team_member_class);?>">
    	<div class="walkerwp-teams-box">
	  		<?php 
	    	if ( has_post_thumbnail() ) {?>
				<div class="team-image"><?php the_post_thumbnail(); ?></div>
			<?php	} ?>
			
			<div class="content-part">
				<h3 class="team-name"><?php  the_title(); ?></h3>
				<?php echo '<h5 class="team-position">'. esc_html(get_post_meta( $post->ID, 'walker_team_position', true )) .'</h5>';?>
				<div class="team-social-media"><?php 
				$member_facebook_link = get_post_meta( $post->ID, 'walker_team_facebook', true );
				if($member_facebook_link){
				 	echo '<a href="' . esc_url($member_facebook_link) . '" target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i></a>';
				}
				$member_twitter_link = get_post_meta( $post->ID, 'walker_team_twitter', true );
				if($member_twitter_link){
				 	echo '<a href="' . esc_url($member_twitter_link) . '" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>';
				}
				$member_twitter_instagram = get_post_meta( $post->ID, 'walker_team_instagram', true );
				if($member_twitter_instagram){
				 	echo '<a href="' . esc_url($member_twitter_instagram) . '" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>';
				}
				$member_twitter_linkedin = get_post_meta( $post->ID, 'walker_team_linkedin', true );
				if($member_twitter_linkedin){
				 	echo '<a href="' . esc_url($member_twitter_linkedin) . '" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>';
				}
				$member_twitter_github = get_post_meta( $post->ID, 'walker_team_github', true );
				if($member_twitter_github){
				 	echo '<a href="' . esc_url($member_twitter_github) . '" target="_blank"><i class="fa fa-github" aria-hidden="true"></i></a>';
				}

				?>
					
				</div>
				<span class="team-desc"><?php echo gridchamp_excerpt('10');?></span>
				<?php if(get_theme_mod('team_view_more_text')){
					$team_more_text= get_theme_mod('team_view_more_text');
				}else{
					$team_more_text= __('View More','gridchamp');
				}?>
				<a href="<?php the_permalink();?>" class="gridchamp-primary-button team-more"><?php echo esc_html($team_more_text); ?></a>
		</div>
	</div>
	</div>
	
<?php endwhile; 
wp_reset_postdata(); ?>
</div>
</div>
<?php } ?>