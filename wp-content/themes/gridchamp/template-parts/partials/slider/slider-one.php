<?php
if(gridchamp_set_to_premium()){?>
<div class="walker-wraper slider-wraper no-gap">
	
		<div class="swiper-container gridchamp-slider-one">
  			<div class="swiper-wrapper">
				<?php $gridchamp_query = new WP_Query( array( 'post_type' => 'wcr_slider', 'order'=> 'DESC') );
					    while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();
					   if(get_theme_mod('walker_slder_text_align')=='slide-text-align-right'){
							$slide_text_align='text-right';
							}elseif(get_theme_mod('walker_slder_text_align')=='slide-text-align-center'){
								$slide_text_align='text-center';
							}else{
								$slide_text_align='';
							}
						?>

					    <div class="swiper-slide">
					    	<div class="walkerwp-slider-box">
						  		<?php 
						    	if ( has_post_thumbnail() ) {?>
									<div class="slide-image"><?php the_post_thumbnail(); ?></div>
								<?php	} ?>
								<div class="walker-container  <?php echo esc_attr($slide_text_align);?>">
								<div class="slide-content">
									
									<div class="slide-overlay-text <?php echo esc_attr($slide_text_align);?>">
									<h1 class="slider-title"><?php  the_title(); ?></h1>	
									<span class="slider-short-inco"><?php the_excerpt();?></span>
									<div class="button-group">
										<?php
										$primary_button_link = get_post_meta( $post->ID, 'walker_slider_primary_button_link', true );
										$primary_button_text = get_post_meta( $post->ID, 'walker_slider_primary_button', true );
										if(!$primary_button_link){
											$primary_button_link ='#';
										}
										if(!empty($primary_button_text)){
											echo '<a class="gridchamp-primary-button" href="' . esc_url($primary_button_link) . '">'. esc_html(get_post_meta(get_the_ID(), 'walker_slider_primary_button', true)) .'</a>';
										}
										
											$secondary_button_link = get_post_meta( $post->ID, 'walker_slider_secondary_button_link', true );
											$secondary_button_text = get_post_meta( $post->ID, 'walker_slider_secondary_button', true );
											if(!$secondary_button_link){
												$secondary_button_link = '#';
											}
											if(!empty($secondary_button_text)){
												echo '<a class="gridchamp-secondary-button" href="' . esc_url($secondary_button_link) . '">'. esc_html(get_post_meta(get_the_ID(), 'walker_slider_secondary_button', true)) .'</a>';
											}
											
										?>
									</div>
										</div>				
								</div>
							</div>
							</div>
						</div>
					<?php endwhile; 
					wp_reset_postdata(); ?>
				</div>

    <div class="gridchamp-slider-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
    <div class="gridchamp-slider-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
    <!-- !next / prev arrows -->
				<div class="gridchamp-slider-pagination"></div>
			</div>
		
	</div>
<?php } ?>