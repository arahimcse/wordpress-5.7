<?php
$gridchamp_banner_status = get_theme_mod('banner_status');
if( $gridchamp_banner_status){
	if(gridchamp_set_to_premium()){
		$gridchamp_current_hero_type = get_theme_mod('gridchamp_hero_type','gridchamp-banner');
		if($gridchamp_current_hero_type=='gridchamp-banner'){
		  	gridchamp_banner_section();
	 	}else{
		 	$slider_type = get_theme_mod('gridchamp_slder_layout');
			if($slider_type=='gridchamp-slider-3'){
				get_template_part( 'template-parts/partials/slider/slider-three');
			}elseif($slider_type=='gridchamp-slider-2'){
				get_template_part( 'template-parts/partials/slider/slider-two');
			} else{
				get_template_part( 'template-parts/partials/slider/slider-one');
			}
		
		}
 	}else{
		gridchamp_banner_section();
	} 
} ?>