<?php
if(gridchamp_set_to_premium()){?>
<div class="walker-wraper slider-wraper slider-three no-gap">
	
		<div class="swiper-container gridchamp-slider-three">
  			<div class="swiper-wrapper">
				<?php $gridchamp_query = new WP_Query( array( 'post_type' => 'wcr_slider', 'order'=> 'DESC') );
					    while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();
					    ?>

					    <div class="swiper-slide">
					    	<div class="walkerwp-slider-box">
						  		
								<div class="walker-container slide-text">
									<?php
										if(get_theme_mod('walker_slder_text_align')=='slide-text-align-right'){
											$slide_text_align='text-right';
										}elseif(get_theme_mod('walker_slder_text_align')=='slide-text-align-center'){
											$slide_text_align='text-center';
										}else{
											$slide_text_align='';
										}

										if(get_theme_mod('walker_slder_text_align')=='slide-text-align-right'){?>
											<div class="walkerwp-grid-6">
												<?php 
											    	if ( has_post_thumbnail() ) {?>
													<div class="slide-image"><?php the_post_thumbnail(); ?></div>
												<?php	} ?>
											</div>
										<?php }
									?>

									<div class="walkerwp-grid-6">
										<div class="slide-content">
											<div class="slide-overlay-text <?php echo esc_attr($slide_text_align);?>">
												<h1 class="slider-title"><?php  the_title(); ?></h1>	
												<p class="slider-short-inco"><?php the_excerpt();?></p>
												<div class="button-group">
													<?php
													$primary_button_link = get_post_meta( $post->ID, 'walker_slider_primary_button_link', true );
													$primary_button_text = get_post_meta( $post->ID, 'walker_slider_primary_button', true );
													if(!$primary_button_link){
														$primary_button_link ='#';
													}
													if(!empty($primary_button_text)){
														echo '<a class="gridchamp-primary-button" href="' . esc_url($primary_button_link) . '">'. esc_html(get_post_meta(get_the_ID(), 'walker_slider_primary_button', true)) .'</a>';
													}
													
														$secondary_button_link = get_post_meta( $post->ID, 'walker_slider_secondary_button_link', true );
														$secondary_button_text = get_post_meta( $post->ID, 'walker_slider_secondary_button', true );
														if(!$secondary_button_link){
															$secondary_button_link = '#';
														}
														if(!empty($secondary_button_text)){
															echo '<a class="gridchamp-secondary-button" href="' . esc_url($secondary_button_link) . '">'. esc_html(get_post_meta(get_the_ID(), 'walker_slider_secondary_button', true)) .'</a>';
														}
														
													?>
												</div>
											</div>				
										</div>
									</div>
									<?php if(get_theme_mod('walker_slder_text_align')=='slide-text-align-left' || get_theme_mod('walker_slder_text_align')=='slide-text-align-center'){?>
											<div class="walkerwp-grid-6">
												<?php 
											    	if ( has_post_thumbnail() ) {?>
													<div class="slide-image"><?php the_post_thumbnail(); ?></div>
												<?php	} ?>
											</div>
										<?php } ?>
								</div>
							</div>
						</div>
					<?php endwhile; 
					wp_reset_postdata(); ?>
				</div>

    
    <!-- !next / prev arrows -->
				<div class="gridchamp-slider-pagination"></div>
			</div>
		
	</div>
<?php } ?>