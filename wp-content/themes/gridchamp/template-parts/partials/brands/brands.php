<?php 
if(gridchamp_set_to_premium()){
	if(get_theme_mod('brand_status')){?>
		<div class="walker-wraper brands-wraper">
			<div class="walker-container text-center">
				<div class="walkerwp-grid-12">
				<?php 
					if(esc_attr(get_theme_mod('brands_heading_text')) ){
						echo '<h1 class="section-heading text-center">'.esc_attr(get_theme_mod('brands_heading_text')).'</h1>';
					}
					if(esc_attr(get_theme_mod('brands_desc_text')) ){
						echo '<p class="section-subheader text-center">'.esc_attr(get_theme_mod('brands_desc_text')).'</p>';
					}?>
				</div>
			</div>
			<div class="walker-container">
				<div class="swiper-container gridchamp-brands">
      				<div class="swiper-wrapper">
						<?php $gridchamp_query = new WP_Query( array( 'post_type' => 'wcr_brands', 'order'=> 'DESC', 'posts_per_page' =>-1) );
						    while ($gridchamp_query->have_posts()) : $gridchamp_query->the_post();?>
						    <div class="swiper-slide">
						    	
							  		<?php 
							    	if ( has_post_thumbnail() ) {?>
										<?php the_post_thumbnail(); ?>
									<?php	} ?>
									
								
							</div>
						<?php endwhile; 
						wp_reset_postdata(); ?>
					</div>
					<div class="brands-pagination"></div>
				</div>
			</div>
		</div>
	<?php } 
} ?>