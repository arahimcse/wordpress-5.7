<?php
/**
 *Footer widget for Gridchamp
 *
 * @package gridchamp
 * @since version 1.0.0
 */
	if(is_active_sidebar('footer-1') 
		&& is_active_sidebar('footer-2')
		&& is_active_sidebar('footer-3')
		&& is_active_sidebar('footer-4')
	){
		$gridchamp_widget_class ="walkerwp-grid-3";
	}
	elseif(is_active_sidebar('footer-1') 
		&& is_active_sidebar('footer-2')
		&& is_active_sidebar('footer-3')
	){
		$gridchamp_widget_class ="walkerwp-grid-4";
	}
	elseif(is_active_sidebar('footer-1') 
		&& is_active_sidebar('footer-2')
		){
		$gridchamp_widget_class ="walkerwp-grid-6";
	}else{
		$gridchamp_widget_class ="walkerwp-grid-12";
	}

?>
<?php
if(is_active_sidebar('footer-1') || is_active_sidebar('footer-2') || is_active_sidebar('footer-3') || is_active_sidebar('footer-4')){ ?>
	<div class="walker-wraper footer-widgets-wraper">
		<div class="walker-container">
			<div class="walkerwp-grid-12 gridchamp-footer-widget">
				<div class="walker-container">
					<div id="secondary" class="widget-area gridchamp-bottom <?php echo esc_attr($gridchamp_widget_class); ?>" role="complementary" >
						<?php dynamic_sidebar( 'footer-1' ); ?>
					</div>

					<div id="secondary" class="widget-area gridchamp-bottom <?php echo esc_attr($gridchamp_widget_class); ?>" role="complementary" >
						<?php dynamic_sidebar( 'footer-2' ); ?>
					</div>
					<div id="secondary" class="widget-area gridchamp-bottom <?php echo esc_attr($gridchamp_widget_class); ?>" role="complementary" >
						<?php dynamic_sidebar( 'footer-3' ); ?>
					</div>
					<div id="secondary" class="widget-area gridchamp-bottom <?php echo esc_attr($gridchamp_widget_class); ?>" role="complementary" >
						<?php dynamic_sidebar( 'footer-4' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php 
} ?>