<?php do_action('gridchamp_header_before', 'gridchamp'); ?>
<div class="walker-wraper header-layout-3">
	<div class="walker-container">
		<?php do_action('gridchamp_brandings', 'gridchamp');?>
		<div class="header-right">
			<?php 
				
				
				do_action('gridchamp_header_primary_button','gridchamp');
				do_action('gridchamp_header_secondary_button','gridchamp');
			?>
		</div>
	</div>
</div>
<!-- Navigation Start -->
<div class="walker-wraper header-layout-3-navigation no-gap">
	<div class="walker-container">
		<?php 
			do_action('gridchamp_navigation','gridchamp');?>
			<div class="header-right">
				<?php 
				do_action('gridchamp_header_search_icon','gridchamp');
				do_action('gridchamp_cart_icon','gridchamp');
				?>
			</div>
	</div>
</div>