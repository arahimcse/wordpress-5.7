<?php 
	$current_search_status = get_theme_mod('search_icon_status');
	$current_cart_status = get_theme_mod('cart_icon_status');
	$current_slogan_status = get_theme_mod('gridchamp_header_slogan_text');
	$current_contact_status = get_theme_mod('gridchamp_header_contact');
	$current_button_status = get_theme_mod('gridchamp_header_primary_button');
	if($current_search_status || $current_cart_status || $current_slogan_status ||  $current_contact_status || $current_button_status){?>
		
	<div class="walker-wraper header-layout-2">
	<div class="walker-container full-width">
		<div class="header-2-left">
			<?php do_action('gridchamp_brandings', 'gridchamp');?>
			<div class="header-2-navigation">
				<?php do_action('gridchamp_navigation','gridchamp'); ?>
			</div>
		</div>
		<?php if($current_search_status || $current_cart_status || $current_slogan_status ||  $current_contact_status || $current_button_status){?>
			<div class="header-right">
			<?php 
				do_action('gridchamp_header_search_icon','gridchamp');
				do_action('gridchamp_cart_icon','gridchamp');
				gridchamp_header_hot_line();
				
				do_action('gridchamp_header_primary_button','gridchamp');
			?>
		</div>
	<?php	}
	?>
		
	</div>
</div>

	<?php }else{?>
		<div class="walker-wraper header-layout-2">
	<div class="walker-container full-width">
		<?php do_action('gridchamp_brandings', 'gridchamp');?>
		<div class="header-2-navigation">
			<?php do_action('gridchamp_navigation','gridchamp'); ?>
		</div>
		<?php if($current_search_status || $current_cart_status || $current_slogan_status ||  $current_contact_status || $current_button_status){?>
			<div class="header-right">
			<?php 
				do_action('gridchamp_header_search_icon','gridchamp');
				do_action('gridchamp_cart_icon','gridchamp');
				gridchamp_header_hot_line();
				
				do_action('gridchamp_header_primary_button','gridchamp');
			?>
		</div>
	<?php	}
	?>
		
	</div>
</div>
	<?php }?>
