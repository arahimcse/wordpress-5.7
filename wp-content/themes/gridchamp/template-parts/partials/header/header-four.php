<?php do_action('gridchamp_header_before', 'gridchamp'); ?>
<div class="walker-wraper header-layout-4">
	<div class="walker-container">
		<?php do_action('gridchamp_brandings', 'gridchamp');?>
		
	</div>
</div>
<!-- Navigation Start -->
<div class="walker-wraper header-layout-4-navigation no-gap">
	<div class="walker-container">
		<?php 
			do_action('gridchamp_navigation','gridchamp');?>
			
	</div>
</div>