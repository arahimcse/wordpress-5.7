<div class="walker-wraper header-layout-5">
	<div class="walker-container">
		<div class="walkerwp-grid-4 header-social-media">
			<?php 
				get_template_part( 'template-parts/partials/social-media/social-media'); 
			?>
		</div>
		<div class="walkerwp-grid-4 header-branding">
			<?php 
				do_action('gridchamp_brandings', 'gridchamp');
			?>
		</div>
		<div class="header-btn-group walkerwp-grid-4">
			<?php
				do_action('gridchamp_header_primary_button','gridchamp');
				do_action('gridchamp_header_secondary_button','gridchamp');
			?>
		</div>
	</div>
</div>
<!-- Navigation Start -->
<div class="walker-wraper header-layout-5-navigation no-gap">
	<div class="walker-container">
		<?php 
			do_action('gridchamp_navigation','gridchamp');?>
			
	</div>
</div>