<?php do_action('gridchamp_header_before', 'gridchamp');

if(get_theme_mod('gridchamp_transparent_header') && is_front_page() || get_theme_mod('gridchamp_transparent_header') && is_home() ){
	$header_style= 'transparent-header';
}else{
	$header_style= '';
}?>
<div class="walker-wraper header-layout-1 no-gap <?php echo esc_attr($header_style);?>">
	<div class="walker-container">
		<?php do_action('gridchamp_brandings', 'gridchamp');?>
		<div class="header-right">
			<?php 
				do_action('gridchamp_navigation','gridchamp');
				do_action('gridchamp_header_search_icon','gridchamp');
				do_action('gridchamp_cart_icon','gridchamp');
				do_action('gridchamp_header_primary_button','gridchamp');
			?>
		</div>
	</div>
</div>