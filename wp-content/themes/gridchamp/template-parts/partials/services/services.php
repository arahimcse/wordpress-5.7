<?php $gridchamp_service_status = get_theme_mod('services_status');
if($gridchamp_service_status){?>
	<div class="walker-wraper service-wraper service-layou-1">
		<div class="walker-container service-header">
			<div class="walkerwp-grid-8">
				<?php 
				if(get_theme_mod('services_heading_text') ){
					echo '<h1 class="section-heading">'.esc_html(get_theme_mod('services_heading_text')).'</h1>';
				}
				if(get_theme_mod('services_desc_text') ){
					echo '<p class="section-subheader">'.esc_html(get_theme_mod('services_desc_text')).'</p>';
				}?>
			</div>
			<div class="walkerwp-grid-4 button-col">
				<?php
					if(get_theme_mod('services_more_list_text')){
						if(get_theme_mod('services_more_list_text_link')){
							$more_service_link = get_theme_mod('services_more_list_text_link');
						}else{
							$more_service_link = '#';
						}
						?>
						<a href="<?php echo esc_url($more_service_link);?>" class="details-about gridchamp-secondary-button style-outline-light ">
							<?php echo esc_html(get_theme_mod('services_more_list_text'));?>
						</a>
					<?php }
				?>
			</div>
		</div>
		<div class="walker-container">
			<div class="services-list walkerwp-grid-12">
				<?php
				
				$gridchamp_service_parent_page= get_theme_mod('gridchamp_service_page');
				if(!empty($gridchamp_service_parent_page) && $gridchamp_service_parent_page != 'None' ){
					if(gridchamp_set_to_premium()){
						$gridchamp_service_items = -1;
					}else{
						$gridchamp_service_items = 3;
					}
					$service_page_id = get_page_by_title( $gridchamp_service_parent_page );
					$args = array(
						'posts_per_page' => $gridchamp_service_items,
						'post_type' => 'page',
						'post_parent' => $service_page_id->ID
					);

					$gridchamp_query = new WP_Query( $args );
					if ( $gridchamp_query->have_posts() ) :
						while ( $gridchamp_query->have_posts() ) : $gridchamp_query->the_post();
							echo '<div class="gridchamp-service-box">';
							if ( has_post_thumbnail() ) : ?>
								    <a class="service-thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								        <?php the_post_thumbnail(); ?>
								    </a>
							<?php endif;
							echo '<h3 class="service-title">'; the_title(); echo '</h3>';
							echo '<p class="service-description">'. esc_html(gridchamp_excerpt( 20 )).'</p>';
							if(esc_html(get_theme_mod('services_readmore_text')) || esc_html(get_theme_mod('services_readmore_text'))!=''){?>
								<a href="<?php echo the_permalink();?>" class="gridchamp-primary-button details-service style-extend"> <?php echo esc_html(get_theme_mod('services_readmore_text'));?></a>
							<?php } else{?>
								<a href="<?php echo the_permalink();?>" class="details-service gridchamp-primary-button style-extend"> <?php echo __('Read More', 'gridchamp');?></a>
							<?php }
							echo '</div>';
						endwhile;
					endif;
					wp_reset_postdata();
				}
				
				?>
			</div>

		</div>
	</div>
	<?php 
} ?>