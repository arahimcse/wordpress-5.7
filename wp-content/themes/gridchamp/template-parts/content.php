<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Gridchamp
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php gridchamp_post_thumbnail();
		if ( has_post_thumbnail() ) {
			$inner_content_class= 'has-thumbnails';
		}else{
			$inner_content_class= 'has-no-thumbnails';
		}

	 ?>
	<div class="article-inner <?php echo $inner_content_class;?>">
		<header class="entry-header">
			<?php
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
					<?php
					if(gridchamp_set_to_premium()){
						if(get_theme_mod('author_status','true')){
							gridchamp_posted_by();
						}
						if(get_theme_mod('post_date_status','true')){
							gridchamp_posted_on();
						}
						if(get_theme_mod('category_status','true')){
							gridchamp_post_category();
						}
						if(get_theme_mod('tags_status','true')){
							gridchamp_post_tag();
						}
						if(get_theme_mod('comment_status','true')){
							gridchamp_post_comments();
						}
					}else{
						gridchamp_posted_by();
						gridchamp_posted_on();
						gridchamp_post_category();
						gridchamp_post_tag();
						gridchamp_post_comments();
					}
					?>
				</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

	

	<div class="entry-content">
		<?php
		if ( is_singular() ) {
			the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'gridchamp' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'gridchamp' ),
				'after'  => '</div>',
			)
		);
	} else{
		echo'<div class="gridchamp-excerpt">'. esc_html(gridchamp_excerpt( gridchamp_custom_excerpt_length() )).'</div>';?>
		<br /><a href="<?php echo the_permalink();?>" class="gridchamp-primary-button"> 
		<?php if(get_theme_mod('gridchamp_excerpt_more')){
				echo esc_html(get_theme_mod('gridchamp_excerpt_more'));
			}else{
				echo __('Read More','gridchamp');
			}?>
		</a>
	<?php }

		
		?>
		</div>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
