<?php
/**
 * Template part for displaying portfolio
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Gridchamp
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="walker-wraper single-portfolio">
		<div class="walker-container">
			<div class="walkerwp-grid-4">
				<h2 class="portfolio-title"><?php the_title();?></h2>
				<?php
				
					$terms = get_the_terms( $post->ID, 'wcr_portfolio_category' );
					if($terms){
						echo '<h3 class="cat-level">'.__('Project Category','gridchamp').'</h3>';
						echo '<ul class="portfolio-categories">';
					    foreach($terms as $term) {
					      echo '<li>'. $term->name .'</li>';
					    }
					echo '</ul>';
					}
					
				 ?>
			</div>
			<div class="walkerwp-grid-8">
				<?php 
				gridchamp_post_thumbnail();
				the_content();?>
			</div>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
