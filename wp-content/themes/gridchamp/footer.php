<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gridchamp
 */

?>
	<footer id="colophon" class="site-footer">
		<?php get_template_part( 'template-parts/partials/footer/footer-widgets'); ?>
		<div class="walker-wraper footer-copyright-section">
			<div class="walker-container">
				<?php do_action('gridchamp_footer_text','gridchamp'); ?>
			</div>
		</div>
	<?php 
	if(gridchamp_set_to_premium()){
		do_action('gridchamp_scroll_top_icon','gridchamp');
	}
	?>
	</footer><!-- #colophon -->

<?php 

wp_footer(); ?>

</body>
</html>

