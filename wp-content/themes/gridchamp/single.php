<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Gridchamp
 */

get_header();
?>
<?php
	if(get_theme_mod('blog_sidebar_layout')){
		$gridchamp_blog_layout = get_theme_mod('blog_sidebar_layout');
	} else{
		$gridchamp_blog_layout ='right-sidebar';
	}

	if($gridchamp_blog_layout =='no-sidebar'){
		$gridchamp_content_class= 'walkerwp-grid-12';
	} else{
		$gridchamp_content_class= 'walkerwp-grid-9';
	}
	if($gridchamp_blog_layout =='right-sidebar'){
		$content_sub_class='right-sidebar-layout';
	} elseif($gridchamp_blog_layout =='left-sidebar'){
		$content_sub_class='left-sidebar-layout';
	} else{
		$content_sub_class='full-width-content';
	}
?>
<div class="walker-wraper">
	<div class="walker-container">
		<?php if($gridchamp_blog_layout=='left-sidebar'){?>
			<div class="walkerwp-grid-3 sidebar-block"><?php get_sidebar(); ?></div>
		<?php } ?>
		<main id="primary" class="site-main <?php echo esc_attr($gridchamp_content_class); echo ' '. esc_attr($content_sub_class); ?>">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation(
				array(
					'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'gridchamp' ) . '</span> <span class="nav-title">%title</span>',
					'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'gridchamp' ) . '</span> <span class="nav-title">%title</span>',
				)
			);

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->
		<?php if($gridchamp_blog_layout=='right-sidebar'){?>
			<div class="walkerwp-grid-3 sidebar-block"><?php get_sidebar(); ?></div>
		<?php } ?>
	</div>
</div>
<?php get_footer();
