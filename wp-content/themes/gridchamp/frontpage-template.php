<?php
/**
 *  Template Name: Front Page for Theme
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gridchamp
 */


get_header(); ?>
<?php

if(gridchamp_set_to_premium()){

	$default_order = array( 'about-us', 'services', 'features','portfolio','team','testimonial','cta','recentpost','faqs','brands');
	$gridchamp_sections = get_theme_mod( 'gridchamp_section_order', $default_order );
	for ($i = 0; $i < 10; $i++):
		if( !empty($gridchamp_sections[$i]) ):
			switch ( $gridchamp_sections[$i] ) {
				case "about-us":
					gridchamp_homesection_about();
				break;
				case "services":
					gridchamp_homesection_services();
				break;
				case "features":
					gridchamp_homesection_features();
				break;
				case "portfolio":
					gridchamp_homesection_portfolios();
				break;
				case "team":
					gridchamp_homesection_team();
				break;
				case "testimonial":
					gridchamp_homesection_testimonials();
				break;
				case "cta":
					gridchamp_homesection_cta();
				break;
				case "recentpost":
					gridchamp_homesection_blogs();
				break;
				case "faqs":
					gridchamp_homesection_faqs();
				break;
				case "brands":
					gridchamp_homesection_brands();
				break;
			}
		endif;
	endfor;
}else{
	get_template_part( 'template-parts/partials/about/aboutus');
	get_template_part( 'template-parts/partials/services/services');
	get_template_part( 'template-parts/partials/testimonial/testimonials');
	get_template_part( 'template-parts/partials/cta/cta');
	get_template_part( 'template-parts/partials/blogs/recentblogs');
}


?>

<?php get_footer();