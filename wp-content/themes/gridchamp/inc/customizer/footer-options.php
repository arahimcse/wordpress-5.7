<?php
/**
*Footer customizer options
*
* @package gridchamp
*
*/
if(gridchamp_set_to_premium()):
	if (! function_exists('gridchamp_footer_options_register')) {
		function gridchamp_footer_options_register( $wp_customize ) {
			$wp_customize->add_section('gridchamp_footer_setting', 
			 	array(
			        'title' => esc_html__('Footer', 'gridchamp'),
			        'panel' =>'gridchamp_theme_option',
			        'priority' => 12
		    	)
			 );
			$wp_customize->add_setting( 'gridchamp_footer_bg_color', 
				array(
			        'default'        => '#232323',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'gridchamp_footer_bg_color', 
				array(
			        'label'   => esc_html__( 'Footer Background Color', 'gridchamp' ),
			        'section' => 'gridchamp_footer_setting',
			        'settings'   => 'gridchamp_footer_bg_color',
			        'priority' => 4
			    ) ) 
			);
			$wp_customize->selective_refresh->add_partial( 'gridchamp_footer_bg_color', array(
	            'selector' => 'footer#colophon',
	        ) );
			$wp_customize->add_setting( 'gridchamp_footer_text_color', 
				array(
			        'default'        => '#ffffff',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'gridchamp_footer_text_color', 
				array(
			        'label'   => esc_html__( 'Footer Text Color', 'gridchamp' ),
			        'section' => 'gridchamp_footer_setting',
			        'settings'   => 'gridchamp_footer_text_color',
			        'priority' => 4
			    ) ) 
			);
			$wp_customize->add_setting( 'gridchamp_footer_link_color', 
				array(
			        'default'        => '#7602d0',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'gridchamp_footer_link_color', 
				array(
			        'label'   => esc_html__( 'Footer Link Color', 'gridchamp' ),
			        'section' => 'gridchamp_footer_setting',
			        'settings'   => 'gridchamp_footer_link_color',
			        'priority' => 4
			    ) ) 
			);
			$wp_customize->add_setting( 'gridchamp_footer_link_hover_color', 
				array(
			        'default'        => '#fc6f11',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'gridchamp_footer_link_hover_color', 
				array(
			        'label'   => esc_html__( 'Footer Link Hover Color', 'gridchamp' ),
			        'section' => 'gridchamp_footer_setting',
			        'settings'   => 'gridchamp_footer_link_hover_color',
			        'priority' => 4
			    ) ) 
			);
			$wp_customize->add_setting( 'gridchamp_footer_bottom_color', 
				array(
			        'default'        => '#000000',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'gridchamp_footer_bottom_color', 
				array(
			        'label'   => esc_html__( 'Copyright Background Color', 'gridchamp' ),
			        'section' => 'gridchamp_footer_setting',
			        'settings'   => 'gridchamp_footer_bottom_color',
			        'priority' => 4
			    ) ) 
			);
			$wp_customize->add_setting( 'gridchamp_copyright_text_color', 
				array(
			        'default'        => '#ffffff',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'gridchamp_copyright_text_color', 
				array(
			        'label'   => esc_html__( 'Footer Copyright Text Color', 'gridchamp' ),
			        'section' => 'gridchamp_footer_setting',
			        'settings'   => 'gridchamp_copyright_text_color',
			        'priority' => 4
			    ) ) 
			);
			
			$wp_customize->add_setting( 'footer_copiright_text', 
			 	array(
					'capability' => 'edit_theme_options',
					'default' => '',
					'sanitize_callback' => 'wp_kses_post',
				) 
			);

			$wp_customize->add_control( 'footer_copiright_text', 
				array(
					'type' => 'textarea',
					'section' => 'gridchamp_footer_setting',
					'label' => esc_html__( 'Footer Text','gridchamp' ),
					'description' => esc_html__( 'Adding Footer Copyright Text','gridchamp' ),
				)
			);
			$wp_customize->add_setting( 
	        'copyright_text_alignment', 
	        array(
	            'default'           => 'copyright-text-align-left',
	            'sanitize_callback' => 'gridchamp_sanitize_choices'
	        ) 
	    );
	    
	    $wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'copyright_text_alignment',
				array(
					'section'	  => 'gridchamp_footer_setting',
					'label'		  => esc_html__( 'Copyright Text Alignment', 'gridchamp' ),
					'description' => '',
					'type'        => 'select',
					'choices'	  => array(
						'copyright-text-align-left'  => esc_html__('Left','gridchamp'),
						'copyright-text-align-center'  => esc_html__('Center','gridchamp'),
					),
					
				)
			)
		);
			$wp_customize->selective_refresh->add_partial( 'footer_copiright_text', array(
	            'selector' => '.site-info',
	        ) );
		}

	}
	add_action( 'customize_register', 'gridchamp_footer_options_register' );
endif;
