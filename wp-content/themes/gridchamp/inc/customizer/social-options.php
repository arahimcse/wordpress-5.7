<?php
/**
*Social Media customizer options
*
* @package Gridchamp
*
*/

if (! function_exists('gridchamp_social_options_register')) {
    function gridchamp_social_options_register( $wp_customize ) {
        // Social media 
        $wp_customize->add_section('gridchamp_social_setup', 
          array(
            'title' => esc_html__('Social Media', 'gridchamp'),
            'panel' => 'gridchamp_theme_option',
            'priority' => 6
          )
        );

        //Facebook Link
        $wp_customize->add_setting( 'gridchamp_facebook',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_facebook', 
            array(
              'label'   => esc_html__( 'Facebook', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_facebook',
              'type'     => 'text',
              'priority' => 1
          )
        );
        $wp_customize->selective_refresh->add_partial( 'gridchamp_facebook', array(
            'selector' => '.gridchamp-top-header ul.gridchamp-social',
        ) );
        //Twitter Link
        $wp_customize->add_setting( 'gridchamp_twitter',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_twitter', 
            array(
              'label'   => esc_html__( 'Twitter', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_twitter',
              'type'     => 'text',
              'priority' => 2
          )
        );
        $wp_customize->selective_refresh->add_partial( 'gridchamp_twitter', array(
            'selector' => '.gridchamp-top-header ul.gridchamp-social',
        ) );
        //Youtube Link
        $wp_customize->add_setting( 'gridchamp_youtube',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_youtube', 
            array(
              'label'   => esc_html__( 'Youtube', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_youtube',
              'type'     => 'text',
              'priority' => 2
          )
        );
        //Instagram
        $wp_customize->add_setting( 'gridchamp_instagram',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_instagram', 
            array(
              'label'   => esc_html__( 'Instagram', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_instagram',
              'type'     => 'text',
              'priority' => 2
          )
        );
        //Linkedin
        $wp_customize->add_setting( 'gridchamp_linkedin',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_linkedin', 
            array(
              'label'   => esc_html__( 'Linkedin', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_linkedin',
              'type'     => 'text',
              'priority' => 2
          )
        );
        //Google
        $wp_customize->add_setting( 'gridchamp_google',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_google', 
            array(
              'label'   => esc_html__( 'Google Business', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_google',
              'type'     => 'text',
              'priority' => 2
          )
        );
        //Pinterest
        $wp_customize->add_setting( 'gridchamp_pinterest',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_pinterest', 
            array(
              'label'   => esc_html__( 'Pinterest', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_pinterest',
              'type'     => 'text',
              'priority' => 2
          )
        );
        //Pinterest
        $wp_customize->add_setting( 'gridchamp_github',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_github', 
            array(
              'label'   => esc_html__( 'GitHub', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_github',
              'type'     => 'text',
              'priority' => 2
          )
        );
      
        //Yelp
        $wp_customize->add_setting( 'gridchamp_yelp',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_yelp', 
            array(
              'label'   => esc_html__( 'Yelp', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_yelp',
              'type'     => 'text',
              'priority' => 2
          )
        );
        //Angellist
        $wp_customize->add_setting( 'gridchamp_angellist',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_angellist', 
            array(
              'label'   => esc_html__( 'Angellist', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_angellist',
              'type'     => 'text',
              'priority' => 2
          )
        );
         //Reddit
        $wp_customize->add_setting( 'gridchamp_reddit',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_reddit', 
            array(
              'label'   => esc_html__( 'Reddit', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_reddit',
              'type'     => 'text',
              'priority' => 2
          )
        );
        //Flickr
        $wp_customize->add_setting( 'gridchamp_flickr',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_flickr', 
            array(
              'label'   => esc_html__( 'Flickr', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_flickr',
              'type'     => 'text',
              'priority' => 2
          )
        );
        $wp_customize->add_setting( 'gridchamp_dribble',
          array(
            'default'        => '',
            'sanitize_callback' => 'gridchamp_sanitize_url'
          ) 
        );
        $wp_customize->add_control( 'gridchamp_dribble', 
            array(
              'label'   => esc_html__( 'Dribble', 'gridchamp' ),
              'section' => 'gridchamp_social_setup',
              'settings'   => 'gridchamp_dribble',
              'type'     => 'text',
              'priority' => 2
          )
        );
    }

}
add_action( 'customize_register', 'gridchamp_social_options_register' );