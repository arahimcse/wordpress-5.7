<?php
/**
*Typography customizer options
*
* @package Gridchamp
*
*/

if (! function_exists('gridchamp_extra_options_register')) {
	function gridchamp_extra_options_register( $wp_customize ) {
		//Search Icon
		$wp_customize->add_section('gridchamp_extra_setup', 
		 	array(
		        'title' => esc_html__('Extra Settings', 'gridchamp'),
		        'panel' =>'gridchamp_theme_option',
		        'priority' => 8,
	    	)
		 );
		$wp_customize->add_setting( 'search_icon_status', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'search_icon_status', 
			array(
			  'label'   => esc_html__( 'Show Search Icon in Header', 'gridchamp' ),
			  'section' => 'gridchamp_extra_setup',
			  'settings' => 'search_icon_status',
			  'type'    => 'checkbox',
			)
		);
		if(class_exists('woocommerce')){
			$wp_customize->add_setting( 'cart_icon_status', 
		    	array(
			      'default'  =>  false,
			      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
			  	)
		    );
			$wp_customize->add_control( 'cart_icon_status', 
				array(
				  'label'   => esc_html__( 'Show Cart Icon in Header', 'gridchamp' ),
				  'section' => 'gridchamp_extra_setup',
				  'settings' => 'cart_icon_status',
				  'type'    => 'checkbox',
				)
			);
		}
		$wp_customize->add_setting( 'gridchamp_container_width', 
			array(
				'capability' => 'edit_theme_options',
				'sanitize_callback' => 'gridchamp_sanitize_number_absint',
				'default' => 1180,
				) 
			);

			$wp_customize->add_control( 'gridchamp_container_width', 
				array(
					'type' => 'number',
					'section' => 'gridchamp_extra_setup',
					'settings' => 'gridchamp_container_width',
					'label' => esc_html__( 'Container Width (px) ','gridchamp' ),
					'description' => esc_html__('This features increase or decrease container width of site.','gridchamp'),
					'description' => '',
				) 
			);
		
		//Inner page Header
		$wp_customize->add_section('gridchamp_innerpage_subheader', 
		 	array(
		        'title' => esc_html__('Page Sub-Header', 'gridchamp'),
		        'panel' =>'gridchamp_theme_option',
		        'priority' => 9,
	    	)
		 );
		$wp_customize->add_setting( 'innerpage_header_status', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
	    $wp_customize->selective_refresh->add_partial( 'innerpage_header_status', array(
            'selector' => '.inner-page-subheader span.page-header-title',
        ) );
		$wp_customize->add_control( 'innerpage_header_status', 
			array(
			  'label'   => esc_html__( 'Display Sub-header', 'gridchamp' ),
			  'section' => 'gridchamp_innerpage_subheader',
			  'settings' => 'innerpage_header_status',
			  'type'    => 'checkbox',
			  'priority' => 1
			)
		);
		$wp_customize->add_setting( 'breadcrumbs_status', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'breadcrumbs_status', 
			array(
			  'label'   => esc_html__( 'Display Breadcrumbs', 'gridchamp' ),
			  'section' => 'gridchamp_innerpage_subheader',
			  'settings' => 'breadcrumbs_status',
			  'type'    => 'checkbox',
			  'priority' => 2,
			  'active_callback' => function(){
		          return get_theme_mod( 'innerpage_header_status', true );
		      },
			)
		);
		$wp_customize->add_setting( 'subheader_bg_color', 
			array(
		        'default'        => '#7602d0',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'subheader_bg_color', 
			array(
		        'label'   => esc_html__( 'Background Color', 'gridchamp' ),
		        'section' => 'gridchamp_innerpage_subheader',
		        'settings'   => 'subheader_bg_color',
		        'priority' => 4,
		        'active_callback' => function(){
		            return get_theme_mod( 'innerpage_header_status', true );
		        },
		    ) ) 
		);
		$wp_customize->add_setting('subheader_bg_image', array(
	        'transport'         => 'refresh',
	        'sanitize_callback'     =>  'gridchamp_sanitize_file',
	    ));

	    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'subheader_bg_image', array(
	        'label'             => esc_html__('Background Image', 'gridchamp'),
	        'section'           => 'gridchamp_innerpage_subheader',
	        'settings'          => 'subheader_bg_image',
	        'priority' 			=> 4,
	        'active_callback' => function(){
	            return get_theme_mod( 'innerpage_header_status', true );
	        },
	    )));   
		$wp_customize->add_setting( 'subheader_text_color', 
			array(
		        'default'        => '#ffffff',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'subheader_text_color', 
			array(
		        'label'   => esc_html__( 'Text Color', 'gridchamp' ),
		        'section' => 'gridchamp_innerpage_subheader',
		        'settings'   => 'subheader_text_color',
		        'priority' => 5,
		        'active_callback' => function(){
		            return get_theme_mod( 'innerpage_header_status', true );
		        },
		    ) ) 
		);

		$wp_customize->add_setting( 'breadcrumbs_link_color', 
			array(
		        'default'        => '#ffffff',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'breadcrumbs_link_color', 
			array(
		        'label'   => esc_html__( 'Breadcrumbs Link Color', 'gridchamp' ),
		        'section' => 'gridchamp_innerpage_subheader',
		        'settings'   => 'breadcrumbs_link_color',
		        'priority' => 6,
		        'active_callback' => function(){
		            return get_theme_mod( 'innerpage_header_status', true );
		        },
		    ) ) 
		);
		$wp_customize->add_setting( 'breadcrumbs_link_hover_color', 
			array(
		        'default'        => '#fc6f11',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'breadcrumbs_link_hover_color', 
			array(
		        'label'   => esc_html__( 'Breadcrumbs Link Hover Color', 'gridchamp' ),
		        'section' => 'gridchamp_innerpage_subheader',
		        'settings'   => 'breadcrumbs_link_hover_color',
		        'priority' => 7,
		        'active_callback' => function(){
		            return get_theme_mod( 'innerpage_header_status', true );
		        },
		    ) ) 
		);

		if(gridchamp_set_to_premium()){
			//paginations
			$wp_customize->add_setting( 
		        'pagination_style', 
		        array(
		            'default'           => 'normal-paginate-style',
		            'sanitize_callback' => 'gridchamp_sanitize_choices'
		        ) 
		    );
		    
		    $wp_customize->add_control(
				new WP_Customize_Control(
					$wp_customize,
					'pagination_style',
					array(
						'section'	  => 'gridchamp_extra_setup',
						'label'		  => esc_html__( 'Choose Pagination style', 'gridchamp' ),
						'description' => '',
						'type'           => 'select',
						'choices'	  => array(
							'normal-paginate-style'    => esc_html__('Next/Preview','gridchamp'),
							'numeric-paginate-style'  => esc_html__('Numeric','gridchamp'),
						)
					)
				)
			);
			//Scroll Top
			$wp_customize->add_setting( 'Scroll_top_option', 
		    	array(
			      'default'  =>  false,
			      'sanitize_callback' => 'walker_core_sanitize_checkbox'
			  	)
		    );
			$wp_customize->add_control( 'Scroll_top_option', 
				array(
				  'label'   => esc_html__( 'Enable Scroll Top', 'gridchamp' ),
				  'section' => 'gridchamp_extra_setup',
				  'settings' => 'Scroll_top_option',
				  'type'    => 'checkbox',
				)
			);

			$wp_customize->add_setting( 'gridchamp_btns_radius', 
			array(
				'capability' => 'edit_theme_options',
				'sanitize_callback' => 'gridchamp_sanitize_number_absint',
				'default' => 40,
				) 
			);

			$wp_customize->add_control( 'gridchamp_btns_radius', 
				array(
					'type' => 'number',
					'min' => 1,
					'max' => 50,
					'section' => 'gridchamp_extra_setup',
					'settings' => 'gridchamp_btns_radius',
					'label' => esc_html__( 'Radius of Button(Min Value 1)','gridchamp' ),
					'description' => esc_html__('This features create round/square corner of buttons all over the site','gridchamp'),
					'description' => '',
				) 
			);
		}
	}
}
add_action( 'customize_register', 'gridchamp_extra_options_register' );