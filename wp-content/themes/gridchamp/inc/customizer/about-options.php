<?php
/**
*Features customizer options
*
* @package gridchamp
*
*/

if (! function_exists('gridchamp_about_options_register')) {
	function gridchamp_about_options_register( $wp_customize ) {
		if(gridchamp_set_to_premium()){
			$gridchamp_about_layout_choices =array(
				'gridchamp-about-layout-1'    => esc_html__('Layout 1','gridchamp'),
				'gridchamp-about-layout-2'  => esc_html__('Layout 2','gridchamp'),
			);
		}else{
			$gridchamp_about_layout_choices =array(
				'gridchamp-about-layout-1'    => esc_html__('Layout 1','gridchamp'),
			);
		}
		$wp_customize->add_setting( 
	        'gridchamp_about_layout', 
	        array(
	            'default'           => 'gridchamp-about-layout-1',
	            'sanitize_callback' => 'gridchamp_sanitize_choices'
	        ) 
	    );
	    
	    $wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'gridchamp_about_layout',
				array(
					'section'	  => 'walker_about_options',
					'label'		  => esc_html__( 'Choose Section Layout', 'gridchamp' ),
					'description' => '',
					'type'        => 'select',
					'settings'	  => 'gridchamp_about_layout',
					'priority'	  => 2,
					'choices'	  => $gridchamp_about_layout_choices,
					'active_callback' => function(){
				    	return get_theme_mod( 'about_status', true );
					},
				)
			)
		);
	    if(gridchamp_set_to_premium()){
	    	$wp_customize->add_setting( 'gridchamp_about_bg_color', 
				array(
			        'default'        => '#7602d0',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
				) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'gridchamp_about_bg_color', 
				array(
			        'label'   => esc_html__( 'Background Color', 'gridchamp' ),
			        'section' => 'walker_about_options',
			        'settings'   => 'gridchamp_about_bg_color',
			        'active_callback' => 'gridchamp_counter_status_check',
			        'priority'	  => 2,
			    ) ) 
			);
			$wp_customize->add_setting( 'gridchamp_about_text_color', 
				array(
			        'default'        => '#ffffff',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
				) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'gridchamp_about_text_color', 
				array(
			        'label'   => esc_html__( 'Text Color', 'gridchamp' ),
			        'section' => 'walker_about_options',
			        'settings'   => 'gridchamp_about_text_color',
			        'active_callback' => 'gridchamp_counter_status_check',
			        'priority'	  => 2,
			    ) ) 
			);
			$wp_customize->add_setting( 'about_video_status', 
		    	array(
			      'default'  =>  false,
			      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
			  	)
		    );
			$wp_customize->add_control( 'about_video_status', 
				array(
				  'label'   => esc_html__( 'Use Video in Section', 'gridchamp' ),
				  'description' => esc_html__('This features adding video instead of features image in about section','gridchamp'),
				  'section' => 'walker_about_options',
				  'settings' => 'about_video_status',
				  'type'    => 'checkbox',
				  'active_callback' => function(){
					    	return get_theme_mod( 'about_status', true );
					},
				  'priority'	  => 2,
				)
			);
			$wp_customize->add_setting( 'about_video_url',
	          array(
	            'default'        =>'',
	            'sanitize_callback' => 'gridchamp_sanitize_text'
	          ) 
	        );
	        $wp_customize->add_control( 'about_video_url', 
	            array(
	              'label'   => esc_html__( 'Youtube Video ID', 'gridchamp' ),
	              'section' => 'walker_about_options',
	              'settings'   => 'about_video_url',
	              'type'     => 'text',
	              'priority' => 2,
	              'active_callback' => function(){
					    return get_theme_mod( 'about_video_status', true );
					},
	          )
	        );
			$wp_customize->add_setting( 'walker_counter_number', 
		    	array(
			      'default'  =>  '',
			      'sanitize_callback' => 'gridchamp_sanitize_number_absint'
			  	)
		    );
			$wp_customize->add_control( 'walker_counter_number', 
				array(
					'label'   => esc_html__( 'Counter Number 1', 'gridchamp' ),
					'description' => __('Add number to show the count in counter 1','gridchamp'),
					'section' => 'walker_about_options',
					'settings' => 'walker_counter_number',
					'priority' => 6,
					'type'    => 'number',
					'active_callback' => 'gridchamp_counter_status_check',
				)
			);
			$wp_customize->add_setting( 'walker_counter_text', 
		    	array(
			      'default'  =>  '',
			      'sanitize_callback' => 'gridchamp_sanitize_text'
			  	)
		    );
			$wp_customize->add_control( 'walker_counter_text', 
				array(
					'label'   => esc_html__( 'Counter Text 1', 'gridchamp' ),
					'description' => esc_html__('Add Text to show the count in counter 1','gridchamp'),
					'section' => 'walker_about_options',
					'settings' => 'walker_counter_text',
					'priority' => 7,
					'type'    => 'text',
					'active_callback' => 'gridchamp_counter_status_check',
				)
			);
			$wp_customize->add_setting( 'walker_counter_number_2', 
		    	array(
			      'default'  =>  '',
			      'sanitize_callback' => 'gridchamp_sanitize_number_absint'
			  	)
		    );
			$wp_customize->add_control( 'walker_counter_number_2', 
				array(
					'label'   => esc_html__( 'Counter Number 2', 'gridchamp' ),
					'description' => esc_html__('Add number to show the count in counter 2','gridchamp'),
					'section' => 'walker_about_options',
					'settings' => 'walker_counter_number_2',
					'priority' => 8,
					'type'    => 'number',
					'active_callback' => 'gridchamp_counter_status_check',
				)
			);
			$wp_customize->add_setting( 'walker_counter_text_2', 
		    	array(
			      'default'  =>  '',
			      'sanitize_callback' => 'gridchamp_sanitize_text'
			  	)
		    );
			$wp_customize->add_control( 'walker_counter_text_2', 
				array(
					'label'   => esc_html__( 'Counter Text 2', 'gridchamp' ),
					'description' => esc_html__('Add Text to show the count in counter 2','gridchamp'),
					'section' => 'walker_about_options',
					'settings' => 'walker_counter_text_2',
					'priority' => 9,
					'type'    => 'text',
					'active_callback' => 'gridchamp_counter_status_check',
				)
			);
			$wp_customize->add_setting( 'walker_counter_number_3', 
		    	array(
			      'default'  =>  '',
			      'sanitize_callback' => 'gridchamp_sanitize_number_absint'
			  	)
		    );
			$wp_customize->add_control( 'walker_counter_number_3', 
				array(
					'label'   => esc_html__( 'Counter Number 3', 'gridchamp' ),
					'description' => esc_html__('Add number to show the count in counter 3','gridchamp'),
					'section' => 'walker_about_options',
					'settings' => 'walker_counter_number_3',
					'priority' => 10,
					'type'    => 'number',
					'active_callback' => 'gridchamp_counter_status_check',
				)
			);
			$wp_customize->add_setting( 'walker_counter_text_3', 
		    	array(
			      'default'  =>  '',
			      'sanitize_callback' => 'gridchamp_sanitize_text'
			  	)
		    );
			$wp_customize->add_control( 'walker_counter_text_3', 
				array(
					'label'   => esc_html__( 'Counter Text 3', 'gridchamp' ),
					'description' => esc_html__('Add Text to show the count in counter 3','gridchamp'),
					'section' => 'walker_about_options',
					'settings' => 'walker_counter_text_3',
					'priority' => 11,
					'type'    => 'text',
					'active_callback' => 'gridchamp_counter_status_check',
				)
			);
		}
	}

	function gridchamp_counter_status_check(){
        $gridchamp_about_status_chk= get_theme_mod( 'about_status' );
        $gridchamp_about_layout_chk= get_theme_mod( 'gridchamp_about_layout' );
		$gridchamp_counter_status = false;
		if($gridchamp_about_status_chk == true && $gridchamp_about_layout_chk=='gridchamp-about-layout-2'){
			$gridchamp_counter_status = true;
		}
		return $gridchamp_counter_status;
    }
	
}
add_action( 'customize_register', 'gridchamp_about_options_register' );