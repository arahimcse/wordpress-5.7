<?php
add_action( 'customize_register', 'gridchamp_cta_register' );
	function gridchamp_cta_register( $wp_customize ) {

		$wp_customize->add_section('gridchamp_cta_settings', 
		 	array(
		        'title' => esc_html__('CTA Section', 'gridchamp'),
		        'panel' =>'gridchamp_frontpage_option',
		        'priority' => 9,
	    	)
		 );
		$wp_customize->add_setting( 'gridchamp_cta_status', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'gridchamp_cta_status', 
			array(
			  'label'   => esc_html__( 'Display CTA Section', 'gridchamp' ),
			  'section' => 'gridchamp_cta_settings',
			  'settings' => 'gridchamp_cta_status',
			  'type'    => 'checkbox',
			  'priority' => 1
			)
		);
		$wp_customize->selective_refresh->add_partial( 'gridchamp_cta_status', array(
            'selector' => '.cta-wraper h1.section-heading',
        ) );
		$wp_customize->add_setting( 'cta_bg_color', 
			array(
		        'default'        => '#7602d0',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'cta_bg_color', 
			array(
		        'label'   => esc_html__( 'Background Color', 'gridchamp' ),
		        'section' => 'gridchamp_cta_settings',
		        'settings'   => 'cta_bg_color',
		        'priority' => 1,
		        'active_callback' => function(){
		            return get_theme_mod( 'gridchamp_cta_status', true );
		        },
		    ) ) 
		);
		$wp_customize->add_setting('cta_bg_image', array(
	        'transport'         => 'refresh',
	        'sanitize_callback'     =>  'gridchamp_sanitize_file',
	    ));

	    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'cta_bg_image', array(
	        'label'             => esc_html__('Background Image', 'gridchamp'),
	        'section'           => 'gridchamp_cta_settings',
	        'settings'          => 'cta_bg_image',
	        'priority' 			=> 4,
	        'active_callback' => function(){
	            return get_theme_mod( 'gridchamp_cta_status', true );
	        },
	    )));
	     $wp_customize->add_setting( 'cta_message_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'sanitize_textarea_field',
			) 
		);

		$wp_customize->add_control( 'cta_message_text', 
			array(
				'type' => 'textarea',
				'section' => 'gridchamp_cta_settings',
				'label' => esc_html__( 'Text','gridchamp' ),
				'active_callback' => function(){
		            return get_theme_mod( 'gridchamp_cta_status', true );
		        },
			)
		);
	    
	    $wp_customize->add_setting( 'cta_btn_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',
			) 
		);

		$wp_customize->add_control( 'cta_btn_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_cta_settings',
				'label' => esc_html__( 'Button','gridchamp' ),
				'active_callback' => function(){
		            return get_theme_mod( 'gridchamp_cta_status', true );
		        },
			)
		);
		$wp_customize->add_setting( 'cta_btn_url', 
			array(
				'default' => '',
		        'sanitize_callback'     =>  'esc_url_raw',
		    ) 
		);

	    $wp_customize->add_control( 'cta_btn_url', 
	    	array(
		        'label' => esc_html__( 'Button Link', 'gridchamp' ),
		        'section' => 'gridchamp_cta_settings',
		        'settings' => 'cta_btn_url',
		        'type'=> 'url',
		        'active_callback' => function(){
		            return get_theme_mod( 'gridchamp_cta_status', true );
		        },
	    	) 
	    );
	    $wp_customize->add_setting( 'cta_btn_target', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'cta_btn_target', 
			array(
			  'label'   => esc_html__( 'Open in New Tab', 'gridchamp' ),
			  'section' => 'gridchamp_cta_settings',
			  'settings' => 'cta_btn_target',
			  'type'    => 'checkbox',
			  'active_callback' => function(){
		            return get_theme_mod( 'gridchamp_cta_status', true );
		        },
			)

		);

		
	}
?>