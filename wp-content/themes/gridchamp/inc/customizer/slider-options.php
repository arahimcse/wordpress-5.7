<?php
/**
*Slider customizer options
*
* @package walker_core
*
*/
 if(gridchamp_set_to_premium()){
	add_action( 'customize_register', 'gridchamp_slider_checks' );
	function gridchamp_slider_checks( $wp_customize)  {
	    $wp_customize->get_control('header_image')->active_callback ='gridchamp_current_hero_layout_chk';
	}
}
if (! function_exists('gridchamp_slider_options_register')) {
	function gridchamp_slider_options_register( $wp_customize ) {

	    if(gridchamp_set_to_premium()){
	        $hero_choices = array(
	            'gridchamp-banner'  => esc_html__('Banner','gridchamp'),
				'gridchamp-slider'  => esc_html__('Slider','gridchamp'),
	        );
    	}
	    else {
	        $hero_choices = array(
	            'gridchamp-banner'  => esc_html__('Banner','gridchamp'),
	        );
	    }
	    $wp_customize->add_setting( 
	        'gridchamp_hero_type', 
	        array(
	            'default'           => 'gridchamp-banner',
	            'sanitize_callback' => 'gridchamp_sanitize_choices'
	        ) 
	    );
	    $wp_customize->add_control( new WP_Customize_Control( $wp_customize,
				'gridchamp_hero_type',
				array(
					'section'	  => 'header_image',
					'label'		  => esc_html__( 'Choose Hero Section Layout Type', 'gridchamp' ),
					'description' => '',
					'type'        => 'select',
					'priority'	  => 2,
					'choices'	  => $hero_choices,
					'active_callback' => function(){
				    	return get_theme_mod( 'banner_status', true );
					},
				)
			)
		);

	    if(gridchamp_set_to_premium()){
	    	$wp_customize->selective_refresh->add_partial( 'gridchamp_hero_type', array(
	            'selector' => '.walkerwp-slider-box h1.slider-title',
	        ) );
			$wp_customize->add_setting( 
		        'gridchamp_slder_layout', 
		        array(
		            'default'           => 'gridchamp-slider-1',
		            'sanitize_callback' => 'gridchamp_sanitize_choices'
		        ) 
		    );
		    
		    $wp_customize->add_control(
				new WP_Customize_Control(
					$wp_customize,
					'gridchamp_slder_layout',
					array(
						'section'	  => 'header_image',
						'label'		  => esc_html__( 'Choose Slider Layout', 'gridchamp' ),
						'description' => '',
						'type'        => 'select',
						'priority'	  => 2,
						'choices'	  => array(
							'gridchamp-slider-1'  => esc_html__('Slider Layout 1','gridchamp'),
							'gridchamp-slider-2'  => esc_html__('Slider Layout 2','gridchamp'),
							'gridchamp-slider-3'  => esc_html__('Slider Layout 3','gridchamp'),
						),
						'active_callback' => 'gridchmap_current_hero_layout',
					)
				)
			);

			$wp_customize->add_setting( 'slider_primary_btn_color', 
				array(
			        'default'        => '#ffffff',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'slider_primary_btn_color', 
				array(
			        'label'   => esc_html__( 'Primary Button Color', 'gridchamp' ),
			        'section' => 'header_image',
			        'settings'   => 'slider_primary_btn_color',
			        'active_callback' => 'gridchmap_current_hero_layout',
			    ) ) 
			);
			$wp_customize->add_setting( 'slider_primary_btn_hover_color', 
				array(
			        'default'        => '#7602d0',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'slider_primary_btn_hover_color', 
				array(
			        'label'   => esc_html__( 'Primary Button hover Color', 'gridchamp' ),
			        'section' => 'header_image',
			        'settings'   => 'slider_primary_btn_hover_color',
			        'active_callback' => 'gridchmap_current_hero_layout',
			    ) ) 
			);
			$wp_customize->add_setting( 'slider_secondary_btn_color', 
				array(
			        'default'        => '#fc6f11',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'slider_secondary_btn_color', 
				array(
			        'label'   => esc_html__( 'Secondary Button Color', 'gridchamp' ),
			        'section' => 'header_image',
			        'settings'   => 'slider_secondary_btn_color',
			        'active_callback' => 'gridchmap_current_hero_layout',
			    ) ) 
			);
			$wp_customize->add_setting( 'slider_secondary_btn_hover_color', 
				array(
			        'default'        => '#ffffff',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
				'slider_secondary_btn_hover_color', 
				array(
			        'label'   => esc_html__( 'Secondary Button Hover Color', 'gridchamp' ),
			        'section' => 'header_image',
			        'settings'   => 'slider_secondary_btn_hover_color',
			        'active_callback' => 'gridchmap_current_hero_layout',
			    ) ) 
			);

			
		}
		
	}
	function gridchmap_current_hero_layout(){
        $hero_layout= get_theme_mod( 'gridchamp_hero_type' );
		$slider_status = false;
		if($hero_layout == 'gridchamp-slider'){
			$slider_status = true;
		}
		return $slider_status;
    }
    function gridchamp_current_hero_layout_chk(){
        $hero_layouts= get_theme_mod( 'gridchamp_hero_type' );
		$slider_statuss = false;
		if($hero_layouts == 'gridchamp-banner'){
			$slider_statuss = true;
		}
		return $slider_statuss;
    }


}
add_action( 'customize_register', 'gridchamp_slider_options_register' );
