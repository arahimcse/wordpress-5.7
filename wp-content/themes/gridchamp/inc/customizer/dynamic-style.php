<?php
    $primary_color = sanitize_hex_color( get_theme_mod( 'gridchamp_primary_color', '#7602d0' ) );
	$secondary_color = sanitize_hex_color( get_theme_mod( 'gridchamp_secondary_color', '#fc6f11' ) );
    $text_color = sanitize_hex_color( get_theme_mod( 'gridchamp_text_color', '#404040' ) );
    $dark_color = sanitize_hex_color( get_theme_mod( 'gridchamp_dark_color', '#000000' ) );
    $light_color = sanitize_hex_color( get_theme_mod( 'gridchamp_light_color', '#ffffff' ) );
    if(get_theme_mod( 'gridchamp_link_color')){
        $link_color = sanitize_hex_color( get_theme_mod( 'gridchamp_link_color', '#7602d0' ) );
    } else{
        $link_color = $primary_color;
    }
    $gridchamp_contain_width = absint(get_theme_mod('gridchamp_container_width','1180'));
    if(get_theme_mod( 'gridchamp_link_hover_color')){
        $link_hover_color = sanitize_hex_color( get_theme_mod( 'gridchamp_link_hover_color', '#fc6f11' ) );
    } else{
        $link_hover_color = $secondary_color;
    }
    if(get_theme_mod( 'gridchamp_footer_bg_color')){
        $footer_widgets_bg = sanitize_hex_color(get_theme_mod( 'gridchamp_footer_bg_color','#232323'));
    }else{
        $footer_widgets_bg = $primary_color;   
    }
    if(get_theme_mod('gridchamp_footer_text_color')){
        $footer_widget_text_color = sanitize_hex_color(get_theme_mod('gridchamp_footer_text_color'));
    }else{
        $footer_widget_text_color = $light_color;
    }
    if(get_theme_mod( 'gridchamp_footer_link_color')){
        $footer_link_color = sanitize_hex_color(get_theme_mod( 'gridchamp_footer_link_color','#7602d0'));
    }else{
        $footer_link_color = $light_color;   
    }
    if(get_theme_mod( 'gridchamp_footer_link_hover_color')){
        $footer_link__hover_color = sanitize_hex_color(get_theme_mod( 'gridchamp_footer_link_hover_color','#fc6f11'));
    }else{
        $footer_link__hover_color = $secondary_color;   
    }
    if(gridchamp_set_to_premium()){
        if(get_theme_mod('site_stikcy_menu_option')){
            $sticky_class_attr='fixed';
        }else{
            $sticky_class_attr='relative';
        }
    }else{
        $sticky_class_attr='relative';
    }
    if(gridchamp_set_to_premium()){
        if(get_theme_mod('walker_core_team_bg_color')){
            $team_bg_color =sanitize_hex_color(get_theme_mod('walker_core_team_bg_color'));
        }else{
            $team_bg_color = $primary_color;
        }
    }else{
        $team_bg_color = $primary_color;
    }

    if(gridchamp_set_to_premium()){
        if(get_theme_mod('walker_core_team_text_color')){
            $team_heading_text_color = sanitize_hex_color(get_theme_mod('walker_core_team_text_color'));
        }else{
            $team_heading_text_color= $light_color;
        }
    }else{
         $team_heading_text_color= $light_color;
    }
    if(gridchamp_set_to_premium()){
        if(get_theme_mod('gridchamp_service_bg_color')){
            $service_bg_color = sanitize_hex_color(get_theme_mod('gridchamp_service_bg_color'));
        }else{
            $service_bg_color= $primary_color;
        }
    }else{
        $service_bg_color= $primary_color;
    }
    if(gridchamp_set_to_premium()){
        if(get_theme_mod('gridchamp_service_text_color')){
            $service_text_color = sanitize_hex_color(get_theme_mod('gridchamp_service_text_color'));
        }else{
            $service_text_color= $light_color;
        }
    }else{
        $service_text_color= $light_color;
    }

    $subheader_bg_color = sanitize_hex_color(get_theme_mod('subheader_bg_color','#7602d0'));
    $subheader_text_color = sanitize_hex_color(get_theme_mod('subheader_text_color','#ffffff'));
    $breadcrumbs_link_color = sanitize_hex_color(get_theme_mod('breadcrumbs_link_color','#ffffff'));
    $breadcrumbs_link_hover_color = sanitize_hex_color(get_theme_mod('breadcrumbs_link_hover_color','#565656'));
    if(get_theme_mod('cta_bg_color')){
        $cta_background_color = sanitize_hex_color(get_theme_mod('cta_bg_color','#7602d0'));
    }else{
        $cta_background_color=$primary_color;
    }
    if(get_theme_mod('notification_bg_color')){
        $noticebar_background = sanitize_hex_color(get_theme_mod('notification_bg_color'));
    }else{
        $noticebar_background=$primary_color;
    }
    if(get_theme_mod('notification_text_color')){
        $noticebar_text_color = sanitize_hex_color(get_theme_mod('notification_text_color'));
    }else{
        $noticebar_text_color = $light_color;
    }
    if(get_theme_mod('notification_button_text_color')){
        $noticebar_btn_color = sanitize_hex_color(get_theme_mod('notification_button_text_color'));
    }else{
        $noticebar_btn_color = $light_color;
    }
    if(get_theme_mod('notification_button_text_hover_color')){
        $noticebar_btn_hover_color = sanitize_hex_color(get_theme_mod('notification_button_text_hover_color'));
    }else{
        $noticebar_btn_hover_color = $secondary_color;
    }
    if(gridchamp_set_to_premium()){
        if(get_theme_mod('gridchamp_btns_radius')){
            $site_btns_radius = absint(get_theme_mod('gridchamp_btns_radius'))-1;
        }
        else{
            $site_btns_radius = 40;
        }
    }else{
        $site_btns_radius = 40;
    }
    if(get_theme_mod('gridchamp_slider_backgroud')){
        $slider_bg_color = sanitize_hex_color(get_theme_mod('gridchamp_slider_backgroud'));
    }else{
        $slider_bg_color = $primary_color;
    }

    if(get_theme_mod('gridchamp_slider_text_color')){
        $slider_text_color = sanitize_hex_color(get_theme_mod('gridchamp_slider_text_color'));
    }else{
        $slider_text_color = $light_color;
    }

    $slider_image_opacity= get_theme_mod('banner_image_opacity');
    if(get_theme_mod('gridchamp_about_bg_color')){
        $about_bg_color = sanitize_hex_color(get_theme_mod('gridchamp_about_bg_color'));
    }else{
        $about_bg_color = $primary_color;  
    }
    if(get_theme_mod('gridchamp_about_text_color')){
        $about_text_color = sanitize_hex_color(get_theme_mod('gridchamp_about_text_color'));
    }else{
        $about_text_color = $light_color;
    }
    $site_font = esc_attr(get_theme_mod('gridchamp_body_fonts','Montserrat'));
    $copyright_background_color = sanitize_hex_color(get_theme_mod('gridchamp_footer_bottom_color','#000000'));
    $copright_text_color = sanitize_hex_color(get_theme_mod('gridchamp_copyright_text_color','#ffffff'));
    
    $slider_primary_color= sanitize_hex_color(get_theme_mod('slider_primary_btn_color','#ffffff'));
    $slider_primary_hover_color= sanitize_hex_color(get_theme_mod('slider_primary_btn_hover_color','#7602d0'));
    $slider_secondary_color= sanitize_hex_color(get_theme_mod('slider_secondary_btn_color','#fc6f11'));
    $slider_secondary_hover_color= sanitize_hex_color(get_theme_mod('slider_secondary_btn_hover_color','#ffffff'));
   
    if( $site_font ){
        switch ( $site_font) {
            case "Source Sans Pro:400,700,400italic,700italic":
                $site_font = 'Source Sans Pro';
            break;

            case "Open Sans:400italic,700italic,400,700":
                $site_font = 'Open Sans';
            break;

            case "Oswald:400,700":
                $site_font = 'Oswald';
            break;

            case "Montserrat:400,700":
                $site_font = 'Montserrat';
            break;

            case "Playfair Display:400,700,400italic":
                $site_font = 'Playfair Display';
            break;

            case "Raleway:400,700":
                $site_font = 'Raleway';
            break;

            case "Raleway:400,700":
                $site_font = 'Raleway';
            break;

            case "Droid Sans:400,700":
                $site_font = 'Droid Sans';
            break;

            case "Lato:400,700,400italic,700italic":
                $site_font = 'Lato';
            break;

            case "Arvo:400,700,400italic,700italic":
                $site_font = 'Arvo';
            break;

            case "Lora:400,700,400italic,700italic":
                $site_font = 'Lora';
            break;

            case "Merriweather:400,300italic,300,400italic,700,700italic":
                $site_font = 'Merriweather';
            break;

            case "Oxygen:400,300,700":
                $site_font = 'Oxygen';
            break;

            case "PT Serif:400,700' => 'PT Serif":
                $site_font = 'PT Serif';
            break;

            case "PT Sans:400,700,400italic,700italic":
                $site_font = 'PT Sans';
            break;

            case "PT Sans Narrow:400,700":
                $site_font = 'PT Sans Narrow';
            break;

            case "Cabin:400,700,400italic":
                $site_font = 'Cabin';
            break;

            case "Fjalla One:400":
                $site_font = 'Fjalla One';
            break;

            case "Francois One:400":
                $site_font = 'Francois One';
            break;

            case "Josefin Sans:400,300,600,700":
                $site_font = 'Josefin Sans';
            break;

            case "Libre Baskerville:400,400italic,700":
                $site_font = 'Libre Baskerville';
            break;

            case "Arimo:400,700,400italic,700italic":
                $site_font = 'Arimo';
            break;

            case "Ubuntu:400,700,400italic,700italic":
                $site_font = 'Ubuntu';
            break;

            case "Bitter:400,700,400italic":
                $site_font = 'Bitter';
            break;

            case "Droid Serif:400,700,400italic,700italic":
                $site_font = 'Droid Serif';
            break;

            case "Roboto:400,400italic,700,700italic":
                $site_font = 'Roboto';
            break;

            case "Open Sans Condensed:700,300italic,300":
                $site_font = 'Open Sans Condensed';
            break;

            case "Roboto Condensed:400italic,700italic,400,700":
                $site_font = 'Roboto Condensed';
            break;

            case "Roboto Slab:400,700":
                $site_font = 'Roboto Slab';
            break;

            case "Yanone Kaffeesatz:400,700":
                $site_font = 'Yanone Kaffeesatz';
            break;

            case "Rokkitt:400":
                $site_font = 'Rokkitt';
            break;

            default:
                $site_font = 'Roboto';
        }
    }else{
        $site_font = 'Roboto';
    }

    $heading_font = esc_attr(get_theme_mod('gridchamp_heading_fonts','Montserrat'));
    if( $heading_font ){
        switch ( $heading_font) {
            case "Source Sans Pro:400,700,400italic,700italic":
                $heading_font = 'Source Sans Pro';
            break;

            case "Open Sans:400italic,700italic,400,700":
                $heading_font = 'Open Sans';
            break;

            case "Oswald:400,700":
                $heading_font = 'Oswald';
            break;

            case "Montserrat:400,700":
                $heading_font = 'Montserrat';
            break;

            case "Playfair Display:400,700,400italic":
                $heading_font = 'Playfair Display';
            break;

            case "Raleway:400,700":
                $heading_font = 'Raleway';
            break;

            case "Raleway:400,700":
                $heading_font = 'Raleway';
            break;

            case "Droid Sans:400,700":
                $heading_font = 'Droid Sans';
            break;

            case "Lato:400,700,400italic,700italic":
                $heading_font = 'Lato';
            break;

            case "Arvo:400,700,400italic,700italic":
                $heading_font = 'Arvo';
            break;

            case "Lora:400,700,400italic,700italic":
                $heading_font = 'Lora';
            break;

            case "Merriweather:400,300italic,300,400italic,700,700italic":
                $heading_font = 'Merriweather';
            break;

            case "Oxygen:400,300,700":
                $heading_font = 'Oxygen';
            break;

            case "PT Serif:400,700' => 'PT Serif":
                $heading_font = 'PT Serif';
            break;

            case "PT Sans:400,700,400italic,700italic":
                $heading_font = 'PT Sans';
            break;

            case "PT Sans Narrow:400,700":
                $heading_font = 'PT Sans Narrow';
            break;

            case "Cabin:400,700,400italic":
                $heading_font = 'Cabin';
            break;

            case "Fjalla One:400":
                $heading_font = 'Fjalla One';
            break;

            case "Francois One:400":
                $heading_font = 'Francois One';
            break;

            case "Josefin Sans:400,300,600,700":
                $heading_font = 'Josefin Sans';
            break;

            case "Libre Baskerville:400,400italic,700":
                $heading_font = 'Libre Baskerville';
            break;

            case "Arimo:400,700,400italic,700italic":
                $heading_font = 'Arimo';
            break;

            case "Ubuntu:400,700,400italic,700italic":
                $heading_font = 'Ubuntu';
            break;

            case "Bitter:400,700,400italic":
                $heading_font = 'Bitter';
            break;

            case "Droid Serif:400,700,400italic,700italic":
                $heading_font = 'Droid Serif';
            break;

            case "Roboto:400,400italic,700,700italic":
                $heading_font = 'Roboto';
            break;

            case "Open Sans Condensed:700,300italic,300":
                $heading_font = 'Open Sans Condensed';
            break;

            case "Roboto Condensed:400italic,700italic,400,700":
                $heading_font = 'Roboto Condensed';
            break;

            case "Roboto Slab:400,700":
                $heading_font = 'Roboto Slab';
            break;

            case "Yanone Kaffeesatz:400,700":
                $heading_font = 'Yanone Kaffeesatz';
            break;

            case "Rokkitt:400":
                $heading_font = 'Rokkitt';
            break;

            default:
                $heading_font = 'Roboto';
        }
    }else{
        $heading_font = 'Roboto';
    }
    

    $site_font_size = absint(get_theme_mod('gridchamo_font_size','16'));


    $heading_font_one = absint(get_theme_mod('gridchamo_heading_one_size','48'));
    $heading_font_two = absint(get_theme_mod('gridchamo_heading_two_size','36'));
    $heading_font_three = absint(get_theme_mod('gridchamo_heading_three_size','24'));
    $heading_font_four = absint(get_theme_mod('gridchamo_heading_four_size','20'));
    $heading_font_five = absint(get_theme_mod('gridchamo_heading_five_size','16'));
    $heading_font_six = absint(get_theme_mod('gridchamo_heading_six_size','14'));
    
?>
<style type="text/css">
    :root{
        --primary-color: <?php echo $primary_color;?>;
        --secondary-color: <?php echo $secondary_color;?>;
        --text-color:<?php echo $text_color;?>;
        --dark-color:<?php echo $dark_color;?>;
        --light-color:<?php echo $light_color;?>;
        --link-color:<?php echo $link_color;?>;
        --link-hover-color:<?php echo $link_hover_color;?>;
    }

    body{
        font-family: '<?php echo $site_font;?>',sans-serif;
        font-size: <?php echo $site_font_size;?>px;
        color: var(--text-color);
    }
    
    h1, h2, h3, h4, h5,h6{
        font-family: '<?php echo $heading_font;?>',sans-serif;
    }
    h1{
        font-size: <?php echo $heading_font_one;?>px;
    }
    h2{
        font-size: <?php echo $heading_font_two;?>px;
    }
    h3{
        font-size: <?php echo $heading_font_three;?>px;
    }
    h4{
        font-size: <?php echo $heading_font_four;?>px;
    }
    h5{
        font-size: <?php echo $heading_font_five;?>px;
    }
    h6{
        font-size: <?php echo $heading_font_six;?>px;
    }
    .inner-page-subheader {
        background: <?php echo $subheader_bg_color;?>;
        color:<?php echo $subheader_text_color;?>;
    }
    .gridchamp-breadcrumbs nav ul li a {
        color:<?php echo $breadcrumbs_link_color;?>;
    }
    .gridchamp-breadcrumbs nav ul li a:hover {
        color:<?php echo $breadcrumbs_link_hover_color;?>;
    }

    .walker-wraper.footer-widgets-wraper{
        background: <?php echo $footer_widgets_bg;?>;
        color: <?php echo $footer_widget_text_color;?>;
    }
    .site-footer a{
        color :<?php echo $footer_link_color;?>;
    }
    .site-footer a:hover{
        color :<?php echo $footer_link__hover_color;?>;
    }
    .cta-wraper:before{
        background: <?php echo $cta_background_color;?>;
    }
    .walker-wraper.top-notification-bar{
        background: <?php echo $noticebar_background;?>;
        color: <?php echo $noticebar_text_color;?>;
    }
    a.noticebar-button{
        color: <?php echo $noticebar_btn_color;?>;
    }
    a.noticebar-button:hover{
        color: <?php echo $noticebar_btn_hover_color;?>;
    }
    .gridchamp-primary-button,
    .gridchamp-secondary-button,
    a.gridchamp-primary-button,
    a.gridchamp-secondary-button{
        border-radius: <?php echo $site_btns_radius.'px';?>;
    }
    .walker-wraper.banner-wraper,
    .walker-wraper.slider-wraper{
        background: <?php echo $slider_bg_color;?>;
    }
    .banner-overlay, 
    .slide-content{
        color: <?php echo $slider_text_color;?>;
    }
    .walker-container{
        max-width: <?php echo  $gridchamp_contain_width.'px';?>;
    }
    .banner-wraper img,
    .walkerwp-slider-box .slide-image img{
        opacity: <?php echo $slider_image_opacity; ?>;
    }
    <?php if(gridchamp_set_to_premium()){?>
        .active-sticky,
        .walker-wraper.header-layout-1.no-gap.transparent-header.active-sticky {
            position: <?php echo $sticky_class_attr;?>;
            box-shadow: 0 15px 7px -13px rgb(0 0 0 / 8%);
            background: var(--light-color);
        }
    <?php } ?>
    .walker-wraper.about-wraper.about-layout-2{
        background:<?php echo $about_bg_color;?>;
        color:<?php echo $about_text_color;?>;
    }
     .walker-wraper.about-wraper.about-layout-2 .section-heading:after{
        background: <?php echo $about_text_color;?>;
    }
    .walker-wraper.about-wraper.about-layout-2 h5.about-title{
        color:<?php echo $about_text_color;?>;
    }
    .walker-wraper.about-wraper.about-layout-2 a.gridchamp-secondary-button.style-outline-light{
        color:<?php echo $about_text_color;?>;
        border-color: <?php echo $about_text_color;?>;
    }
    .walker-wraper.about-wraper.about-layout-2 a.gridchamp-secondary-button.style-outline-light:hover{
        color: <?php echo $secondary_color;?>;
    }
    .walker-wraper.team-wraper.team-layout-2 .overlay-img{
        background:<?php echo $team_bg_color;?>;
    }
    .walker-wraper.team-wraper.team-layout-2 .team-heading{
        color:<?php echo $team_heading_text_color;?>;
    }
    .walker-wraper.team-wraper.team-layout-2 .section-heading.text-center:after{
        background: <?php echo $team_heading_text_color;?>;
    }
    .service-layou-1.service-wraper:before{
        background:<?php echo $service_bg_color;?>;
    }
    .service-layou-1 .walker-container.service-header {
        color: <?php echo $service_text_color;?>;
    }
    .service-layou-1 .walker-container.service-header .section-heading:after{
        background: <?php echo $service_text_color;?>;
    }
    .service-layou-1 a.gridchamp-secondary-button.style-outline-light{
        color:<?php echo $service_text_color;?>;
        border-color: <?php echo $service_text_color;?>;
    }
    .service-layou-1 a.gridchamp-secondary-button.style-outline-light:hover{
        color: <?php echo $secondary_color;?>;
    }
    .walker-wraper.footer-copyright-section{
        background: <?php echo $copyright_background_color;?>;
        color:<?php echo $copright_text_color?>;
    }
    .walkerwp-slider-box a.gridchamp-primary-button{
        border:1px solid <?php echo $slider_primary_color;?>;
        color: <?php echo $slider_primary_color;?>;
        background: transparent;

    }
    .walkerwp-slider-box a.gridchamp-primary-button:hover{
        background:<?php echo $slider_primary_color;?>;
        color: <?php echo $slider_primary_hover_color;?>;
    }

    .walkerwp-slider-box a.gridchamp-secondary-button{
        border:1px solid <?php echo $slider_secondary_color;?>;
        color: <?php echo $slider_secondary_hover_color;?>;
        background: <?php echo $slider_secondary_color;?>;

    }
    .walkerwp-slider-box a.gridchamp-secondary-button:hover{
        border-color:<?php echo $slider_secondary_hover_color;?>;
        background:<?php echo $slider_secondary_hover_color;?>;
        color: <?php echo $slider_secondary_color;?>;
    }
</style>
