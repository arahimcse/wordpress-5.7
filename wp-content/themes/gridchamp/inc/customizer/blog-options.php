<?php
/**
*Blog customizer options
*
* @package Gridchamp
*
*/

if (! function_exists('gridchamp_blog_options_register')) {
	function gridchamp_blog_options_register( $wp_customize ) {
		/** Blog Layout */
	    $wp_customize->add_section(
	        'gridchamp_blog_layout',
	        array(
	            'title'    => esc_html__( 'Blog', 'gridchamp' ),
	            'panel'    => 'gridchamp_theme_option',
	            'priority' => 4,
	        )
	    );
	    
	    /** blog Sidebar layout */
	    $wp_customize->add_setting( 
	        'blog_sidebar_layout',
	        array(
	            'default'           => 'right-sidebar',
	            'sanitize_callback' => 'gridchamp_sanitize_choices'
	        ) 
	    );
	    
	    $wp_customize->add_control(
			new Gridchamp_Radio_Image_Control_Horizontal(
				$wp_customize,
				'blog_sidebar_layout',
				array(
					'section'	  => 'gridchamp_blog_layout',
					'label'		  => esc_html__( 'Choose Blog Layout', 'gridchamp' ),
					'description' => '',
					'priority' => 1,
					'choices'	  => array(
						'no-sidebar'    => esc_url( get_template_directory_uri() . '/images/full.png' ),
						'left-sidebar'  => esc_url( get_template_directory_uri() . '/images/left.png' ),
	                    'right-sidebar' => esc_url( get_template_directory_uri() . '/images/right.png' ),
					)
				)
			)
		);

		$wp_customize->add_setting( 'gridchamo_excerpt_length', 
			array(
				'capability' => 'edit_theme_options',
				'sanitize_callback' => 'gridchamp_sanitize_number_absint',
				'default' => 30,
			) 
		);

		$wp_customize->add_control( 'gridchamo_excerpt_length', 
			array(
				'type' => 'number',
				'section' => 'gridchamp_blog_layout',
				'settings' => 'gridchamo_excerpt_length',
				'label' => esc_html__( 'Excerpt Length (Words)','gridchamp' ),
				'description' => '',
				'priority' => 2,
			) 
		);
		$wp_customize->add_setting( 'gridchamp_excerpt_more', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' =>'',
				'sanitize_callback' => 'gridchamp_sanitize_text',

			) 
		);
		$wp_customize->add_control( 'gridchamp_excerpt_more', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_blog_layout',
				'label' => esc_html__( 'Read More Text','gridchamp' ),
				'priority' => 3,
			)
		);

		
	}

}
add_action( 'customize_register', 'gridchamp_blog_options_register' );