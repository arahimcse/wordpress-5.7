<?php
/**
*Typography customizer options
*
* @package Gridchamp
*
*/
add_action( 'customize_register', 'gridchamp_color_settings_panel' );

function gridchamp_color_settings_panel( $wp_customize)  {
    $wp_customize->get_section('colors')->priority = 1;
    $wp_customize->get_section( 'colors' )->title  = esc_html__('Color Options', 'gridchamp');
    $wp_customize->get_section('colors')->panel = 'gridchamp_theme_option';
}
if (! function_exists('gridchamp_colors_options_register')) {
	function gridchamp_colors_options_register( $wp_customize ) {
	
		$wp_customize->add_setting( 'gridchamp_primary_color', 
			array(
		        'default'        => '#7602d0',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'gridchamp_primary_color', 
			array(
		        'label'   => esc_html__( 'Primary Color', 'gridchamp' ),
		        'section' => 'colors',
		        'settings'   => 'gridchamp_primary_color',
		        'priority' => 1
		    ) ) 
		);
		$wp_customize->add_setting( 'gridchamp_secondary_color', 
			array(
		        'default'        => '#fc6f11',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'gridchamp_secondary_color', 
			array(
		        'label'   => esc_html__( 'Secondary Color', 'gridchamp' ),
		        'section' => 'colors',
		        'settings'   => 'gridchamp_secondary_color',
		        'priority' => 2
		    ) ) 
		);
		$wp_customize->add_setting( 'gridchamp_text_color', 
			array(
		        'default'        => '#404040',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'gridchamp_text_color', 
			array(
		        'label'   => esc_html__( 'Text Color', 'gridchamp' ),
		        'section' => 'colors',
		        'settings'   => 'gridchamp_text_color',
		        'priority' => 3
		    ) ) 
		);
		$wp_customize->add_setting( 'gridchamp_dark_color', 
			array(
		        'default'        => '#000000',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'gridchamp_dark_color', 
			array(
		        'label'   => esc_html__( 'Dark Color', 'gridchamp' ),
		        'section' => 'colors',
		        'settings'   => 'gridchamp_dark_color',
		        'priority' => 4
		    ) ) 
		);
		$wp_customize->add_setting( 'gridchamp_light_color', 
			array(
		        'default'        => '#ffffff',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'gridchamp_light_color', 
			array(
		        'label'   => esc_html__( 'Light Color', 'gridchamp' ),
		        'section' => 'colors',
		        'settings'   => 'gridchamp_light_color',
		        'priority' => 5
		    ) ) 
		);
		$wp_customize->add_setting( 'gridchamp_link_color', 
			array(
		        'default'        => '#7602d0',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'gridchamp_link_color', 
			array(
		        'label'   => esc_html__( 'Link Color', 'gridchamp' ),
		        'section' => 'colors',
		        'settings'   => 'gridchamp_link_color',
		        'priority' => 6
		    ) ) 
		);
		$wp_customize->add_setting( 'gridchamp_link_hover_color', 
			array(
		        'default'        => '#fc6f11',
		        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
	    	) 
		);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'gridchamp_link_hover_color', 
			array(
		        'label'   => esc_html__( 'Link Hover Color', 'gridchamp' ),
		        'section' => 'colors',
		        'settings'   => 'gridchamp_link_hover_color',
		        'priority' => 7
		    ) ) 
		);
		
		
	}

}
add_action( 'customize_register', 'gridchamp_colors_options_register' );