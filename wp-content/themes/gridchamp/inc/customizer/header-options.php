<?php
/**
*Header customizer options
*
* @package Gridchamp
*
*/

if (! function_exists('gridchamp_header_options_register')) {
	function gridchamp_header_options_register( $wp_customize ) {
		/** Header Layout */
	    $wp_customize->add_section(
	        'gridchamp_header_layout',
	        array(
	            'title'    => esc_html__( 'Header', 'gridchamp' ),
	            'panel'    => 'gridchamp_theme_option',
	            'priority' => 1,
	        )
	    );
	    if(gridchamp_set_to_premium()){
		    $wp_customize->add_setting( 'site_stikcy_menu_option', 
		    	array(
			      'default'  =>  false,
			      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
			  	)
		    );
			$wp_customize->add_control( 'site_stikcy_menu_option', 
				array(
				  'label'   => esc_html__( 'Enable Stikcy Menu', 'gridchamp' ),
				  'section' => 'gridchamp_header_layout',
				  'settings' => 'site_stikcy_menu_option',
				  'type'    => 'checkbox',
				  'priority' => 1,
				)
			);
		}
		$wp_customize->add_setting( 'gridchamp_topbar_status', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'gridchamp_topbar_status', 
			array(
			  'label'   => esc_html__( 'Enable Topbar', 'gridchamp' ),
			  'section' => 'gridchamp_header_layout',
			  'settings' => 'gridchamp_topbar_status',
			  'type'    => 'checkbox',
			  'active_callback' => 'gridchamp_topbar_status_check',
			)
		);

		$wp_customize->add_setting( 'gridchamp_transparent_header', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'gridchamp_transparent_header', 
			array(
			  'label'   => esc_html__( 'Enable Transparent Header', 'gridchamp' ),
			  'section' => 'gridchamp_header_layout',
			  'settings' => 'gridchamp_transparent_header',
			  'type'    => 'checkbox',
			  'active_callback' => 'gridchamp_transparent_header_check',
			)
		);
	    
	    /** header layout layout */
	    $wp_customize->add_setting( 
	        'gridchamp_select_header_layout', 
	        array(
	            'default'           => 'gridchamp-header-one',
	            'sanitize_callback' => 'gridchamp_sanitize_choices'
	        ) 
	    );

	    if(gridchamp_set_to_premium()){
	    	$header_choices = array(
				'gridchamp-header-one'  => esc_url( get_template_directory_uri() . '/images/header_one.png' ),
				'gridchamp-header-two'  => esc_url( get_template_directory_uri() . '/images/header_two.png' ),
                'gridchamp-header-three' => esc_url( get_template_directory_uri() . '/images/header_three.png' ),
                'gridchamp-header-four' => esc_url( get_template_directory_uri() . '/images/header_four.png' ),
                'gridchamp-header-five' => esc_url( get_template_directory_uri() . '/images/header_five.png' ),
			);
	    }else{
	    	$header_choices = array(
				'gridchamp-header-one'  => esc_url( get_template_directory_uri() . '/images/header_one.png' ),
			);
	    }
	    
	    $wp_customize->add_control(
			new Walker_Radio_Image_Control_Vertical(
				$wp_customize,
				'gridchamp_select_header_layout',
				array(
					'section'	  => 'gridchamp_header_layout',
					'label'		  => esc_html__( 'Choose Header Layout', 'gridchamp' ),
					'description' => '',
					'choices'	  => $header_choices,
				)
			)
		);
	    $wp_customize->add_setting( 'gridchamp_header_primary_button', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',

			) 
		);
		$wp_customize->add_control( 'gridchamp_header_primary_button', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_header_layout',
				'label' => esc_html__( 'Primary Button Text','gridchamp' ),
				'active_callback' => 'gridchamp_current_header_primary',
			)
		);
		$wp_customize->add_setting( 'primary_btn_link', 
			array(
		        'sanitize_callback'     =>  'esc_url_raw',
		    ) 
		);

	    $wp_customize->add_control( 'primary_btn_link', 
	    	array(
		        'label' => esc_html__( 'Primary Button Link', 'gridchamp' ),
		        'section' => 'gridchamp_header_layout',
		        'settings' => 'primary_btn_link',
		        'type'=> 'url',
		        'active_callback' => 'gridchamp_current_header_primary',
	    	) 
	    );
	    $wp_customize->add_setting( 'primary_btn_link_target', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'primary_btn_link_target', 
			array(
			  'label'   => esc_html__( 'Open in New Tab', 'gridchamp' ),
			  'section' => 'gridchamp_header_layout',
			  'settings' => 'primary_btn_link_target',
			  'type'    => 'checkbox',
			  'active_callback' => 'gridchamp_current_header_primary',
			)
		);
		
		$wp_customize->add_setting( 'gridchamp_header_secondary_button', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',

			) 
		);
		
		$wp_customize->add_control( 'gridchamp_header_secondary_button', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_header_layout',
				'label' => esc_html__( 'Secondary Button Text','gridchamp' ),
				'active_callback' => 'gridchamp_current_header',
			)
		);
		$wp_customize->add_setting( 'secondary_btn_link', 
			array(
		        'sanitize_callback'     =>  'esc_url_raw',
		    ) 
		);

	    $wp_customize->add_control( 'secondary_btn_link', 
	    	array(
		        'label' => esc_html__( 'Secondary Button Link', 'gridchamp' ),
		        'section' => 'gridchamp_header_layout',
		        'settings' => 'secondary_btn_link',
		        'type'=> 'url',
		        'active_callback' => 'gridchamp_current_header',
	    	) 
	    );
		$wp_customize->add_setting( 'secondary_btn_link_target', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'secondary_btn_link_target', 
			array(
			  'label'   => esc_html__( 'Open in New Tab', 'gridchamp' ),
			  'section' => 'gridchamp_header_layout',
			  'settings' => 'secondary_btn_link_target',
			  'type'    => 'checkbox',
			  'active_callback' => 'gridchamp_current_header',
			)
		);
		$wp_customize->add_setting( 'gridchamp_header_slogan_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',

			) 
		);
		$wp_customize->add_control( 'gridchamp_header_slogan_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_header_layout',
				'label' => esc_html__( 'Header Slogan Text','gridchamp' ),
				'active_callback' => 'gridchamp_current_header_check',
				
			)
		);
		$wp_customize->selective_refresh->add_partial( 'gridchamp_header_slogan_text', array(
            'selector' => 'span.header-slogan',
        ) );
		$wp_customize->add_setting( 'gridchamp_header_contact', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',

			) 
		);
		$wp_customize->add_control( 'gridchamp_header_contact', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_header_layout',
				'label' => esc_html__( 'Header Contact Number','gridchamp' ),
				'active_callback' => 'gridchamp_current_header_check',
				
			)
		);
	    
	}
	function gridchamp_current_header(){
        $choice_header= get_theme_mod( 'gridchamp_select_header_layout' );
		$header_status = false;
		if($choice_header == 'gridchamp-header-three' || $choice_header == 'gridchamp-header-five'){
			$header_status = true;
		}
		return $header_status;
    }
	
    function gridchamp_current_header_check(){
        $choice_header= get_theme_mod( 'gridchamp_select_header_layout' );
		$header_status = false;
		if($choice_header == 'gridchamp-header-one' || $choice_header == 'gridchamp-header-two' || $choice_header == 'gridchamp-header-three' || $choice_header == 'gridchamp-header-four'){
			$header_status = true;
		}
		return $header_status;
    }

    function gridchamp_topbar_status_check(){
        $choice_header= get_theme_mod( 'gridchamp_select_header_layout' );
		$header_status = false;
		if($choice_header == 'gridchamp-header-one' || $choice_header == 'gridchamp-header-three' || $choice_header == 'gridchamp-header-four'){
			$header_status = true;
		}
		return $header_status;
    }

    function gridchamp_current_header_primary(){
	    $choice_header= get_theme_mod( 'gridchamp_select_header_layout' );
		$header_status = false;
		if($choice_header == 'gridchamp-header-one' || $choice_header == 'gridchamp-header-two' || $choice_header == 'gridchamp-header-three' || $choice_header == 'gridchamp-header-five'){
			$header_status = true;
		}
		return $header_status;
	}

	function gridchamp_transparent_header_check(){
	    $choice_header= get_theme_mod( 'gridchamp_select_header_layout' );
		$transparent_header_status = false;
		if($choice_header == 'gridchamp-header-one'){
			$transparent_header_status = true;
		}
		return $transparent_header_status;
	}

}
add_action( 'customize_register', 'gridchamp_header_options_register' );