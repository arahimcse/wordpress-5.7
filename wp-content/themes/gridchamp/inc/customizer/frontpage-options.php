<?php
/**
*Frontpage  customizer options
*
* @package Gridchamp
*
*/

add_action( 'customize_register', 'gridchamp_banner_settings_panel' );

function gridchamp_banner_settings_panel( $wp_customize)  {
    $wp_customize->get_section('header_image')->priority = 1;
    $wp_customize->get_section( 'header_image')->title  = esc_html__('Hero Section Options', 'gridchamp');
    $wp_customize->get_section('header_image')->panel = 'gridchamp_frontpage_option';
}
if (! function_exists('gridchamp_frontpage_options_register')) {
	function gridchamp_frontpage_options_register( $wp_customize ) {
	//Banner/Slider
		$wp_customize->add_setting( 'banner_status', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'banner_status', 
			array(
			  'label'   => esc_html__( 'Enable Hero Section', 'gridchamp' ),
			  'section' => 'header_image',
			  'settings' => 'banner_status',
			  'type'    => 'checkbox',
			  'priority' => 1,
			)
		);
		$wp_customize->selective_refresh->add_partial( 'banner_status', array(
            'selector' => '.banner-overlay-content h1',
        ) );
		$wp_customize->add_setting( 'banner_status_allpage', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'banner_status_allpage', 
			array(
			  'label'   => esc_html__( 'Enable hero section all over site', 'gridchamp' ),
			  'section' => 'header_image',
			  'settings' => 'banner_status_allpage',
			  'type'    => 'checkbox',
			  'priority' => 1,
			)
		);



		$wp_customize->add_setting( 'gridchamp_slider_backgroud', 
				array(
			        'default'        => '#7602d0',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'gridchamp_slider_backgroud', 
			array(
		        'label'   => esc_html__( 'Background Color', 'gridchamp' ),
		        'section' => 'header_image',
		        'settings'   => 'gridchamp_slider_backgroud',
		        'priority' => 2,
		        'active_callback' => function(){
				    return get_theme_mod( 'banner_status', true );
				},
		    ) ) 
		);
		$wp_customize->add_setting( 'banner_image_opacity', 
			array(
				'capability' => 'edit_theme_options',
				'sanitize_callback' => 'gridchamp_sanitize_number_range',
				'default' => '',
			) 
		);

		$wp_customize->add_control( 'banner_image_opacity', 
			array(
				'section' => 'header_image',
				'settings' => 'banner_image_opacity',
				'label' => esc_html__( 'Opacity of slider/banner image','gridchamp' ),
				'description' => esc_html__('This features eanble transparent featurs of image','gridchamp'),
				'type'              => 'range',
			    'input_attrs'       => array(
			        'min'           => 0,
			        'max'           => 1.000,
			        'step'          => 0.001,
			    ),
			    'active_callback' => function(){
				    return get_theme_mod( 'banner_status', true );
				},
			) 
		);
		$wp_customize->add_setting( 'gridchamp_slider_text_color', 
				array(
			        'default'        => '#ffffff',
			        'sanitize_callback' => 'gridchamp_sanitize_hex_color',
		    	) 
			);

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 
			'gridchamp_slider_text_color', 
			array(
		        'label'   => esc_html__( 'Text Color', 'gridchamp' ),
		        'section' => 'header_image',
		        'settings'   => 'gridchamp_slider_text_color',
		        'priority' => 2,
		        'active_callback' => function(){
				    return get_theme_mod( 'banner_status', true );
				},
		    ) ) 
		);
		$wp_customize->add_setting( 
	        'walker_slder_text_align', 
	        array(
	            'default'           => 'slide-text-align-left',
	            'sanitize_callback' => 'gridchamp_sanitize_choices'
	        ) 
	    );
	    
	    $wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'walker_slder_text_align',
				array(
					'section'	  => 'header_image',
					'label'		  => esc_html__( 'Section Text Alignment', 'gridchamp' ),
					'description' => '',
					'type'        => 'select',
					'priority'	  => 2,
					'choices'	  => array(
						'slide-text-align-left'  => esc_html__('Left','gridchamp'),
						'slide-text-align-center'  => esc_html__('Center','gridchamp'),
						'slide-text-align-right'  => esc_html__('Right','gridchamp'),
					),
					'active_callback' => function(){
					    return get_theme_mod( 'banner_status', true );
					},
					
				)
			)
		);
		$wp_customize->add_setting( 'banner_heading_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',
			) 
		);

		$wp_customize->add_control( 'banner_heading_text', 
			array(
				'type' => 'text',
				'section' => 'header_image',
				'label' => esc_html__( 'Heading','gridchamp' ),
				'description' => esc_html__( 'Heading text for display on hero section','gridchamp' ),
				'active_callback' => 'banner_btns_status_check',
			)
		);
		$wp_customize->add_setting( 'banner_desc_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'sanitize_textarea_field',
			) 
		);

		$wp_customize->add_control( 'banner_desc_text', 
			array(
				'type' => 'textarea',
				'section' => 'header_image',
				'label' => esc_html__( 'Description','gridchamp' ),
				'description' => esc_html__( 'Short Description text for display on hero section','gridchamp' ),
				'active_callback' => 'banner_btns_status_check',
			)
		);
		$wp_customize->add_setting( 'gridchamp_banner_button_one', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',

			) 
		);
		$wp_customize->add_control( 'gridchamp_banner_button_one', 
			array(
				'type' => 'text',
				'section' => 'header_image',
				'label' => esc_html__( 'Button 1','gridchamp' ),
				'active_callback' => 'banner_btns_status_check',
				'settings' => 'gridchamp_banner_button_one',
			)
		);
		$wp_customize->add_setting( 'banner_button_one_link', 
			array(
		        'sanitize_callback'     =>  'esc_url_raw',
		    ) 
		);

	    $wp_customize->add_control( 'banner_button_one_link', 
	    	array(
		        'label' => esc_html__( 'Button 1 Link', 'gridchamp' ),
		        'section' => 'header_image',
		        'settings' => 'banner_button_one_link',
		        'type'=> 'url',
		        'active_callback' => 'banner_btns_status_check',
	    	) 
	    );
	    $wp_customize->add_setting( 'banner_button_one_target', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'banner_button_one_target', 
			array(
			  'label'   => esc_html__( 'Open in New Tab', 'gridchamp' ),
			  'section' => 'header_image',
			  'settings' => 'banner_button_one_target',
			  'type'    => 'checkbox',
			  'active_callback' => 'banner_btns_status_check',
			)
		);

		
		$wp_customize->add_setting( 'gridchamp_banner_button_2', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',


			) 
		);
		$wp_customize->add_control( 'gridchamp_banner_button_2', 
			array(
				'type' => 'text',
				'section' => 'header_image',
				'label' => esc_html__( 'Button 2','gridchamp' ),
				'active_callback' => 'banner_btns_status_check',
			)
		);
		$wp_customize->add_setting( 'banner_button_two_link', 
			array(
		        'sanitize_callback'     =>  'esc_url_raw',
		    ) 
		);

	    $wp_customize->add_control( 'banner_button_two_link', 
	    	array(
		        'label' => esc_html__( 'Button 2 Link', 'gridchamp' ),
		        'section' => 'header_image',
		        'settings' => 'banner_button_two_link',
		        'type'=> 'url',
		        'active_callback' => 'banner_btns_status_check',
	    	) 
	    );
	    $wp_customize->add_setting( 'banner_button_two_target', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'banner_button_two_target', 
			array(
			  'label'   => esc_html__( 'Open in New Tab', 'gridchamp' ),
			  'section' => 'header_image',
			  'settings' => 'banner_button_two_target',
			  'type'    => 'checkbox',
			  'active_callback' => 'banner_btns_status_check',
			)
		);


		//Services
		$wp_customize->add_section('gridchamp_services_options', 
		 	array(
		        'title' => esc_html__('Services', 'gridchamp'),
		        'panel' =>'gridchamp_frontpage_option',
		        'priority' => 2,
		        'divider' => 'before',
	    	)
		 );
		$wp_customize->add_setting( 'services_status', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'services_status', 
			array(
			  'label'   => esc_html__( 'Enable Services', 'gridchamp' ),
			  'section' => 'gridchamp_services_options',
			  'settings' => 'services_status',
			  'type'    => 'checkbox',
			   'priority' => 1,
			)
		);
		$wp_customize->selective_refresh->add_partial( 'services_status', array(
            'selector' => '.service-wraper h1.section-heading',
        ) );
		$wp_customize->add_setting( 'gridchamp_service_page', array(
		        'default' => '',
		        'capability' => 'edit_theme_options',
		        'sanitize_callback' =>'gridchamp_sanitize_text'
		        ));
		    $wp_customize->add_control(
				new Gridchamp_Dropdown_Pages_Control($wp_customize, 
				'gridchamp_service_page',
			    	array(
						'label'    => esc_html__( 'Select Parent Page', 'gridchamp' ),
						'description' => esc_html__('Select parent page of services to display on services section.','gridchamp'),
						'section'  => 'gridchamp_services_options',
						'type'     => 'dropdown-pages',
						'settings' => 'gridchamp_service_page',
						'active_callback' => function(){
						    return get_theme_mod( 'services_status', true );
						},
						'priority' => 1,
			    	) 
		    	)
		    );	
		$wp_customize->add_setting( 'services_heading_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',
			) 
		);
		$wp_customize->add_control( 'services_heading_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_services_options',
				'label' => esc_html__( 'Heading','gridchamp' ),
				'description' => '',
				 'priority' => 2,
				'active_callback' => function(){
				    return get_theme_mod( 'services_status', true );
				},
			)
		);
		$wp_customize->add_setting( 'services_desc_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control( 'services_desc_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_services_options',
				'label' => esc_html__( 'Description','gridchamp' ),
				'description' => '',
				'priority' => 3,
				'active_callback' => function(){
				    return get_theme_mod( 'services_status', true );
				},
			)
		);
		$wp_customize->add_setting( 'services_readmore_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',
			) 
		);
		$wp_customize->add_control( 'services_readmore_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_services_options',
				'label' => esc_html__( 'Read More Text','gridchamp' ),
				'description' => '',
				'priority' => 3,
				'active_callback' => function(){
				    return get_theme_mod( 'services_status', true );
				},
			)
		);
		$wp_customize->add_setting( 'services_more_list_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',
			) 
		);
		$wp_customize->add_control( 'services_more_list_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_services_options',
				'label' => esc_html__( 'View all Services Text','gridchamp' ),
				'description' => '',
				'priority' => 3,
				'active_callback' => function(){
				    return get_theme_mod( 'services_status', true );
				},
			)
		);
		$wp_customize->add_setting( 'services_more_list_text_link', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' =>'',
				'sanitize_callback' => 'gridchamp_sanitize_url',
			) 
		);
		$wp_customize->add_control( 'services_more_list_text_link', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_services_options',
				'label' => esc_html__( 'View all Services button link','gridchamp' ),
				'description' => '',
				'priority' => 3,
				'active_callback' => function(){
				    return get_theme_mod( 'services_status', true );
				},
			)
		);
		//About
		$wp_customize->add_section('walker_about_options', 
		 	array(
		        'title' => esc_html__('About Us', 'gridchamp'),
		        'panel' =>'gridchamp_frontpage_option',
		        'priority' => 3,
		        'divider' => 'before',
	    	)
		 );
		$wp_customize->add_setting( 'about_status', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'about_status', 
			array(
			  'label'   => esc_html__( 'Enable About Section', 'gridchamp' ),
			  'section' => 'walker_about_options',
			  'settings' => 'about_status',
			  'type'    => 'checkbox',
			  'priority' => 1,
			)
		);
		$wp_customize->selective_refresh->add_partial( 'about_status', array(
            'selector' => '.about-wraper h5.about-title',
        ) );
		$wp_customize->add_setting( 'about_heading_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'sanitize_text_field',

			) 
		);
		$wp_customize->add_control( 'about_heading_text', 
			array(
				'type' => 'text',
				'section' => 'walker_about_options',
				'label' => esc_html__( 'Heading','gridchamp' ),
				'description' => esc_html__( 'Add the text for heading of  about us section.','gridchamp' ),
				'active_callback' => function(){
				    return get_theme_mod( 'about_status', true );
				},
				'priority' =>2,
			)
		);
	    $wp_customize->add_setting('about_page',
		    array(
		        'default'           => '',
		        'capability'        => 'edit_theme_options',
		        'sanitize_callback' => 'gridchamp_sanitize_text',
		    )
			);
			$wp_customize->add_control(
				new Gridchamp_Dropdown_Pages_Control($wp_customize, 
				'about_page',
				    array(
				        'label'       => esc_html__('Select Page', 'gridchamp'),
				        'description' => esc_html__('Select page to be shown on home page about section', 'gridchamp'),
				        'section'     => 'walker_about_options',
				        'type'        => 'dropdown-pages',
				        'settings'	  => 'about_page',
				        'priority'    => 2,
				        'active_callback' => function(){
								return get_theme_mod( 'about_status', true );
						},
			    	)
				)
			);

	    $wp_customize->add_setting( 'about_readmore_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',
			) 
		);
		$wp_customize->add_control( 'about_readmore_text', 
			array(
				'type' => 'text',
				'section' => 'walker_about_options',
				'label' => esc_html__( 'Read More Text','gridchamp' ),
				'description' => esc_html__( 'Add the text for read more button in about us section.','gridchamp' ),
				'active_callback' => function(){
				    return get_theme_mod( 'about_status', true );
				},
				'priority' => 4,
			)
		);
		//Testimonial
		$wp_customize->add_section('gridchamp_testimonial_options', 
		 	array(
		        'title' => esc_html__('Testimonial', 'gridchamp'),
		        'panel' =>'gridchamp_frontpage_option',
		        'priority' => 4,
		        'divider' => 'before',
	    	)
		 );
		$wp_customize->add_setting( 'testimonial_status', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'testimonial_status', 
			array(
			  'label'   => esc_html__( 'Enable Testimonial', 'gridchamp' ),
			  'section' => 'gridchamp_testimonial_options',
			  'settings' => 'testimonial_status',
			  'type'    => 'checkbox',
			  'priority' => 1,
			)
		);
		$wp_customize->selective_refresh->add_partial( 'testimonial_status', array(
            'selector' => '.testimonial-wraper h1.section-heading',
        ) );
        $wp_customize->add_setting( 'testimonials_total_items', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_number_absint',
			) 
		);
		$wp_customize->add_control( 'testimonials_total_items', 
			array(
				'type' => 'number',
				'section' => 'gridchamp_testimonial_options',
				'label' => esc_html__( 'Total Testimonials to Show','gridchamp' ),
				'description' => esc_html__( 'Total testimonials to be shown on homepage testimonials section','gridchamp' ),
				 'priority' => 2,
				'active_callback' => function(){
				    return get_theme_mod( 'testimonial_status', true );
				},
			)
		);
		$wp_customize->add_setting( 'testimonial_heading_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',
			) 
		);
		
		$wp_customize->add_control( 'testimonial_heading_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_testimonial_options',
				'label' => esc_html__( 'Heading','gridchamp' ),
				'description' => esc_html__( 'Heading text for display on testimonial section','gridchamp' ),
				'active_callback' => function(){
				    return get_theme_mod( 'testimonial_status', true );
				},
			)
		);
		$wp_customize->add_setting( 'testimonial_desc_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control( 'testimonial_desc_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_testimonial_options',
				'label' => esc_html__( 'Description','gridchamp' ),
				'description' => esc_html__( 'Short Description text for display on testimonial section','gridchamp' ),
				'active_callback' => function(){
				    return get_theme_mod( 'testimonial_status', true );
				},
			)
		);
		if(!gridchamp_set_to_premium()){
			$wp_customize->add_setting('gridchamp_testimonial_category',
		    array(
		        'default'           => '',
		        'capability'        => 'edit_theme_options',
		        'sanitize_callback' => 'gridchamp_sanitize_text',
		    )
			);
			$wp_customize->add_control(
				new Gridchamp_Dropdown_Taxonomies_Control($wp_customize, 
				'gridchamp_testimonial_category',
				    array(
				        'label'       => esc_html__('Select Category', 'gridchamp'),
				        'description' => esc_html__('Select category to be shown on home page testimonial section', 'gridchamp'),
				        'section'     => 'gridchamp_testimonial_options',
				        'type'        => 'dropdown-taxonomies',
				        'settings'	  => 'gridchamp_testimonial_category',
				        'taxonomy'    => 'category',
				        'priority'    => 20,
				        'active_callback' => function(){
								return get_theme_mod( 'testimonial_status', true );
						},
			    	)
				)
			);
		}
		

		//Blog
		$wp_customize->add_section('gridchamp_recentpost_options', 
		 	array(
		        'title' => esc_html__('Recent Posts', 'gridchamp'),
		        'panel' =>'gridchamp_frontpage_option',
		        'priority' => 5,
		        'divider' => 'before',
	    	)
		 );
		$wp_customize->add_setting( 'recent_post_status', 
	    	array(
		      'default'  =>  false,
		      'sanitize_callback' => 'gridchamp_sanitize_checkbox'
		  	)
	    );
		$wp_customize->add_control( 'recent_post_status', 
			array(
			  'label'   => esc_html__( 'Enable Recent Posts', 'gridchamp' ),
			  'section' => 'gridchamp_recentpost_options',
			  'settings' => 'recent_post_status',
			  'type'    => 'checkbox',
			)
		);
		$wp_customize->selective_refresh->add_partial( 'recent_post_status', array(
            'selector' => '.recentblog-wraper h2.section-heading',
        ) );
		$wp_customize->add_setting( 'recentpost_heading_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',
			) 
		);
		$wp_customize->add_control( 'recentpost_heading_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_recentpost_options',
				'label' => esc_html__( 'Heading','gridchamp' ),
				'description' => esc_html__( 'Heading text for display on recent blog section','gridchamp' ),
				'active_callback' => function(){
				    return get_theme_mod( 'recent_post_status', true );
				},
			)
		);
		$wp_customize->add_setting( 'recentpost_desc_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control( 'recentpost_desc_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_recentpost_options',
				'label' => esc_html__( 'Description','gridchamp' ),
				'description' => esc_html__( 'Short Description text for display on recent blog section','gridchamp' ),
				'active_callback' => function(){
				    return get_theme_mod( 'recent_post_status', true );
				},
			)
		);
		$wp_customize->add_setting( 
	        'recent_blog_home', 
	        array(
	            'default'           => 'latest-post',
	            'sanitize_callback' => 'gridchamp_sanitize_choices'
	        ) 
	    );
	    
	    $wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'recent_blog_home',
				array(
					'section'	  => 'gridchamp_recentpost_options',
					'label'		  => esc_html__( 'Choose Post Type', 'gridchamp' ),
					'description' => '',
					'type'           => 'select',
					'choices'	  => array(
						'latest-post'    => esc_html__('Latest Posts','gridchamp'),
						'select-category'  => esc_html__('Select Category','gridchamp'),
					),
					'active_callback' => function(){
							return get_theme_mod( 'recent_post_status', true );
					},
				)
			)
		);

		$wp_customize->add_setting('gridchamp_recent_category',
	    array(
	        'default'           => '',
	        'capability'        => 'edit_theme_options',
	        'sanitize_callback' => 'gridchamp_sanitize_text',
	    )
		);
		$wp_customize->add_control(
			new Gridchamp_Dropdown_Taxonomies_Control($wp_customize, 
			'gridchamp_recent_category',
			    array(
			        'label'       => esc_html__('Select Category', 'gridchamp'),
			        'description' => esc_html__('Select category to be shown on home page blog section', 'gridchamp'),
			        'section'     => 'gridchamp_recentpost_options',
			        'type'        => 'dropdown-taxonomies',
			        'taxonomy'    => 'category',
			        'settings'	  => 'gridchamp_recent_category',
			        'priority'    => 10,
			        'active_callback' => 'gridchamp_current_post_type',
		    	)
			)
		);
		$wp_customize->add_setting( 'recentpost_readmore_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',
			) 
		);
		$wp_customize->add_control( 'recentpost_readmore_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_recentpost_options',
				'label' => esc_html__( 'Read More Text','gridchamp' ),
				'description' => esc_html__( 'Add the text for read more button in recent blog section.','gridchamp' ),
				'active_callback' => function(){
				    return get_theme_mod( 'recent_post_status', true );
				},
				'priority'    => 12,
			)
		);

		$wp_customize->add_setting( 'recentpost_viewall_text', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' =>'',
				'sanitize_callback' => 'gridchamp_sanitize_text',
			) 
		);
		$wp_customize->add_control( 'recentpost_viewall_text', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_recentpost_options',
				'label' => esc_html__( 'View All Text','gridchamp' ),
				'description' => esc_html__( 'Add the text for view all button in recent blog section.','gridchamp' ),
				'active_callback' => function(){
				    return get_theme_mod( 'recent_post_status', true );
				},
				'priority'    => 12,
			)
		);
		$wp_customize->add_setting( 'recentpost_viewall_text_link', 
		 	array(
				'capability' => 'edit_theme_options',
				'default' => '',
				'sanitize_callback' => 'gridchamp_sanitize_text',
			) 
		);
		$wp_customize->add_control( 'recentpost_viewall_text_link', 
			array(
				'type' => 'text',
				'section' => 'gridchamp_recentpost_options',
				'label' => esc_html__( 'View All Button Link','gridchamp' ),
				'description' => esc_html__( 'Add the link for view all button in recent blog section.','gridchamp' ),
				'active_callback' => function(){
				    return get_theme_mod( 'recent_post_status', true );
				},
				'priority'    => 12,
			)
		);
		
	}
	function gridchamp_current_post_type(){
		$current_blog_status = get_theme_mod( 'recent_post_status');
        $choice_post_type= get_theme_mod( 'recent_blog_home' );
		$blog_display_type = false;
		if($current_blog_status == true && $choice_post_type == 'select-category'){
			$blog_display_type = true;
		}
		return $blog_display_type;
    }
    function banner_btns_status_check(){
    	$chk_status_banner = get_theme_mod('banner_status');
        $hero_type= get_theme_mod('gridchamp_hero_type');
    	$current_btn_one_status = false;
    	if( $chk_status_banner=='true' && $hero_type=='gridchamp-banner'){
    		$current_btn_one_status = true;
    	}
    	return $current_btn_one_status;
    }
}
add_action( 'customize_register', 'gridchamp_frontpage_options_register' );