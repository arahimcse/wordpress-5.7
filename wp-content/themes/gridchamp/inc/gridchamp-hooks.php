<?php
add_action( 'gridchamp_header_before', 'gridchamp_topbar');
add_action( 'gridchamp_header_section', 'gridchamp_header');
add_action( 'gridchamp_header_after', 'gridchamp_subheader');
add_action('gridchamp_footer_text', 'gridchamp_footer_copyright');
add_action('gridchamp_brandings','gridchamp_branding');
add_action('gridchamp_navigation','gridchamp_site_navigation');
add_action('gridchamp_header_search_icon','header_search_icon');
add_action('gridchamp_cart_icon','header_cart_icon');
add_action('gridchamp_header_primary_button','header_primary_button');
add_action('gridchamp_header_secondary_button','header_secondary_button');
if(gridchamp_set_to_premium()){
	add_action('gridchamp_scroll_top_icon','gridchamp_scroll_top');
	add_action('gridchamp_top_notification_bar','gridchamp_notification_bar');
}

?>