<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Gridchamp
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function gridchamp_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'gridchamp_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function gridchamp_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'gridchamp_pingback_header' );

if(!function_exists('gridchamp_topbar')){
	function gridchamp_topbar(){
		if( get_theme_mod('gridchamp_topbar_status')){?>
		<div class="gridchamp-top-header walker-wraper">
			<div class="walker-container">
				<div class="walkerwp-grid-6 light-color">
						<?php gridchamp_header_hot_line();?>
				</div>
				<div class="walkerwp-grid-6 topbar-socials">
					<?php get_template_part( 'template-parts/partials/social-media/social-media'); ?>
				</div>
					
			</div>
		</div>
	<?php }
	}
}
if(!function_exists('gridchamp_header_hot_line')){
	function gridchamp_header_hot_line(){?>
		<?php if(get_theme_mod('gridchamp_header_slogan_text') || get_theme_mod('gridchamp_header_contact')){
			$tel_tag_link = get_theme_mod('gridchamp_header_contact');
			?>
			<span class="header-slogan">
			<?php 
			if(get_theme_mod('gridchamp_header_slogan_text')){
				echo esc_html(get_theme_mod('gridchamp_header_slogan_text'));
			}
			?> 
			<a href="<?php echo esc_url( 'tel:' . $tel_tag_link ); ?>"><?php echo esc_html(get_theme_mod('gridchamp_header_contact'));?></a>
		</span>
		<?php }		
	 }
}

if(!function_exists('gridchamp_header')){
	function gridchamp_header(){
		if(gridchamp_set_to_premium()){
			$gridchamp_header_layout = get_theme_mod('gridchamp_select_header_layout');
			if($gridchamp_header_layout=='gridchamp-header-five'){
				$gridchamp_header_layout='five';
			}elseif($gridchamp_header_layout=='gridchamp-header-four'){
				$gridchamp_header_layout='four';
			}elseif($gridchamp_header_layout=='gridchamp-header-three'){
				$gridchamp_header_layout='three';
			}elseif($gridchamp_header_layout=='gridchamp-header-two'){
				$gridchamp_header_layout='two';
			}else{
				$gridchamp_header_layout='one';
			}
		}else{
			$gridchamp_header_layout='one';
		}
		

		get_template_part( 'template-parts/partials/header/header',$gridchamp_header_layout);
	}
}
if(!function_exists('gridchamp_subheader')){
	function gridchamp_subheader(){
		if(get_theme_mod('innerpage_header_status')){
			 if ( !is_front_page() && !is_home() ) {?>
			<div class="walker-wraper inner-page-subheader">
				<?php
				if(get_theme_mod('subheader_bg_image')){
					$header_background_img = get_theme_mod('subheader_bg_image');?>
					<img class="header-overlay-image" src="<?php echo esc_url($header_background_img);?>" />
				<?php }
				?>
				
				<div class="walker-container">
					<div class="walkerwp-grid-12">
						<span class="page-header-title">
						<?php if(is_search()){
								$gridchamp_title= __('Search', 'gridchamp');
							}elseif(is_404()){
								$gridchamp_title = __('404', 'gridchamp');
							}elseif(is_archive()){
								$gridchamp_title = the_archive_title();
							}else{
								$gridchamp_title = get_the_title();
							}?>
							<?php echo $gridchamp_title; ?>
								
							</span>
							<?php
							if(get_theme_mod('breadcrumbs_status')){?>
								<div class="gridchamp-breadcrumbs"><?php breadcrumb_trail();?></div>
							<?php } ?>
					</div>
				</div>
			</div>
			<?php 
		} 
	}
}
}
if ( ! function_exists( 'gridchamp_footer_copyright' ) ) :
	function gridchamp_footer_copyright() {
		$gridchamp_copyright = get_theme_mod('footer_copiright_text');
		if(get_theme_mod('copyright_text_alignment') =='copyright-text-align-center'){
			$copyright_text_align_class ="text-center";
		}else{
			$copyright_text_align_class ="text-left";
		}
		if($gridchamp_copyright && gridchamp_set_to_premium() ){?>
			<div class="site-info walkerwp-grid-12 <?php echo esc_attr($copyright_text_align_class);?>"><?php echo wp_kses_post($gridchamp_copyright);?></div>
		<?php } else{ ?>
			<div class="site-info walkerwp-grid-12 <?php echo esc_attr($copyright_text_align_class);?>">
				<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'gridchamp' ) ); ?>">
					<?php
					/* translators: %s: CMS name, i.e. WordPress. */
					printf( esc_html__( 'Proudly powered by %s', 'gridchamp' ), 'WordPress' );
					?>
				</a>
				<span class="sep"> | </span>
					<?php
					/* translators: 1: Theme name, 2: Theme author. */
					printf( esc_html__( 'Theme: %1$s by %2$s.', 'gridchamp' ), 'gridchamp', '<a href="http://walkerwp.com/">WalkerWP</a>' );
					?>
			</div><!-- .site-info -->
		<?php } ?>
		

		
	<?php }
endif;

if(! function_exists('gridchamp_branding')):
	function gridchamp_branding(){?>
		<div class="site-branding">
			<?php
				the_custom_logo();
				$gridchamp_site_name = get_bloginfo('name');
				if ( $gridchamp_site_name ){
					  echo '<h2 class="site-title"><a href="'.esc_url(home_url()).'" rel="home">' .esc_html( $gridchamp_site_name ) . '</a></h2>';
				}
				$gridchamp_description = get_bloginfo( 'description', 'display' );
				if ( $gridchamp_description || is_customize_preview() ) : ?>
					<p class="site-description"><?php echo esc_textarea($gridchamp_description); ?></p>
					<?php 
				endif; ?>
		</div><!-- .site-branding -->
	<?php }
endif;

if(!function_exists('gridchamp_site_navigation')):
	function gridchamp_site_navigation(){?>
		<nav id="site-navigation" class="main-navigation">
				<button type="button" class="menu-toggle">
					<span></span>
					<span></span>
					<span></span>
				</button>
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'main-menu',
						'menu_id'        => 'primary-menu',
					)
				);
				?>
			</nav><!-- #site-navigation -->
	<?php }
endif;

if(! function_exists('header_search_icon')):
	function header_search_icon(){
		if(get_theme_mod('search_icon_status')){?>
			<span class="header-icon-search">
				<button class="search-toggle"><i class="fa fa-search" aria-hidden="true"></i></button>
				<!-- The Modal -->
				<div id="searchModel" class="search-modal modal">
					<div class="modal-content">
						<div class="modal-body">
							<button  class="modal-close">&times;</button>
							<p><?php get_Search_form(); ?></p>
						</div>
					</div>
				</div>
			</span>

		<?php }
	 }
endif;

if(! function_exists('header_cart_icon')):
	function header_cart_icon(){
		if(get_theme_mod('cart_icon_status') && class_exists( 'woocommerce' ) ){?>
			<span class="header-cart-icon"><a class="header-cart" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart','gridchamp' ); ?>"> <i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="item-count"><?php echo  WC()->cart->get_cart_contents_count(); ?> </span></a></span>
		<?php } 
	}
endif;

if(! function_exists('header_primary_button')):
	function header_primary_button(){
		if(get_theme_mod('primary_btn_link')){
			$primary_button_link = get_theme_mod('primary_btn_link');
		}else{
			$primary_button_link ='#';
		}
		if(get_theme_mod('primary_btn_link_target')){
			$primary_button_target='_blank';
		}else{
			$primary_button_target='_self';
		}
		?>
		<?php if(get_theme_mod('gridchamp_header_primary_button')){?>
		 <a href="<?php echo esc_url($primary_button_link); ?>" class="gridchamp-primary-button header-button" target="<?php echo esc_attr($primary_button_target);?>"><?php echo esc_html(get_theme_mod('gridchamp_header_primary_button'));?></a>
		  <?php }
	 }
endif;

if(! function_exists('header_secondary_button')):
	function header_secondary_button(){
		if(get_theme_mod('secondary_btn_link')){
			$secondary_button_link = get_theme_mod('secondary_btn_link');
		}else{
			$secondary_button_link ='#';
		}
		if(get_theme_mod('secondary_btn_link_target')){
			$secondary_button_target='_blank';
		}else{
			$secondary_button_target='_self';
		}
		?>
		<?php if(get_theme_mod('gridchamp_header_secondary_button')){?>
		 <a href="<?php echo esc_url($secondary_button_link);?>" class="gridchamp-secondary-button header-button" target="<?php echo esc_attr($secondary_button_target);?>"><?php echo esc_html(get_theme_mod('gridchamp_header_secondary_button'));?></a>
	<?php }
	}
endif;

if(gridchamp_set_to_premium()){
	if(! function_exists('gridchamp_scroll_top')):
		function gridchamp_scroll_top(){
			if(get_theme_mod('Scroll_top_option')){ ?>
				<a href="#" class="gridchamp-top"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></a>
		<?php }
		}
	endif;
}

if(! function_exists('gridchamp_banner_section')):
	function gridchamp_banner_section(){?>
		<div class="walker-wraper banner-wraper no-gap">
			<?php $gridchamp_banner_image = get_header_image();
				if(!empty($gridchamp_banner_image)){?>
					<img src="<?php header_image(); ?>" alt="">
				<?php }
				if(get_theme_mod('walker_slder_text_align')=='slide-text-align-right'){
					$slide_text_align='text-right';
				}elseif(get_theme_mod('walker_slder_text_align')=='slide-text-align-center'){
					$slide_text_align='text-center';
				}else{
					$slide_text_align='';
				}
			?>
			<div class="walker-container  <?php echo esc_attr($slide_text_align);?>">
				<div class="banner-overlay">
					
					<div class="banner-overlay-content <?php echo esc_attr($slide_text_align);?>">
						<?php 
						
							if(get_theme_mod('banner_heading_text')){
								echo '<h1>' .esc_html(get_theme_mod('banner_heading_text')).'</h1>';
							}
							if(get_theme_mod('banner_desc_text')){
								echo '<p>' .esc_html(get_theme_mod('banner_desc_text')) .'</p>';
							}
							if(get_theme_mod('gridchamp_banner_button_one')){
								if(get_theme_mod('banner_button_one_link')){
									$banner_btn_one_link = get_theme_mod('banner_button_one_link');
								}else{
									$banner_btn_one_link='#';
								}
								if(get_theme_mod('banner_button_one_target')){
									$banner_btn_one_target='_blank';
								}else{
									$banner_btn_one_target='_self';
								}
								?>
								<a href="<?php echo esc_url($banner_btn_one_link);?>" class="gridchamp-primary-button style-outline-light" target="<?php echo esc_attr($banner_btn_one_target);?>"><?php echo esc_html(get_theme_mod('gridchamp_banner_button_one')); ?></a>
							<?php }
							if(get_theme_mod('gridchamp_banner_button_2')){

								if(get_theme_mod('banner_button_two_link')){
									$banner_btn_two_link = get_theme_mod('banner_button_two_link');
								}else{
									$banner_btn_two_link='#';
								}
								if(get_theme_mod('banner_button_two_target')){
									$banner_btn_two_target='_blank';
								}else{
									$banner_btn_two_target='_self';
								}

								?>
								<a href="<?php echo esc_url($banner_btn_two_link);?>" class="gridchamp-secondary-button" target="<?php echo esc_attr($banner_btn_two_target);?>"><?php echo esc_html(get_theme_mod('gridchamp_banner_button_2'));?></a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	<?php }
endif;
if(gridchamp_set_to_premium()){
	if(!function_exists('gridchamp_homesection_about')):
		function gridchamp_homesection_about(){
			$gridchamp_about_layout_style= get_theme_mod('gridchamp_about_layout');
			if($gridchamp_about_layout_style=='gridchamp-about-layout-2'){
				get_template_part( 'template-parts/partials/about/about-layout-two'); 
			}else{
				get_template_part( 'template-parts/partials/about/aboutus'); 
			}
		}
	endif;

	if(!function_exists('gridchamp_homesection_services')):
		function gridchamp_homesection_services(){
			$front_service_layout = get_theme_mod('walker_service_layout');
			if($front_service_layout=='service-layout-2'){
				get_template_part( 'template-parts/partials/services/service-two');
			}else{
				get_template_part( 'template-parts/partials/services/services');
			}
		}
	endif;

	if(! function_exists('gridchamp_homesection_features')):
		function gridchamp_homesection_features(){
			get_template_part( 'template-parts/partials/features/features-one');
		}
	endif;

	if(! function_exists('gridchamp_homesection_testimonials')):
		function gridchamp_homesection_testimonials(){
			$front_testimonial_layout = get_theme_mod('walker_testimonial_layout');
			if($front_testimonial_layout=='testimonial-layout-3'){
				get_template_part( 'template-parts/partials/testimonial/testimonial-three');
			}elseif($front_testimonial_layout=='testimonial-layout-2'){
				get_template_part( 'template-parts/partials/testimonial/testimonial-two');
			}else{
				get_template_part( 'template-parts/partials/testimonial/testimonial-one');
			}
		}
	endif;

	if(! function_exists('gridchamp_homesection_portfolios')):
		function gridchamp_homesection_portfolios(){
			$front_portfolio_layout = get_theme_mod('walker_portfolio_layout');
			if($front_portfolio_layout=='grid-layout'){
				get_template_part( 'template-parts/partials/portfolio/portfolio');
			}else{
				get_template_part( 'template-parts/partials/portfolio/portfolio-slider');
			}
		}
	endif;

	if(!function_exists('gridchamp_homesection_team')):
		function gridchamp_homesection_team(){
			$front_team_layout = get_theme_mod('walker_team_layout');
			if($front_team_layout=='team-layout-2'){
				get_template_part( 'template-parts/partials/team/team-layout2');
			}else{
				get_template_part( 'template-parts/partials/team/team');
			}
		}
	endif;

	if(! function_exists('gridchamp_homesection_cta')):
		function gridchamp_homesection_cta(){
			get_template_part( 'template-parts/partials/cta/cta');
		}
	endif;

	if(! function_exists('gridchamp_homesection_faqs')):
		function gridchamp_homesection_faqs(){
			get_template_part( 'template-parts/partials/faq/faqs');
		}
	endif;

	if(! function_exists('gridchamp_homesection_blogs')):
		function gridchamp_homesection_blogs(){
			get_template_part( 'template-parts/partials/blogs/recentblogs');
		}
	endif;

	if(! function_exists('gridchamp_homesection_brands')):
		function gridchamp_homesection_brands(){
			get_template_part( 'template-parts/partials/brands/brands');
		}
	endif;
}

if(!function_exists('gridchamp_notification_bar')):
	function gridchamp_notification_bar(){
		if(gridchamp_set_to_premium()){
			if(get_theme_mod('notification_bar_status')){?>
				<div class="walker-wraper top-notification-bar no-gap">
						<div class="walker-container text-center">
							<div class="noticebar-content">
								<span class="noticebar-text"><?php echo esc_html(get_theme_mod('notification_text'));?></span>
								<?php
									if(get_theme_mod('notification_btn_text')){
										if(get_theme_mod('notification_btn_url')){
											$noticebar_btn_url=get_theme_mod('notification_btn_url');
										}else{
											$noticebar_btn_url='#';
										}

										if(get_theme_mod('notification_btn_target')){
											$noticebar_btn_target='_blank';
										}else{
											$noticebar_btn_target='_self';
										}

										?>
										<a href="<?php echo esc_url($noticebar_btn_url);?>" class="noticebar-button" target="<?php echo esc_attr($noticebar_btn_target);?>"><?php echo esc_html(get_theme_mod('notification_btn_text'));?></a> 
								<?php }

								?>
								

							</div>
						</div>
				</div>
				<button class="notice-toggle"></button>
				
			<?php }	
		}
	}
endif;

if(!function_exists('gridchamp_post_pagination')){
	function gridchamp_post_pagination(){
		if(gridchamp_set_to_premium()){
			if(get_theme_mod('pagination_style')=='numeric-paginate-style'){
				gridchamp_pagination();
			}else{
				the_posts_navigation();
			}
		}else{
			the_posts_navigation();
		}
	}
}