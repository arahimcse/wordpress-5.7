<?php
/**
 * Gridchamp Theme Customizer
 *
 * @package Gridchamp
 */


/**
*
*Custom controls for theme
*/

require get_template_directory() . '/inc/custom-controls.php';
/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function gridchamp_customize_register( $wp_customize ) {
	$wp_customize->get_section( 'title_tagline' )->priority = 15;
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';	

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'gridchamp_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'gridchamp_customize_partial_blogdescription',
			)
		);
	}

	//Panel register for theme option
    $wp_customize->add_panel( 'gridchamp_theme_option', 
	  	array(
		    'priority'       => 20,
		    'capability'     => 'edit_theme_options',
		    'title'      => esc_html__('Theme Options', 'gridchamp'),
		) 
	);
	//Panel register for theme option
    $wp_customize->add_panel( 'gridchamp_frontpage_option', 
	  	array(
		    'priority'       => 30,
		    'capability'     => 'edit_theme_options',
		    'title'      => esc_html__('Theme Frontpage Setup', 'gridchamp'),
		) 
	);
	
}
add_action( 'customize_register', 'gridchamp_customize_register' );


/**
 * Sanitization Functions
*/
require get_template_directory() . '/inc/sanitization_functions.php';

/**
 * Theme customizer options
*/
require get_template_directory() . '/inc/customizer/about-options.php';
require get_template_directory() . '/inc/customizer/header-options.php';
require get_template_directory() . '/inc/customizer/footer-options.php';
require get_template_directory() . '/inc/customizer/slider-options.php';
require get_template_directory() . '/inc/customizer/typography-options.php';
require get_template_directory() . '/inc/customizer/colors-options.php';
require get_template_directory() . '/inc/customizer/blog-options.php';
require get_template_directory() . '/inc/customizer/social-options.php';
require get_template_directory() . '/inc/customizer/extra-options.php';
require get_template_directory() . '/inc/customizer/frontpage-options.php';
require get_template_directory() . '/inc/customizer/cta-options.php';



/**
*
*Enqueue customizer styles and scripts
*/
function gridchamp_customize_controls_register_scripts() {
	wp_enqueue_style( 'gridchamp-customize-controls', get_template_directory_uri() . '/inc/customizer/customize-controls.css', array(), '1.0.0' );
}

add_action( 'customize_controls_enqueue_scripts', 'gridchamp_customize_controls_register_scripts', 0 );


/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function gridchamp_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function gridchamp_customize_partial_blogdescription() {
	bloginfo( 'description' );
}


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function gridchamp_customize_preview_js() {
	wp_enqueue_script( 'gridchamp-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), GIRDCHAMP_VERSION, true );
}
add_action( 'customize_preview_init', 'gridchamp_customize_preview_js' );
