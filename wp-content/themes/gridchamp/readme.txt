=== Gridchamp ===

Contributors: WalkerWP
Tags: custom-background, custom-logo, custom-menu, custom-colors, grid-layout, flexible-header, footer-widgets, full-width-template, left-sidebar, one-column, right-sidebar, sticky-post, three-columns, two-columns,  featured-images, threaded-comments, translation-ready, blog

Requires at least: 5.0
Tested up to: 5.7
Requires PHP: 5.6
Stable tag: 1.0.5
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==

Gridchamp is the feature rich WordPress multi-purpose free theme, which provides tons of features and ready made layout for home section. Gridchamp designed with mobile-first approach, compatibility and responsive layout to build more accessible site for business. 

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Gridchamp includes support for WooCommerce and for Infinite Scroll in Jetpack.

== Changelog ==

= 1.0.5 - Jun 11 2015 =
* Adding Transparent header to the theme
* Screenshot image change
* Custom header images size fix

= 1.0.4 - Jun 09 2015 =
* Section order improvement

= 1.0.3 - Jun 02 2015 =
* Initial release

= 1.0.2 - Jun 01 2015 =
* Initial release

= 1.0.1 - May 29 2015 =
* Initial release

= 1.0.0 - May 12 2015 =
* Initial release


==Copyright==
Gridchamp WordPress Theme, Copyright 2021 WalkerWP
Gridchamp is distributed under the terms of the GNU GPL

== Credits ==
== Resources ==

Image for theme screenshot, https://pxhere.com
License: CC0 1.0 Universal (CC0 1.0)
License URL: https://creativecommons.org/publicdomain/zero/1.0/
Source: https://pxhere.com/en/photo/722218

Image for theme about us section image, https://pxhere.com
License: CC0 1.0 Universal (CC0 1.0)
License URL: https://creativecommons.org/publicdomain/zero/1.0/
Source: https://pxhere.com/en/photo/770914


	    	
Images/Icons Used in customizer
All the images/icons used in the customizer are created by our own.
License: GPL v2 or later

Underscores:
Author: 2012-2015 Automattic
Source: http://underscores.me
License: GPL v2 or later (https://www.gnu.org/licenses/gpl-2.0.html)

normalize.css
Author: (C) 2012-2016 Nicolas Gallagher and Jonathan Neal
Source: https://necolas.github.io/normalize.css/
License: [MIT](https://opensource.org/licenses/MIT)	


TGM Plugin Activation
Source: http://tgmpluginactivation.com/
License: GNU General Public License version 2 (https://opensource.org/licenses/GPL-2.0)

Breadcrumb Trail
Source: https://github.com/justintadlock/breadcrumb-trail
License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Font-Awesome
Author: 2012 Dave Gandy Font
Source: https://github.com/FortAwesome/Font-Awesome
License: SIL OFL 1.1 Code License: MIT License http://fontawesome.io/license/


Swiperjs
Author: Copyright 2014-2021 Vladimir Kharlampidi
Source: https://unpkg.com/browse/swiper@6.6.2/
License: [MIT](https://opensource.org/licenses/MIT)

Google Fonts
Source: https://fonts.google.com/
License: Apache License, version 2.0

Freemius, Copyright (c) 2020 Freemius®, Inc.,
License: GNU General Public License v3.0
Source: https://github.com/Freemius/wordpress-sdk