<?php 
/**
 * The template for displaying index page
 *
 * @version    0.1.84
 * @package    pikaxo
 * @author     Zidithemes
 * @copyright  Copyright (C) 2021 zidithemes.com All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * 
 */
?>

<?php get_header(); ?>



<?php

$is_elementor_theme_exist = function_exists( 'elementor_theme_do_location' );


if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'index' ) ) {

?>

	<div class="page-content">
		<div class="flowid pikaxo-index">

		    <div class="mg-auto wid-90 mobwid-90">
		        
		        <div class="inner jc-sp-btw dsply-fl fl-wrap">
		            
		            <div class="card-outer wid-69 mobwid-100">
		            	<div class="card-box jc-sp-btw dsply-fl fl-wrap">
			            	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			            		<?php get_template_part('content', get_post_format());  ?>

			            	<?php endwhile; else : ?>
								
								<h2><?php esc_html__('No posts Found!', 'pikaxo'); ?></h2>
			                    
			                <?php endif; ?>

		                </div>
		                <ul class="pagination flowid">
						   <?php the_posts_pagination(); ?>
						</ul>
		            </div>
		            <?php get_sidebar(); ?>

		        </div>

		    </div>
		</div>
	</div>
<?php
}

 elseif ( is_singular() ) {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'single' ) ) {
		get_template_part( 'single' );
	}
} elseif ( is_archive() || is_home() || is_search() ) {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'archive' ) ) {
		get_template_part( 'archive' );
	}
} else {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( '404' ) ) {
		get_template_part( '404' );
	}
}

?>




<?php get_footer(); ?>