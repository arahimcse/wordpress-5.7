<?php 
/**
 * @version    0.1.84
 * @package    pikaxo
 * @author     Zidithemes
 * @copyright  Copyright (C) 2021 zidithemes.com All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * 
 */
?>

<?php
if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footer' ) ) {
?>  

    <footer id="site-footer" class="site-footer" role="contentinfo">


        <div class="flowid footer-3-col">

            <div class="mg-auto wid-90 mobwid-90">
                
                <div class="inner dsply-fl fl-wrap">
                    
                    <!-- BEGIN FOOTER -->
                    <div class="wid-32 footer mobwid-100">
                        <div class="footer-inner">
                            
                            <div class="footer-items">
                                <?php if(!function_exists(dynamic_sidebar('left-footer'))) {
        							if (has_nav_menu('footer-left')) {
        							wp_nav_menu(array(
        								'theme_location' => 'footer-left',
        								'container_class' => 'footer-items',
        								'container' => 'ul',
        								'fallback_cb' => ''
        							));
        							} 
        						   }
        						?> 
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="wid-32 footer mobwid-100">
                        <div class="footer-inner">
                            
                            <div class="footer-items">
                                <?php if(!function_exists(dynamic_sidebar('middle-footer'))) {
        							if (has_nav_menu('footer-middle')) {
        							wp_nav_menu(array(
        								'theme_location' => 'footer-middle',
        								'container_class' => 'footer-items',
        								'container' => 'ul',
        								'fallback_cb' => ''
        							));
        							} 
        						   }
        						?>   
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="wid-32 footer mobwid-100">
                        <div class="footer-inner">
                            
                            <div class="footer-items">
                                <?php if(!function_exists(dynamic_sidebar('right-footer'))) {
        							if (has_nav_menu('footer-right')) {
        							wp_nav_menu(array(
        								'theme_location' => 'footer-right',
        								'container_class' => 'footer-items',
        								'container' => 'ul',
        								'fallback_cb' => ''
        							));
        							} 
        						   }
        						?>   
                            </div>
                            
                        </div>
                    </div>

                    <div class="wid-100 footer mobwid-100">
                        <div class="footer-inner text-center">
                            
                            <div class="footer-items">
                                <?php if(!function_exists(dynamic_sidebar('full-width-footer'))) {
        							if (has_nav_menu('footer-full-width')) {
        							wp_nav_menu(array(
        								'theme_location' => 'footer-fourth',
        								'container_class' => 'footer-items',
        								'container' => 'ul',
        								'fallback_cb' => ''
        							));
        							} 
        						   }
        						?>    
                            </div>
                            
                        </div>
                    </div>

                    <div class="wid-100 footer pd-td-10 mobwid-100">
                        <div class="footer-inner text-center">
                            
                            <div class="footer-items site-info">
                            	<div class="site-info-inner">
                                <a href="<?php echo esc_url( esc_html_e( 'http://zidithemes.com', 'pikaxo' ) ); ?>"><?php esc_html_e( 'Theme by Zidithemes', 'pikaxo' ); ?></a>
                            	</div>
                            </div>

                        </div>
                    </div>
                    
                    <!-- END FOOTER -->
                    
                    
                    
                    
                </div>

            </div>

        </div>



</footer>

<?php } ?>


</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>