<?php 
/**
 * @version    0.1.84
 * @package    pikaxo
 * @author     Zidithemes
 * @copyright  Copyright (C) 2021 zidithemes.com All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * 
 */


if ( ! defined( 'ABSPATH' ) ) { exit; }


require_once("inc/pikaxo-customizer.php");
require_once("inc/pikaxo-options.php");


function pikaxo_setup(){
	
		// ADD THEME SUPPORT
	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( "custom-header" );
	add_theme_support( "custom-background" );
	add_theme_support( 'post-thumbnails' );
	add_theme_support('woocommerce');


	// ADD EDITOR STYLE

	//REGISTER NAV MENUS
	register_nav_menus(
	    array(
	      'primary-menu' => __( 'Primary Menu', 'pikaxo' ),
	    )
	  );

	// SET CONTENT WIDTH
	if ( ! isset( $content_width ) ) $content_width = 600;

	load_theme_textdomain( 'pikaxo' );


}

add_action('after_setup_theme', 'pikaxo_setup');


//Load styles
function pikaxo_load_styles_scripts(){
	//NOTE:   SOME OF THESE SCRIPTS ARE HOSTED ON A CDN AND ARE NOT STORED LOCALLY... SO THIS THEME MAY NOT RENDER PROPERLY IF YOU ARE NOT CONNECTED TO THE INTERNET
	
	//wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
		wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');


		if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); 

	
}

add_action('wp_enqueue_scripts', 'pikaxo_load_styles_scripts');

function pikaxo_pingback_wrap() {

		if ( is_singular() && pings_open() ) {
			echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
		}

}
add_action( 'wp_head', 'pikaxo_pingback_wrap' );



if (!function_exists('pikaxo_admin_style')) {
	function pikaxo_admin_style($hook) {
		
		if ('appearance_page_pikaxo_theme_info_options' === $hook) {
			wp_enqueue_style('pikaxo-admin-script-style', get_template_directory_uri() . '/css/pikaxo-admin.css');
		}
	}
}
add_action('admin_enqueue_scripts', 'pikaxo_admin_style');


//Creating the sidebar
function pikaxo_menu_init() {


register_sidebar(
		array(
                'name' 			=> esc_html__('Sidebar Widgets', 'pikaxo'),
                'id'   			=> 'sidebar-widgets',
				'description' 	=> esc_html__('Add sidebar widgets here', 'pikaxo'),
		'before_widget' 		=> '<div class="sidebar-items">',
		'after_widget' 			=> '</div>',
		'before_title' 			=> '<h2>',
		'after_title' 			=> '</h2>',
	));


	register_sidebar(array(
                'name' 			=> esc_html__('Left Footer', 'pikaxo'),
                'id'   			=> 'left-footer',
				'description' 	=> esc_html__('Add widgets here', 'pikaxo'),
		'before_widget' 		=> '<li>',
		'after_widget' 			=> '</li>',
		'before_title' 			=> '<h2>',
		'after_title' 			=> '</h2>',
	));
	
	register_sidebar(array(
                'name' 			=> esc_html__('Middle Footer', 'pikaxo'),
                'id'   			=> 'middle-footer',
				'description' 	=> esc_html__('Add widgets here', 'pikaxo'),
		'before_widget' 		=> '<li>',
		'after_widget' 			=> '</li>',
		'before_title' 			=> '<h2>',
		'after_title' 			=> '</h2>',
	));
	
	register_sidebar(array(
                'name' 			=> esc_html__('Right Footer', 'pikaxo'),
                'id'   			=> 'right-footer',
				'description' 	=> esc_html__('Add widgets here', 'pikaxo'),
		'before_widget' 		=> '<li>',
		'after_widget' 			=> '</li>',
		'before_title' 			=> '<h2>',
		'after_title' 			=> '</h2>',
	));
	
	register_sidebar(array(
                'name' 			=> esc_html__('Full Width Footer', 'pikaxo'),
                'id'  			=>  'full-width-footer',
				'description' 	=> esc_html__('Add widgets here(Full width Footer)', 'pikaxo'),
		'before_widget' 		=> '<li>',
		'after_widget' 			=> '</li>',
		'before_title' 			=> '<h2>',
		'after_title' 			=> '</h2>',
	));

}
add_action('widgets_init', 'pikaxo_menu_init');



// this increases or decreaes the length of the excerpt on the index page
function pikaxo_excerpt_length( $length ) {
	if ( is_admin() ) {
		return $length;
	}
	return 40;
}
add_filter( 'excerpt_length', 'pikaxo_excerpt_length', 999 );

function pikaxo_excerpt_more( $more ) {
    //return 'More';
    return ' <a class="read-more" href="'. esc_url(get_permalink( get_the_ID() ) ) . '">' . __('Read More', 'pikaxo') . '</a>';
}
add_filter( 'excerpt_more', 'pikaxo_excerpt_more' );




/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function pikaxo_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'pikaxo_skip_link_focus_fix' );

