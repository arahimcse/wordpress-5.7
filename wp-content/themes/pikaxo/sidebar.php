<?php 
/**
 * The template for displaying sidebar
 *
 * @version    0.1.84
 * @package    pikaxo
 * @author     Zidithemes
 * @copyright  Copyright (C) 2021 zidithemes.com All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * 
 */
?>
<aside class="mobwid-100 no-show-mob sidebar wid-29">
	<div class="sidebar-inner">
		<?php dynamic_sidebar('sidebar-widgets'); ?>
	</div>
</aside>