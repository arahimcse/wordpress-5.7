<?php


function pikaxo_theme_info_menu() {

	add_theme_page( 
		esc_html__('Welcome To Pikaxo WordPress Theme', 'pikaxo'), 
		esc_html__('Pikaxo Theme Info', 'pikaxo'), 
		'manage_options', 
		'pikaxo_theme_info_options', 
		'pikaxo_theme_info_display' 
	);

}


add_action( 'admin_menu', 'pikaxo_theme_info_menu' );



function pikaxo_theme_info_display() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( esc_html( 'You do not have sufficient permissions to access this page.' ) );
	}
	
	?>
	<div class="wrap pikaxo-adm">
		<h1 class="header-welcome"><?php esc_html_e('Welcome to Pikaxo - 0.1.84', 'pikaxo'); ?></h1>
		<div class="pikaxo-adm-dsply-fl pikaxo-adm-fl-wrap pikaxo-adm-jc-sp-btw">

			<div class="pikaxo-adm-wid-49 theme-para theme-doc pikaxo-adm-mobwid-100">
				<h4><?php esc_html_e('Theme Documentation','pikaxo'); ?></h4>
				<p><?php esc_html_e('Documentation for this theme includes all theme information that is needed to get your site up and running', 'pikaxo'); ?></p>
				<p>
					<a href="<?php echo esc_url('https://zidithemes.com/pikaxo-free/'); ?>" class="button button-secondary" target="_blank">
						<?php esc_html_e('Theme Documentation', 'pikaxo'); ?>
					</a>
				</p>
			</div>

			<div class="pikaxo-adm-wid-49 theme-para theme-opt pikaxo-adm-mobwid-100">
				<h4><?php esc_html_e('Pikaxo Pro','pikaxo'); ?></h4>
				<p class="">
					<?php esc_html_e('Pikaxo Pro includes portfolio page templates, additional features and more options to customize your website.',  'pikaxo'); ?>
				</p>
				<p>
					<a href="<?php echo esc_url('https://zidithemes.com/pikaxo-wordpress-theme-pro/'); ?>" class="button button-primary" target="_blank">
						<?php esc_html_e('Upgrade to Pikaxo Pro', 'pikaxo'); ?>
					</a>
				</p>
			</div>

			<div class="pikaxo-adm-wid-49 theme-para theme-opt pikaxo-adm-mobwid-100">
				<h4><?php esc_html_e('Theme Options','pikaxo'); ?></h4>
				<p class="">
					<?php esc_html_e('pikaxo Theme supports Theme Customizer. Click "Go To Customizer" to open the Customizer now.',  'pikaxo'); ?>
				</p>
				<p>
					<a href="<?php echo esc_html(admin_url('customize.php')); ?>" class="button button-secondary" target="_blank">
						<?php esc_html_e('Go To Customizer', 'pikaxo'); ?>
					</a>
				</p>
			</div>

			<div class="pikaxo-adm-wid-49 theme-para theme-review pikaxo-adm-mobwid-100">
				<h4><?php esc_html_e('Leave us a review','pikaxo'); ?></h4>
				<p><?php esc_html_e('We would love to hear your feedback.', 'pikaxo'); ?></p>
				<p>
					<a href="<?php echo esc_url('https://wordpress.org/support/theme/pikaxo/reviews/#new-post'); ?>" class="button button-secondary" target="_blank">
						<?php esc_html_e('Submit a review', 'pikaxo'); ?>
					</a>
				</p>
			</div>


			<div class="pikaxo-adm-wid-49 theme-para theme-support pikaxo-adm-mobwid-100">
				<h4><?php esc_html_e('Support','pikaxo'); ?></h4>
				<p><?php esc_html_e('Reach out in the theme support forums on WordPress.org.', 'pikaxo'); ?></p>
				<p>
					<a href="<?php echo esc_url('https://wordpress.org/support/theme/pikaxo/'); ?>" class="button button-secondary" target="_blank">
						<?php esc_html_e('Support Forum', 'pikaxo'); ?>
					</a>
				</p>
			</div>


			<div class="theme-upgrade pikaxo-adm-wid-100">
				<table class="pikaxo-adm-wid-100">
					<thead class="theme-table-head">
						<tr>
							<th class="feature"><h3><?php esc_html_e('Features', 'pikaxo'); ?></h3></th>
							<th class="pikaxo-adm-wid-33"><h3><?php esc_html_e('pikaxo', 'pikaxo'); ?></h3></th>
							<th class="pikaxo-adm-wid-33"><h3><?php esc_html_e('Pikaxo Pro', 'pikaxo'); ?></h3></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="feature-items-title">
								<h3><?php esc_html_e('Theme Price', 'pikaxo'); ?></h3>
							</td>
							<td class="free-btn"><?php esc_html_e('Free', 'pikaxo'); ?></td>
							<td>
								<a class="pro-link-btn" href="<?php echo esc_url('https://zidithemes.com/pikaxo-wordpress-theme-pro/'); ?>" target="_blank">
									<?php esc_html_e('View Pricing', 'pikaxo'); ?>
								</a>
							</td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Responsive Design', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-yes"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Portfolio Page Template', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-no"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Testimonials Page Template', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-no"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Team Page Template', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-no"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Gallery Page Template', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-no"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Pricing Page Template', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-no"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('One Column Post Template', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-no"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('3 News Grid Image Styles', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-no"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Full Width Template', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-yes"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Footer Credits Options', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-no"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Color Options', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-yes"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Gutenberg Compatible', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-yes"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Elementor Compatible', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-yes"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Major Browser Compatible', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-yes"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class="feature-items-title"><h3><?php esc_html_e('Woocommerce Compatible', 'pikaxo'); ?></h3></td>
							<td><span class="dashicons dashicons-yes"></span></td>
							<td><span class="dashicons dashicons-yes"></span></td>
						</tr>
						<tr>
							<td class=""></td>
							<td class=""></td>
							<td class="go-pro">
								<span class="">
									<a class="link" href="<?php echo esc_url('https://zidithemes.com/pikaxo-wordpress-theme-pro/'); ?>" target="_blank">
										<?php esc_html_e('View Pro', 'pikaxo'); ?>
									</a>
								</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

		</div>
	</div>
	<?php
}
?>
