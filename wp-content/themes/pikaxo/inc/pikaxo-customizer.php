<?php

function pikaxo_customizer_settings( $wp_customize ){


	//ADD PANEL
	$wp_customize->add_panel('pikaxo_site_layout_option_panel',
		array(
			'title'      => esc_html__('Pikaxo Theme - Customize Layout', 'pikaxo'),
			'priority'   => 2,
			'capability' => 'edit_theme_options',
		)
	);

	//BEGIN NAVIGATION BACKGROUND COLOR SECTION
	$wp_customize->add_section('pikaxo_navbar_section', array(
		'title' => __('Pikaxo Theme - Navbar BG Color', 'pikaxo'),
		'priority' => 10,
		'panel' => 'pikaxo_site_layout_option_panel',
	));

	
	//NAVBAR SECTION BACKGROUND COLOR
	$wp_customize->add_setting('pikaxo_panbtn_section_bg_color_settings', array(
	    'default' => __('#5dc04a', 'pikaxo'),
	    'sanitize_callback'  => 'esc_attr',
	));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pikaxo_navbar_section_bg_color_control', array(
	    'label'    => __('Navbar Background Color', 'pikaxo'),
	    'section'  => 'pikaxo_navbar_section',
	    'settings' => 'pikaxo_panbtn_section_bg_color_settings',
	)));

		//BEGIN PIKAXO SIDE NAV BACKGROUND COLOR SECTION
	$wp_customize->add_section('pikaxo_sidenav_section', array(
		'title' => __('Pikaxo Theme - Side Nav BG Color', 'pikaxo'),
		'priority' => 11,
		'panel' => 'pikaxo_site_layout_option_panel',
	));

	
	//PIKAXO SIDE NAV SECTION BACKGROUND COLOR
	$wp_customize->add_setting('pikaxo_sidenav_bg_color_settings', array(
	    'default' => __('#5dc04a', 'pikaxo'),
	    'sanitize_callback'  => 'esc_attr',
	));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pikaxo_sidenav_bg_color_control', array(
	    'label'    => __('Side Nav Background Color', 'pikaxo'),
	    'section'  => 'pikaxo_sidenav_section',
	    'settings' => 'pikaxo_sidenav_bg_color_settings',
	)));


	//BEGIN PIKAXO READ MORE BACKGROUND COLOR SECTION
	$wp_customize->add_section('pikaxo_readmore_section', array(
		'title' => __('Pikaxo Theme - Read More BG Color', 'pikaxo'),
		'priority' => 11,
		'panel' => 'pikaxo_site_layout_option_panel',
	));

	
	//PIKAXO READ MORE SECTION BACKGROUND COLOR
	$wp_customize->add_setting('pikaxo_readmore_bg_color_settings', array(
	    'default' => __('#5dc04a', 'pikaxo'),
	    'sanitize_callback'  => 'esc_attr',
	));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pikaxo_readmore_bg_color_control', array(
	    'label'    => __('Read More Background Color', 'pikaxo'),
	    'section'  => 'pikaxo_readmore_section',
	    'settings' => 'pikaxo_readmore_bg_color_settings',
	)));


	//BEGIN SIDEBAR BACKGROUND COLOR SECTION
	$wp_customize->add_section('pikaxo_sidebar_section', array(
		'title' => __('Pikaxo Theme - Sidebar Header BG Color', 'pikaxo'),
		'priority' => 11,
		'panel' => 'pikaxo_site_layout_option_panel',
	));

	
	//SIDEBAR SECTION BACKGROUND COLOR
	$wp_customize->add_setting('pikaxo_sidebar_header_bg_color_settings', array(
	    'default' => __('#5dc04a', 'pikaxo'),
	    'sanitize_callback'  => 'esc_attr',
	));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pikaxo_sidebar_header_bg_color_control', array(
	    'label'    => __('Sidebar Header Background Color', 'pikaxo'),
	    'section'  => 'pikaxo_sidebar_section',
	    'settings' => 'pikaxo_sidebar_header_bg_color_settings',
	)));



	//BEGIN PIKAXO PAGINATION BACKGROUND COLOR SECTION
	$wp_customize->add_section('pikaxo_pagination_section', array(
		'title' => __('Pikaxo Theme - Pagination BG Color', 'pikaxo'),
		'priority' => 11,
		'panel' => 'pikaxo_site_layout_option_panel',
	));

	
	//PIKAXO PAGINATION SECTION BACKGROUND COLOR
	$wp_customize->add_setting('pikaxo_pagination_bg_color_settings', array(
	    'default' => __('#5dc04a', 'pikaxo'),
	    'sanitize_callback'  => 'esc_attr',
	));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pikaxo_pagination_bg_color_control', array(
	    'label'    => __('Page Numbers Background Color', 'pikaxo'),
	    'section'  => 'pikaxo_pagination_section',
	    'settings' => 'pikaxo_pagination_bg_color_settings',
	)));


	//BEGIN PIKAXO SEARCH BUTTON SIDEBAR BACKGROUND COLOR SECTION
	$wp_customize->add_section('pikaxo_search_btn_sidebar_section', array(
		'title' => __('Pikaxo Theme - Search Button BG Color', 'pikaxo'),
		'priority' => 11,
		'panel' => 'pikaxo_site_layout_option_panel',
	));

	
	//PIKAXO SEARCH BUTTON SIDEBAR SECTION BACKGROUND COLOR
	$wp_customize->add_setting('pikaxo_search_btn_sidebar_bg_color_settings', array(
	    'default' => __('#5dc04a', 'pikaxo'),
	    'sanitize_callback'  => 'esc_attr',
	));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pikaxo_search_btn_sidebar_bg_color_control', array(
	    'label'    => __('Search Button Sidebar Background Color', 'pikaxo'),
	    'section'  => 'pikaxo_search_btn_sidebar_section',
	    'settings' => 'pikaxo_search_btn_sidebar_bg_color_settings',
	)));



	//BEGIN FOOTER BACKGROUND COLOR SECTION
	$wp_customize->add_section('pikaxo_footer_section', array(
		'title' => __('Pikaxo Theme - Footer BG Color', 'pikaxo'),
		'priority' => 11,
		'panel' => 'pikaxo_site_layout_option_panel',
	));

	
	//FOOTER SECTION BACKGROUND COLOR
	$wp_customize->add_setting('pikaxo_footer_bg_color_settings', array(
	    'default' => __('#5dc04a', 'pikaxo'),
	    'sanitize_callback'  => 'esc_attr',
	));

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pikaxo_footer_bg_color_control', array(
	    'label'    => __('footer Header Background Color', 'pikaxo'),
	    'section'  => 'pikaxo_footer_section',
	    'settings' => 'pikaxo_footer_bg_color_settings',
	)));



    $wp_customize->get_setting('pikaxo_panbtn_section_bg_color_settings')->transport        		= 	'postMessage';
    $wp_customize->get_setting('pikaxo_sidenav_bg_color_settings')->transport 				= 	'postMessage';
    $wp_customize->get_setting('pikaxo_readmore_bg_color_settings')->transport 				= 	'postMessage';
    $wp_customize->get_setting('pikaxo_sidebar_header_bg_color_settings')->transport 				= 	'postMessage';
    $wp_customize->get_setting('pikaxo_search_btn_sidebar_bg_color_settings')->transport 				= 	'postMessage';
    $wp_customize->get_setting('pikaxo_pagination_bg_color_settings')->transport 				= 	'postMessage';
    $wp_customize->get_setting('pikaxo_footer_bg_color_settings')->transport 				= 	'postMessage';


}


//CSS
function pikaxo_custom_css(){
	?>

<style type="text/css">


.panbtn {
	background: <?php echo esc_html(get_theme_mod('pikaxo_panbtn_section_bg_color_settings')); ?>;
}

.side-nav {
	background: <?php echo esc_html(get_theme_mod('pikaxo_sidenav_bg_color_settings')); ?>;
}

.pikaxo-index .inner .card-outer .card-box .card-items .content .content-inner p .read-more {
	background: <?php echo esc_html(get_theme_mod('pikaxo_readmore_bg_color_settings')); ?>;
}

.sidebar .sidebar-inner .sidebar-items h2 {
	background: <?php echo esc_html(get_theme_mod('pikaxo_sidebar_header_bg_color_settings')); ?>;
}

.sidebar .sidebar-inner .sidebar-items .searchform div #searchsubmit {
	background: <?php echo esc_html(get_theme_mod('pikaxo_search_btn_sidebar_bg_color_settings')); ?>;
}

.page-numbers {
	background: <?php echo esc_html(get_theme_mod('pikaxo_pagination_bg_color_settings')); ?>;	
}

.footer-3-col {
	background: <?php echo esc_html(get_theme_mod('pikaxo_footer_bg_color_settings')); ?>; 
}

</style>


	<?php

}


function pikaxo_theme_customizer_live_preview()
{
	wp_enqueue_script( 
		  'pikaxo-theme-site-layout-customizer',			
		  get_template_directory_uri().'/js/pikaxo-customizer.js',
		  array( 'jquery','customize-preview' ),	
		  '',						
		  true						
	);
}




add_action('wp_head', 'pikaxo_custom_css');
add_action('customize_register', 'pikaxo_customizer_settings');
add_action( 'customize_preview_init', 'pikaxo_theme_customizer_live_preview' );


?>