<?php 
/**
 * @version    0.1.84
 * @package    pikaxo
 * @author     Zidithemes
 * @copyright  Copyright (C) 2021 zidithemes.com All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Template Name: Full Width Post No Title Template
 * Template Post Type: post
 * 
 */
?>

<?php get_header(); ?>

<main id="main" class="site-main" role="main">

	<header class="page-header">
		<h1 class="entry-title"></h1>
	</header>

	<div class="page-content">

			<div class="flowid pikaxo-single-full-width-no-title">

			    <div class="mg-auto wid-100 mobwid-100">
			        
			        <div class="inner jc-sp-btw dsply-fl fl-wrap">
			            
			            <div class="card-outer wid-100 mobwid-100">
			            	<div class="card-box jc-sp-btw dsply-fl fl-wrap">
				            	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


								<div class="card-items wid-100 mobwid-100">
									<!-- BEGIN THUMBNAIL -->
							        
							        <!-- END THUMBNAIL -->
							        <div class="content">
							            <div class="content-inner">
							            	<!-- BEGIN TITLE -->
							            	
							                <!-- END TITLE -->
							                <p><?php the_content(); ?> </p>

							            </div>
							        </div>
							        <div class="tags">
										<?php the_tags('<div class="taglist-results dsply-ib"><div class="taglist-items">','</div><div class="taglist-items">', '</div></div>'); ?>
									</div>
							    </div>
							    <?php comments_template(); ?>
				            	<?php endwhile; else : ?>
									
									<h2><?php esc_html__('No posts Found!', 'pikaxo'); ?></h2>
				                    
				                <?php endif; ?>

			                </div>
			            </div>
			            <!-- BEGIN SIDEBAR -->
			            
			            <!-- END SIDEBAR -->

			        </div>
			        
        <div class="pikaxo_link_pages">
            <?php wp_link_pages(); ?>
        </div>
			    </div>
			</div>


		

	</div>

</main>


<?php get_footer(); ?>