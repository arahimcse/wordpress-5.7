<?php 
/**
 * @version    0.1.84
 * @package    pikaxo
 * @author     Zidithemes
 * @copyright  Copyright (C) 2021 zidithemes.com All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * 
 * TEMPLATE NAME: Page No Sidebar & No Title
 */
?>

<?php get_header(); ?>


<main id="main" class="site-main" role="main">

	<header class="page-header">
		<h1 class="entry-title"></h1>
	</header>

	<div class="page-content">

		<div class="flowid pikaxo-page pikaxo-woocommerce-page pikaxo-edd-page">

		    <div class="mg-auto page-no-sidebar wid-100 mobwid-100">
		        
		        <div class="inner jc-sp-btw dsply-fl fl-wrap">
		            
		            <div class="card-outer wid-100 mobwid-100">
		            	<div class="card-box jc-sp-btw dsply-fl fl-wrap">
			            	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


							<div class="card-items wid-100 mobwid-100">
								<h2 class="title">
				                	<a href="<?php the_permalink(); ?>"  >
				                		<?php the_title(); ?>
				                	</a>
				                </h2>
						        <div class="img-box">
						            <?php if ( has_post_thumbnail()) : ?>
						        		<a href="<?php the_permalink(); ?>"  >
						            		<?php the_post_thumbnail(); ?>
						            	</a>
						            <?php endif; ?>	
						        </div>
						        <div class="content">
						            <div class="content-inner">
						                
						                <p class="para"><?php the_content(); ?> </p>
						            </div>
						        </div>
						    </div>
			            	<?php endwhile; else : ?>
								
								<h2><?php esc_html__('No posts Found!', 'pikaxo'); ?></h2>
			                    
			                <?php endif; ?>

		                </div>
		            </div>
		            
        <div class="pikaxo_link_pages">
            <?php wp_link_pages(); ?>
        </div>
		        </div>

		    </div>
		</div>

	</div>

</main>


</div>
<?php get_footer(); ?>