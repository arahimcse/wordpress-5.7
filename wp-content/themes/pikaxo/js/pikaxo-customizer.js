 ( function( $ ) {
 	//js
	wp.customize( 'pikaxo_panbtn_section_bg_color_settings', function( value ) {
		value.bind( function( val ) {
			$( '.panbtn' ).css('background-color', val );
		});
	});	

	wp.customize( 'pikaxo_sidenav_bg_color_settings', function( value ) {
		value.bind( function( val ) {
			$( '.side-nav' ).css('background-color', val );
		});
	});	

	wp.customize( 'pikaxo_readmore_bg_color_settings', function( value ) {
		value.bind( function( val ) {
			$( '.pikaxo-index .inner .card-outer .card-box .card-items .content .content-inner p .read-more' ).css('background-color', val );
		});
	});	

	wp.customize( 'pikaxo_sidebar_header_bg_color_settings', function( value ) {
		value.bind( function( val ) {
			$( '.sidebar .sidebar-inner .sidebar-items h2' ).css('background-color', val );
		});
	});	

	wp.customize( 'pikaxo_search_btn_sidebar_bg_color_settings', function( value ) {
		value.bind( function( val ) {
			$( '.sidebar .sidebar-inner .sidebar-items .searchform div #searchsubmit' ).css('background-color', val );
		});
	});	

	wp.customize( 'pikaxo_pagination_bg_color_settings', function( value ) {
		value.bind( function( val ) {
			$( '.page-numbers' ).css('background-color', val );
		});
	});	

	wp.customize( 'pikaxo_footer_bg_color_settings', function( value ) {
		value.bind( function( val ) {
			$( '.footer-3-col' ).css('background-color', val );
		});
	});	


} )( jQuery );