<?php 
/**
 * The template for displaying woocommerce
 *
 * @version    0.1.84
 * @package    pikaxo
 * @author     Zidithemes
 * @copyright  Copyright (C) 2021 zidithemes.com All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * 
 */
?>

<?php get_header(); ?>

	<div class="flowid pikaxo-index pikaxo-woocommerce-index">

	    <div class="mg-auto wid-90 mobwid-90">
	        
	        <div class="inner jc-sp-btw dsply-fl fl-wrap">
	            
	            <div class="card-outer wid-69 mobwid-100">
	            	<div class="card-box jc-sp-btw dsply-fl fl-wrap">
		            	
		            	<?php  woocommerce_content(); ?>

	                </div>
	            </div>
	            <?php get_sidebar(); ?>

	        </div>

	    </div>
	</div>




<?php get_footer(); ?>