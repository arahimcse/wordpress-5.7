=== Pikaxo ===
Contributors: Zidithemes
Requires at least: 4.7
Tested up to: 5.6.2
Requires PHP: 5.2.4
Version: 0.1.84
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: two-columns, right-sidebar, footer-widgets, Blog, News, Education

== Description ==

Pikaxo is a responsive wordpress theme which improves blog and news experience with minimal and easy to use design which works on all devices.  It is created for blog, publishers, magazines, newspaper, news, editors and bloggers. It is a simple design and multi-purpose theme compatible with elementor page builder and woocommerce.

== Installation ==
1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. click the 'Upload Theme' button to upload the .zip theme file.
3. Click on the 'Activate' button to use your new theme.
4. Navigate to Appearance > Customize in your admin panel.

== Copyright ==

Pikaxo WordPress Theme, Copyright 2021 Zidithemes
Pikaxo is distributed under the terms of the GNU GPL

Pikaxo bundles the following resources:

Bundled screenshot image, Copyright Zidithemes
License: CC0 1.0 Universal (CC0 1.0)
Source: https://www.zidithemes.tumblr.com/photos

Image used in screenshot.png: A photo by sasint
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pixabay.com/en/umbrella-buddhism-monk-monastery-1807513/


== Changelog ==

= 0.1.84 =
* Released: February 23, 2021

added tested up to, requires at least, requires PHP to style.css.

= 0.1.74 =
* Released: June 17, 2020

added tested up to, requires at least, requires PHP to style.css.

= 0.1.73 =
* Released: April 3, 2020

Updated after WordPress 5.4 release.

= 0.1.72 =
* Released: January 1, 2020

Added PHP requirements to readme.txt, added skip links.

= 0.1.71 =
* Released: February 21, 2019

Added Theme Options page and customizer settings.

= 0.1.6 =
* Released: January 6, 2019

Fix view cart issues, fix readme issues, added Gutenberg Compatibility, added full width template for posts

= 0.1.5 =
* Released: October 10, 2018

Fix uri issues

= 0.1.4 =
* Released: September 8, 2018

Added full width template (page no sidebar)

= 0.1.3 =
* Released: August 20, 2018

Fix sidebar class issues
fix functions issues
fix site name issues

= 0.1.2 =
* Released: August 15, 2018

Fix category script issues

= 0.1.1 =
* Released: August 15, 2018

Fix css issues
Fix script issues

= 0.0.9 =
* Released: August 14, 2018

Fix 404 issues
Fix search issues

= 0.0.8 =
* Released: August 10, 2018

Fix functions issues
Fix pagination issues
Fix post class issues
Fix charset issues

= 0.0.7 =
* Released: August 8, 2018

Fix page css issues

= 0.0.6 =
* Released: August 7, 2018

Fix widgets issues

= 0.0.5 =
* Released: August 5, 2018

Fix elementor and woocommerce compatibility issues

= 0.0.4 =
* Released: August 3, 2018

Fix pagination issues

= 0.0.3 =
* Released: July 30, 2018

Fix html issues

= 0.0.2 =
* Released: June 30, 2018

Fix screenshot issues

= 0.0.1 =
* Released: June 24, 2018

Initial release

== Resources  ==
* pixabay.com @ 2018 Pixabay, CC0

## Images used in screenshot ##
https://pixabay.com/en/umbrella-buddhism-monk-monastery-1807513/