<?php 
/**
 * @version    0.1.84
 * @package    pikaxo
 * @author     Zidithemes
 * @copyright  Copyright (C) 2021 zidithemes.com All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * 
 */
?>

<?php get_header(); ?>


<main id="main" class="site-main" role="main">

    <header class="page-header">
    </header>
    
    <div class="page-content">
        <div class="flowid pikaxo-index">

            <div class="mg-auto wid-90 mobwid-90">
                
                <div class="inner jc-sp-btw dsply-fl fl-wrap">
                    
                    <div class="card-outer wid-69 mobwid-100">
                    	<div class="card-box jc-sp-btw dsply-fl fl-wrap">
        	            	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        	            		<div id="post-<?php the_ID(); ?>" <?php post_class('card-items wid-99 mobwid-100'); ?> >
                                    <div class="img-box">
                                        <?php if ( has_post_thumbnail()) : ?>
                                            <a href="<?php the_permalink(); ?>"  >
                                                <?php the_post_thumbnail(); ?>
                                            </a>
                                        <?php else: ?>
                                            <div class="user-no-img-items">
                                                <div class="user-no-img-items-inner text-center">
                                                    <div class=""><?php esc_html_e( 'No Image', 'pikaxo' ); ?></div>
                                                </div>
                                            </div>
                                        <?php endif; ?> 
                                    </div>
                                    <div class="content">
                                        <div class="content-inner">
                                            <h2>
                                                <a href="<?php the_permalink(); ?>"  >
                                                    <?php the_title(); ?>
                                                </a>
                                            </h2>
                                            <div class="meta">
                                                <span class="mg-rt-10 date"><?php the_time(get_option('date_format')); ?></span>
                                                <span class="mg-rt-10 author"><?php the_author_posts_link(); ?></span>
                                                <span class="comments"><a href="<?php comments_link(); ?>"> <?php comments_number(); ?> </a></span>
                                            </div>
                                            <p><?php the_excerpt(__('more &raquo;', 'pikaxo')); ?> </p>
                                        </div>
                                    </div>
                                    <div class="pikaxo_link_pages">
                                        <?php wp_link_pages(); ?>
                                    </div>
                                </div>

        	            	<?php endwhile; else : ?>
        						
        						<h2><?php esc_html__('No posts Found!', 'pikaxo'); ?></h2>
        	                    
        	                <?php endif; ?>

                        </div>
                        <ul class="pagination flowid">
        				   <?php the_posts_pagination(); ?>
        				</ul>
                    </div>
                    <?php get_sidebar(); ?>

                </div>

            </div>
        </div>
    </div>

</main>

<?php get_footer(); ?>