<?php 
/**
 * @version    0.1.84
 * @package    pikaxo
 * @author     Zidithemes
 * @copyright  Copyright (C) 2021 zidithemes.com All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * 
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>	
<body <?php body_class(); ?> >
<?php wp_body_open(); ?>	
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'pikaxo' ); ?></a>    

<div class="page-no-sidebar page-no-title">
<?php

if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'header' ) ) {
?>
	<header id="site-header" class="site-header" role="banner">

	</header>    
<!-- BEGIN NAV MENU -->
	

	<input type="checkbox" class="navcheck" id="navcheck"/>
	<label class="navlabel" for="navcheck" ></label>
	<button class="panbtn" for="navcheck">
		<div></div>
		<div></div>
		<div></div>
	</button>

	<div class="side-nav">
            
            <div class="list">

                
                <a class="site-nav-title" href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-name"><?php echo bloginfo('name'); ?></a>
                <ul>
					<?php 
	                    if (has_nav_menu('primary')) {
	                        wp_nav_menu(array(
	                            'theme_location' => 'primary',
	                            'menu_class'  => '',
	                        ));
	                        }else {
	                         //echo '<ul class="">';
	                         wp_list_pages( array('depth' => 1, 
	                             'title_li' => '')); 
	                         //echo '</ul>';     
	                     } 

	                ?>
				</ul>
            </div>
        
            
            
        </div>
        
	
	<!-- END NAV MENU -->

	<!-- BEGIN TOP -->
<div class="flowid top-header">

    <div class="mg-auto wid-90 mobwid-90">
        
        <div class="inner dsply-fl fl-wrap">
            
            <div class="top-header-items wid-100 text-center">
                <h2>
                	<a class="site-nav-title" href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-name"><?php echo bloginfo('name'); ?></a>
                </h2>
            </div>
            
        </div>

    </div>

</div>
	<!-- END TOP -->


<?php } ?>	