/*
* custom pagination
* link @ https://developer.wordpress.org/reference/functions/paginate_links/ 
*/
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
 $args = array(
  'order'          => 'DESC',
  'orderby'        => 'date',
  'posts_per_page'      => '10',
  'paged'          => $paged
  );
 $parent = new WP_Query( $args );

 $pagination = paginate_links( array(
  'base' => get_pagenum_link(1) . '%_%', //=> get_home_url() .'/%_%'
  'format' => 'page/%#%',
  'type' => 'array', //instead of 'list'
  'total' => $parent->max_num_pages,
  'current' => $paged,
  'mid_size' => 2
  )); 
  ?>
  <?php if ( ! empty( $pagination ) ) : ?>
  <div class="container">
    <div class="row">
      <ul class="pagination">
          <?php foreach ( $pagination as $key => $page_link ) : ?>
          <li class="page-item<?php if ( strpos( $page_link, 'current' ) !== false ) { echo ' active'; } ?>"><?php echo str_replace('page-numbers', 'page-link', $page_link) ?></li>
          <?php endforeach ?>
      </ul>
    </div>
  </div>
<?php endif ?>
