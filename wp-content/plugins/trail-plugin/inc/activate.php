<?php

namespace App;

class activate{
    public function init()
    {
        add_action( 'init', [$this, 'pluginprefix_setup_post_type']);
    }

    public function pluginprefix_setup_post_type()
    {
        register_post_type( 'rahim10046', ['public' => true ] ); 
        flush_rewrite_rules(); 
    }
}