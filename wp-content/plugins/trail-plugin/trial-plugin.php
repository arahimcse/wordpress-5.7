<?php


/**
 * Plugin Name: Trail Plugin
 */

require_once dirname(__FILE__).'/vendor/autoload.php';

class action_trial_plugin{

    public function activate(){
        new \App\activate->init();
    }

    public function deactivate(){
        new \App\deactivate->init();
    }
}

//active
register_activation_hook(__FILE__, ['action_trial_plugin', 'activate']);

//deactivate
register_deactivation_hook(__FILE__, ['action_trial_plugin', 'deactivate']);
?>