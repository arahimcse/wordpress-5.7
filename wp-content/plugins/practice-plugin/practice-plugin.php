<?php
/**
 * Plugin Name:       Practice Plugin
 * Plugin URI:        
 * Description:       This is the practice purpose plugin.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Abdur Rahim
 * Author URI:        
 * Text Domain:       practice-plugin
 * Domain Path:       /languages
 */

require_once dirname(__FILE__) .'/vendor/autoload.php';

use practicePlugin\active;
use practicePlugin\deactive;

class practice_plugin_action{

  public function activation()
  {
    $obj = new active();
    $obj->pluginprefix_activate();
  }

  public function deactivation()
  {
    new \practicePlugin\deactive();
  }
}
  /**
  * Activate WordPress plugin
  * link@ https://developer.wordpress.org/plugins/plugin-basics/activation-deactivation-hooks/#activation
  */
 register_activation_hook(__FILE__, [new practice_plugin_action(), 'activation']);

  /**
   * Deactivation Wordpress plugin
   * link@ https://developer.wordpress.org/plugins/plugin-basics/activation-deactivation-hooks/#deactivation
   */
  register_deactivation_hook(__FILE__, [new practice_plugin_action(), 'deactivation']);

   /**
    * Wordpress boilerplate template 
    * link@ https://developer.wordpress.org/plugins/plugin-basics/best-practices/#architecture-patterns
    */
    
?>