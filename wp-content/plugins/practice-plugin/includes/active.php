<?php
namespace practicePlugin;

class active{
    public function __construct()
    {
        add_action( 'init', [$this,'pluginprefix_setup_post_type']);
    }

    /**
     * Register the "book" custom post type
     */
    public function pluginprefix_setup_post_type() {
        register_post_type( 'book', ['public' => true ] ); 
    } 

    /**
     * Activate the plugin.
     */
    public function pluginprefix_activate() { 
        // Trigger our function that registers the custom post type plugin.
        $this->pluginprefix_setup_post_type(); 
        // Clear the permalinks after the post type has been registered.
        flush_rewrite_rules(); 
    }
}
?>