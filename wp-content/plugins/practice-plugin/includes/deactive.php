<?php
namespace practicePlugin;

class deactive{
    public function __construct()
    {
        // Unregister the post type, so the rules are no longer in memory.
        unregister_post_type( 'rahim10046' );
        // Clear the permalinks to remove our post type's rules from the database.
        flush_rewrite_rules();
    }

}

?>