<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit368189b730f3000ebb08c6521c1bf3aa
{
    public static $prefixLengthsPsr4 = array (
        'd' => 
        array (
            'dhana\\' => 6,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'dhana\\' => 
        array (
            0 => __DIR__ . '/../..' . '/includes',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit368189b730f3000ebb08c6521c1bf3aa::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit368189b730f3000ebb08c6521c1bf3aa::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit368189b730f3000ebb08c6521c1bf3aa::$classMap;

        }, null, ClassLoader::class);
    }
}
