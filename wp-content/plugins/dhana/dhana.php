<?php
/**
 * Plugin Name: Dhana
 */

 /**
  * register wordpress plugin
  */
  require_once 'vendor/autoload.php';
  use dhana\dhana_plugin_init as plugin_init;

register_activation_hook(__FILE__, [new plugin_init(), 'active']);

register_deactivation_hook(__FILE__, [new plugin_init(), 'deactive']);

/**
 * plugin note review
 * https://developer.wordpress.org/plugins/intro/
 * https://developer.wordpress.org/plugins/plugin-basics/
 * https://developer.wordpress.org/plugins/security/
 * https://developer.wordpress.org/plugins/hooks/
 * https://developer.wordpress.org/plugins/privacy/
 * https://developer.wordpress.org/plugins/administration-menus/
 * https://developer.wordpress.org/plugins/shortcodes/
 * https://developer.wordpress.org/plugins/settings/
 * https://developer.wordpress.org/plugins/post-types/
 */

