<?php
namespace dhana;

class dhana_plugin_init{
    public function __construct()
    {
      add_action('init', [$this, 'wporg_custom_post_type']);
    }
    public function active()
    {
          // Trigger our function that registers the custom post type plugin.
          $this->wporg_custom_post_type();
          // Clear the permalinks after the post type has been registered.
          flush_rewrite_rules(); 
    }
  
    /**
   * Register the "products" custom post type
   */
    public function wporg_custom_post_type()
    {
      register_post_type('wporg_product',
          array(
              'labels'      => array(
                  'name'          => __( 'Products', 'textdomain' ),
                  'singular_name' => __( 'Product', 'textdomain' ),
              ),
              'public'      => true,
              'has_archive' => true,
              'rewrite'     => array( 'slug' => 'products' ), // my custom slug
          )
      );
    }
    public function deactive()
    {
      // Unregister the post type, so the rules are no longer in memory.
      unregister_post_type( 'wporg_product' );
      // Clear the permalinks after the post type has been registered.
      flush_rewrite_rules(); 
    }
  }