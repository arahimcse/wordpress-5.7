<?php
namespace dhana;

class active{
    public function init()
    {
        add_action( 'init', [$this, 'dhana_setup_post_type'] );
        // Clear the permalinks after the post type has been registered.
        flush_rewrite_rules(); 
    }

    public function dhana_setup_post_type()
    {
        register_post_type( 'rahimpost', ['public' => true ] ); 
    }
}
