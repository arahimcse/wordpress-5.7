<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Wordpress REST API</title>
</head>
<body>
	<h3>wordpress rest api</h3>
	<!--Access WordPress Dashboard-->
	<button id="user-auth-btn">check user access</button>

	<!--Take Input form user-->
	<input id="title_area" type="text" class="text" placeholder="Post Name" name="title">
	<textarea id="content_area" name="content" id="" placeholder="This is content Area" cols="30" rows="10"></textarea>
	<button id="quick-add-btn">add post</button>
	
	<!--get content area-->
	<button id="wordpress-post-btn">view post</button>
	<div id="post-container" class="container"></div>
	<script src="main.js"></script>
</body>
</html>